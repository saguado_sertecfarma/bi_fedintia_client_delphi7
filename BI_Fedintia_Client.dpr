program BI_Fedintia_Client;

uses
  Forms,
  Windows,
  dIALOGS,
  BI in 'BI.pas' {BI_Form1},
  FTP1 in 'Connections\FTP1.pas',
  BI_Generator in 'Processes\BI_Generator.pas',
  BI_DM in 'BD\BI_DM.pas' {DataModule1: TDataModule},
  INI_Read in 'BD\INI_Read.pas',
  Calculate in 'Processes\Calculate.pas',
  MA_Generator in 'Processes\MA_Generator.pas',
  BI_Functions in 'BI Functions\BI_Functions.pas',
  IMS_Generator in 'Processes\IMS_Generator.pas',
  BI_Check in 'Processes\BI_Check.pas',
  Respuesta_Consulta_WEB in 'Connections\Respuesta_Consulta_WEB.pas';

{$R *.res}

  const NombreMutex='BI_Fedintia_Client';
  var MiMutex:Thandle;


begin
     mimutex:= CreateMutex(nil,true,NombreMutex);
     if MiMutex=0 then
     begin
          halt;
     end;
     if GetLastError=ERROR_ALREADY_EXISTS then
     begin
          halt;
     end;

     Application.Initialize;
     Application.Title := 'BI_Fedintia_Client.exe';
     Application.CreateForm(TBI_Form1, BI_Form1);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
     CloseHandle(MiMutex);
     
end.
