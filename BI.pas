unit BI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ShellAPI, Menus, Mask, ToolEdit;

    procedure ActualizaInformacion(Cadena :String);

const
  WM_MY_MESSAGE = wm_user + 0;

  //SERGIO AGUADO. 11/05/2021. CUANDO SE PONE A TRUE SE VERIFICARAN LAS CONSTANTES "PRUEBAS..." ANTES DE ENVIAR.
  PRUEBAS = TRUE;
  PRUEBAS_ENVIAR = TRUE; {--> cuando estamos en pruebas �enviar ficheros?}
  PRUEBAS_NOM_FICHERO_CODIGO_SOCIO = '99999'; {--> cuando estamos en pruebas, en el nombre del fichero a enviar le pongo este codigo de socio}

  //SERGIO AGUADO. 11/05/2021. CUANDO SE PONE A TRUE, AL ENVIAR POR HTTPS LOS FICHEROS DE DATOS NO ENVIA LOS FICHEROS .FIN(al ftp) .
  ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP = FALSE;
  RESULTADO_OK = 0;
  RESULTADO_ERROR = 1;

  //SERGIO COMUNICACI�N CON IOFCONNECT
  WM_MESSAGE_FINALIZACION_OK    = wm_user + 1;
  WM_MESSAGE_FINALIZACION_ERROR = wm_user + 2;

type
  TBI_Form1 = class(TForm)
    Panel1: TPanel;
    LB_Proceso: TLabel;
    B_Day: TButton;
    MenuBandeja: TPopupMenu;
    Mostrar: TMenuItem;
    Timer1: TTimer;
    PB_BI: TProgressBar;
    PB_FTP: TProgressBar;
    B_Manual: TButton;
    dManual: TDateEdit;
    btnBICheck: TButton;
    PB_BICHECK: TProgressBar;
    lbEntorno: TLabel;
    lbEnviara: TLabel;
    LB_Proceso02: TStaticText;
    procedure B_WeekClick(Sender: TObject);
    procedure B_DayClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MostrarClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OnMyMessage(var Msg: TMessage); message WM_MY_MESSAGE;
    procedure B_ManualClick(Sender: TObject);
    procedure btnBICheckClick(Sender: TObject);

  private
   IconData: TNotifyIconData;

   procedure WMSysCommand( var Msg: TWMSysCommand ); message WM_SYSCOMMAND;
   procedure Restaurar( var Msg: TMessage ); message WM_USER+1;
   procedure minimizar;

    { Private declarations }
  public
   fecha : Tdate;
   Salida : Boolean;
   Codigo_Resultado_Proceso: integer;
   Texto_Resultado_Proceso: string;
  end;

var
   BI_Form1      :TBI_Form1;
   PARAMETRO     :string;
   //RetardoDiario :Boolean;
   BIModo        :Integer;
   BI_Day_Auto :Boolean;
   HandleIOFWIN: THandle;
implementation

{$R *.dfm}

uses BI_Generator, BI_DM, INI_Read, UDF, BI_Check, BI_Functions;

var
   NO_MINIMIZADO : Boolean =  True;

procedure TBI_Form1.OnMyMessage(var Msg: TMessage);
begin
    //hide;
    minimizar;
    B_DayClick(nil);
end;


procedure TBI_Form1.B_WeekClick(Sender: TObject);
begin
     INI_Read.Read(1);
end;

procedure ActualizaInformacion(Cadena :String);
begin
     BI_Form1.LB_Proceso.Caption := Cadena;
     BI_Form1.LB_Proceso02.Caption := Cadena;
     Application.ProcessMessages;
end;

procedure TBI_Form1.B_DayClick(Sender: TObject);
begin
     INI_Read.Read(2);
end;

procedure TBI_Form1.FormCreate(Sender: TObject);
var
   n             :integer;
   nParametro    :integer;
begin
     codigo_resultado_proceso := RESULTADO_OK;
     texto_resultado_proceso := '';

     PARAMETRO := '2';
     BI_Day_Auto :=False;

     n := ParamCount;
     if ( n > 0  ) then
     begin
          nParametro := strtoint(ParamStr(1));
          case nParametro of
              1:begin
                     //Llamada desde BI Click
                     BIModo := 1;
                end;
              2:begin //Escalonado
                     BI_Day_Auto :=True;
                     BIModo := nParametro;
                end;
              3:begin  //00:00
                     BIModo := nParametro;
                end;
          else
                begin  //El resto que no estan controlados, para el cierre, iofwin nos envia su handle
                    HandleIOFWIN := StrToInt(ParamStr(1));
                    BIModo := 99;
                    BI_Day_Auto := True;
                end;
          end;
     end
     else
     begin
          BIModo := 1;
     end;
     infoPrograma:= GetAppVersion();
     BI_Form1.Caption:= 'IOFWIN ' + infoPrograma;
     if PRUEBAS then begin
        BI_Form1.lbEntorno.Color := $00F5FC03; //verde claro
        BI_Form1.lbEntorno.caption := 'Ejecutando en modo pruebas, cod.socio pruebas: '+PRUEBAS_NOM_FICHERO_CODIGO_SOCIO;
     end
     else
     begin
        BI_Form1.lbEntorno.Color := $00D5FFFF; //amarillo //$$00A8A8FF; //rojo claro
        BI_Form1.lbEntorno.caption := 'Ejecutando en producci�n, cod.socio se obtendr� de la BD';
     end;
     if PRUEBAS_ENVIAR then begin
        BI_Form1.lbEnviara.Color := $00D5FFFF; //verde claro
        BI_Form1.lbEnviara.caption := 'Se enviar� facturaci�n';
     end
     else
     begin
        BI_Form1.lbEnviara.Color := $00F5FC03; //amarillo //$$00A8A8FF; //rojo claro
        BI_Form1.lbEnviara.caption := 'No se enviar� nada';
     end;

     Salida := False;
end;

procedure TBI_Form1.FormActivate(Sender: TObject);
begin
     if((BI_Day_Auto = true) AND (NO_MINIMIZADO)) then
     begin
          NO_MINIMIZADO := False;
          PostMessage(self.Handle,WM_MY_MESSAGE,0,0);
     end;
end;

 //menu//
procedure TBI_Form1.MostrarClick(Sender: TObject);
begin
      // Volvemos a mostrar de nuevo el formulario
     BI_Form1.Show;
     ShowWindow( Application.Handle, SW_SHOW );

     // Eliminamos el icono de la bandeja del sistema
     Shell_NotifyIcon( NIM_DELETE, @IconData );
     IconData.Wnd := 0;
end;

procedure TBI_Form1.WMSysCommand( var Msg: TWMSysCommand );
begin
     if Msg.CmdType = SC_MINIMIZE then
       Minimizar
     else
       DefaultHandler( Msg );
end;

procedure TBI_Form1.Restaurar( var Msg: TMessage );
var p: TPoint;
begin
     // �Ha pulsado el bot�n izquierdo del rat�n?
     if Msg.lParam = WM_LBUTTONDOWN then
       MostrarClick( Self );

     // �Ha pulsado en la bandeja del sistema con el bot�n derecho del rat�n?
     if Msg.lParam = WM_RBUTTONDOWN then
     begin
          SetForegroundWindow(Handle);
          GetCursorPos(p);
          MenuBandeja.Popup(p.x, p.y);
          PostMessage(Handle, WM_NULL, 0, 0);
     end;
end;

procedure TBI_Form1.Minimizar;
begin
  with IconData do
  begin
       cbSize := sizeof(IconData);
       Wnd := Handle;
       uID := 100;
       uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
       uCallbackMessage := WM_USER + 1;

       // Usamos de icono el mismo de la aplicaci�n
       hIcon := Application.Icon.Handle;

       // Como Hint del icono, el nombre de la aplicaci�n
       StrPCopy( szTip, Application.Title );
  end;

  // Ponemos el icono al lado del reloj
  Shell_NotifyIcon( NIM_ADD, @IconData );

  // Ocultamos el formulario
  ShowWindow(Application.Handle, SW_HIDE);

 Hide;
end;

procedure TBI_Form1.Timer1Timer(Sender: TObject);
var
cierre :TWMSysCommand;
begin
     //desactivado
     Timer1.Enabled:=False;
     cierre.CmdType:= SC_MINIMIZE;
     WMSysCommand(cierre);
     INI_Read.Read(2);
end;

procedure TBI_Form1.FormClose(Sender: TObject; var Action: TCloseAction);
var
 salir :TWMSysCommand;
 hnd: THandle;
begin
     Shell_NotifyIcon( NIM_DELETE, @IconData );
     Salida := True;

     //SERGIO, 02/06/2021, LE COMUNICO A IOFCONNECT COMO HA FINALIZADO EL PROCESO
     hnd := FindWindow(PChar('TForm1'), PChar('IOFConnect'));
     if hnd > 0 then
     begin
        if Codigo_Resultado_Proceso = RESULTADO_OK then begin
           SendMessage(hnd, WM_MESSAGE_FINALIZACION_OK, 0, 0);
        end
        else
        begin
           if Codigo_Resultado_Proceso = RESULTADO_ERROR then begin
              SendMessage(hnd, WM_MESSAGE_FINALIZACION_ERROR, 0, 0);
           end;
        end;
     end;
end;

procedure TBI_Form1.B_ManualClick(Sender: TObject);
begin
     fecha := dManual.Date;
     if (fecha <> 0) then
     begin
          INI_Read.Read(3);
     end
     else
     begin
          ActualizaInformacion('Fecha no v�lida');
          dManual.Date := 0;
     end;
end;

procedure TBI_Form1.btnBICheckClick(Sender: TObject);
var txtResp:string;
begin
   try
     CompararNumLineasVentaFDB_y_BI('C:\BI_Fedintia_Client_DELPHI7\TEMPORAL\', 'C:\BI_Fedintia_Client_DELPHI7\',txtResp);
   except
     on e:exception do
     begin
        showmessage('Error en prueba BI_CHECK '+e.message);
     end;
   end;

end;


end.
