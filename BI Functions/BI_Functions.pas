unit BI_Functions;

interface
 uses Windows, Forms, SysUtils, BI_DM, Dialogs, StrUtils, INI_Read, ZipMstr, Messages;

 procedure BILog(Mensaje: String);
 Function LimpiaCaracteres(Cadena: String): string;
 Function Eje_SP_Ret(cadena:string; resultEsFecha:boolean=false):variant;
 Function obtenerSiglas (cadena : string) : string;
 procedure BorrarArchivosBI;

 function Replicate(c: char; nLen:integer) : string;
 function Ajusta(sCampo, sOrientacion : string; iLongitud : integer; sRelleno : char) : string;
 Function DameEan7(cCodi: variant):string;
 function ValidaEAN13(cCodProd: string; nEan: Integer): Boolean;
 function ComprimirFicheroZIP(AFileName:string; var outFileName:string):integer;
 function GetAppVersion():string;
 procedure EnviarMSNIOFWIN(HandleIOFWIN :THandle);
 Function dameDatoProveedor(codProv, dato :integer ):string;

 // Constants d'informacio del programa
   const INF_NOMCOMPANYA = 0;
   const INF_DESCRIPCIO = 1;
   const INF_VERSIOARXIU = 2;
   const INF_NOM_INTERN = 3;
   const INF_COPYRIGHT = 4;
   const INF_NOMORIGINAL = 5;
   const INF_NOMPRODUCTE = 6;
   const INF_VERSIOPRODUCTE = 7;
   const INF_LONG = 8;
   const INF_DATA = 9;
   const WM_Datos_BI_Enviados_OK = 9669;

 implementation

uses Variants;


Function LimpiaCaracteres(Cadena: String): string;
var
   n: integer;
Begin
     Cadena := AnsiReplaceStr(Cadena, #10, ' ');
     Cadena := AnsiReplaceStr(Cadena, #13, ' ');

     Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '`', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, #39, ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '"', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, ':', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '&', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '/', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '\', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '-', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '_', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '?', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '!', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '|', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '@', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '#', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '%', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '*', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '^', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '<', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '>', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '[', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, ']', ' ', [rfReplaceAll]);   Cadena := StringReplace(Cadena, '{', ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '}', ' ', [rfReplaceAll]);


     Cadena := StringReplace(Cadena, #126, ' ', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, #124, ' ', [rfReplaceAll]);

     Cadena := StringReplace(Cadena, '�', 'a', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'a', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'a', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'a', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'e', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'e', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'e', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'e', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'i', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'i', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'i', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'i', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'o', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'o', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'o', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'o', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'u', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'u', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'u', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'u', [rfReplaceAll]);

     Cadena := StringReplace(Cadena, '�', 'c', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'C', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'n', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'N', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'A', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'A', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'A', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'A', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'E', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'E', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'E', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'E', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'I', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'I', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'I', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'I', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'O', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'O', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'O', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'O', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'U', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'U', [rfReplaceAll]);
     Cadena := StringReplace(Cadena, '�', 'U', [rfReplaceAll]);     Cadena := StringReplace(Cadena, '�', 'U', [rfReplaceAll]);


     for n:=1 to Length(Cadena) Do
         If NOT (Ord(Cadena[n]) in [32..127]) Then
         begin
              Cadena[n] := ' ';
         end;
     Result := Cadena;

end;

function Eje_SP_Ret(cadena:string; resultEsFecha:boolean=false):variant; {Ejecuta un query i retorna}
begin
     With  BI_DM.DataModule1.IBQ_Ejecuta_SP do
     begin
          Try
             close;
             SQL.Text:= cadena;
             Open;
             result:= Fieldbyname('retorn').asvariant;
             if (resultEsFecha) and (result=null) then result := strToDateTime('01/01/1900 00:00:00');
             Close ;
          Except
             if resultEsFecha then
                result := null
             else
                result := '';
             BILog('Error al ejecutar la cadena : ' + cadena);
             //ShowMessage('Error en la consulta');
          End;
     End;
end;

function obtenerSiglas (cadena : string) : string;
var
  siglas, palabra : string;
  i : integer;
begin
     siglas  := '';
     palabra := '';
     for i := 1 to length(cadena) + 1 do
     begin
          if (cadena[i] <> ' ') and (i <= length(cadena)) then
              palabra := palabra + cadena[i]
          else
          begin
               if palabra <> '' then
               begin
                 siglas := siglas + palabra[1];
                 palabra := '';
               end;
          end;
     end;
     obtenerSiglas := siglas;
end;

procedure BorrarArchivosBI;
var
  SR: TSearchRec;
  Ruta : String;
begin
     Ruta := DIR_IMS;
     if FindFirst(Ruta + '*.*', faArchive+faHidden+faReadOnly, SR)= 0 then
     repeat
       DeleteFile(Ruta+'\'+SR.Name);
     until FindNext(SR) <> 0;
end;

procedure EnviarMSNIOFWIN(HandleIOFWIN :THandle);
begin
     If HandleIOFWIN > 0 Then
     Begin
          SendMessage(HandleIOFWIN, WM_Datos_BI_Enviados_OK, 0, 0);
     end;
end;


function Replicate(c: char; nLen:integer) : string;
begin
  Result := stringOfChar(c, nLen);
end;


function Ajusta(sCampo, sOrientacion : string; iLongitud : integer; sRelleno : char) : string;
begin
  sCampo := Trim(sCampo);
  if uppercase(sOrientacion) = 'I' then
    Result := Replicate(sRelleno,iLongitud - Length(sCampo))+sCampo
  else
    Result := sCampo+Replicate(sRelleno,iLongitud - Length(sCampo));

  if length(Result) > iLongitud then
    Result := copy(Result, 1, iLongitud);
end;


Function DameEan7(cCodi: variant):string;
var  cChecksum : STRING;
      DIGITO,X : INTEGER;
begin

     cCodi:= VarToStr(cCodi);
     result := '000000';
     cCodi:=StringOfChar('0',6-Length(cCodi))+cCodi;

     If Length(cCodi) = 6 Then
     Begin
          cChecksum:='131313';
          digito   :=27;
          For x := 1 to LengtH(cCodi) do
          begin
              digito:= digito + (StrToInt(copy(cCodi,x,1))*StrtoInt(copy(cChecksum,x,1)));
          end;
          If digito > 9 Then
          begin
                digito := 10 - (digito mod 10);
                if digito=10 then
                        digito:=0;
          end;
          result := copy(cCodi+IntToStr(digito),1,7);
     End;
end;

function ValidaEAN13(cCodProd: string; nEan: Integer): Boolean;
var
  i, NumTotal, N, Flag: Integer;
begin
  Result := False;

  // si es un ean 13, pero el cod art�culo es menor al c�digo 0000000999999 salimos
  if (cCodProd <= '0000000999999') and (nEan = 13) then
    Exit;

  // si es un ean de 8 o de 13...
  if (Length(cCodProd) = nEan) then
  begin
    NumTotal := 0;
    N := Length(cCodProd) - 1;
    for i := 1 to N do
    begin
      // es impar?
      if odd(i) then
      begin
        if (N = 12) then
          NumTotal := NumTotal + (StrToInt(cCodProd[i]) * 1)
        else
          NumTotal := NumTotal + (StrToInt(cCodProd[i]) * 3);
      end else
      begin
        if (N = 12) then
          NumTotal := NumTotal + (StrToInt(cCodProd[i]) * 3)
        else
          NumTotal := NumTotal + (StrToInt(cCodProd[i]) * 1);
      end;
    end;

    if (NumTotal > 99) then
      Flag := 10 - (NumTotal mod 100)
    else
      Flag := 10 - (NumTotal mod 10);

    if (Flag = 10) then
      Flag := 0;

    // si el �ltimo n�mero es igual al que hemos calculado, es un ean correcto
    if (StrToInt(cCodProd[N+1]) = Flag) then
      result := True;
  end;
end;

// permite comprimir un fichero utilizando ZIP.
// Result   -1    El fichero fuente, no existe...
// Result   -2    Otros errores inesperados
// Result   0     OK
function ComprimirFicheroZIP(AFileName:string; var outFileName:string):integer;
var
  zMaster: TZipMaster;
  zFileName:string;
  res:integer;
begin
     // Si el fichero no existe, salimos,...
     if Not FileExists(AFileName) then
     begin
          Result := -1;
          Exit;
     end;

     // proteccion para errores
     try
        zMaster := TZipMaster.Create(nil);
        // proteccion para liberar
        try

          // Fichero comprimido
          zFileName := ChangeFileExt(AFileName, '.ZIP');
          // Asignar propiedades
          zMaster.ZipFileName := zFileName;
          zMaster.AddCompLevel := 6;    // compresion baja, + rapidez
          // ficheros
          zMaster.FSpecArgs.Clear;
          zMaster.FSpecArgs.Add(AFileName);
          // A�adir el fichero
          res := zMaster.Add;
          // Descomprimir para testear
          zMaster.FSpecArgs.Clear;
          zMaster.ExtrOptions := zMaster.ExtrOptions + [ExtrTest];
          zMaster.FSpecArgs.Add('*.*');           // Testear todos los ficheros
          // IMPORTANT: In this release, you must test all files.
          zMaster.Extract;

          // Todo Correcto
          outFileName := zFileName;
          Result := 0;
        except
          // error durante la compresi�n
          BIlog('Error al comprimir.');
          Result := -2;
        end;
     finally
       FreeAndNil(zMaster);
     end;
end;

function  GetAppVersion:string;
var
 Size, Size2: DWord;
 Pt, Pt2: Pointer;
begin
     Size := GetFileVersionInfoSize(PChar (ParamStr (0)), Size2);
     if Size > 0 then
     begin
          GetMem (Pt, Size);
          try
             GetFileVersionInfo (PChar (ParamStr (0)), 0, Size, Pt);
             VerQueryValue (Pt, '\', Pt2, Size2);
             with TVSFixedFileInfo (Pt2^) do
             begin
                  Result:= 'BI '+
                           IntToStr (HiWord (dwFileVersionMS)) + '.' +
                           IntToStr (LoWord (dwFileVersionMS)) + '.' +
                           IntToStr (HiWord (dwFileVersionLS)) + '.' +
                           IntToStr (LoWord (dwFileVersionLS));
             end;
          finally
            FreeMem (Pt);
          end;
     end;
end;

procedure BILog(Mensaje: String);
var
  F: TextFile;
  Filename: String;
  Mutex: THandle;
  SearchRec: TSearchRec;
begin
     // Insertamos la fecha y la hora
     Mensaje:= FormatDateTime('[yyyy ddd dd mmm hh:nn] ', Now) + Mensaje;
     // El nombre del archivo es igual al del ejecutable, pero con la extension .log
     //Filename:= ChangeFileExt(ParamStr(0),'.log');
     Filename:= ChangeFileExt(DIR_IMS+ExtractFileName(ParamStr(0)),'.log');
     // Creamos un mutex, usando como identificador unico la ruta completa del ejecutable
     //Mutex:= CreateMutex(nil,FALSE, PChar(StringReplace(ParamStr(0),'\','/',[rfReplaceAll])));
     Mutex:= CreateMutex(nil,FALSE, PChar(StringReplace(DIR_IMS+ExtractFileName(ParamStr(0)),'\','/',[rfReplaceAll])));
     if Mutex <> 0 then
     begin
          // Esperamos nuestro turno para escribir
          WaitForSingleObject(Mutex, INFINITE);
          try
             // Comprobamos el tama�o del archivo
             if FindFirst(Filename,faAnyFile,SearchRec) = 0 then
             begin
                  // Si es mayor de un mega lo copiamos a (nombre).log.1
                  if SearchRec.Size > (1024*1024) then
                  begin
                       MoveFileEx(PChar(Filename),PChar(Filename + '.1'), MOVEFILE_REPLACE_EXISTING);
                  end;
                  FindClose(SearchRec);
             end;
             try
                  AssignFile(F, Filename);
                  {$I-}
                    Append(F);
                  if IOResult <> 0 then
                  begin
                       Rewrite(F);
                  end;
                  {$I+}
                  if IOResult = 0 then
                  begin
                       // Escribimos el mensaje
                       Writeln(F, Mensaje);
                       CloseFile(F);
                  end;
             except
               //
             end;
          finally
             ReleaseMutex(Mutex);
             CloseHandle(Mutex);
          end;
     end;
end;

Function dameDatoProveedor(codProv, dato :integer ):string;
var
   valor, codProvC   :String;

begin
     if (codProv >0) and (dato > 0)then
     begin
          try
             codProvC := IntToStr(codProv);
          except
              codProvC := 'J';
          end;
          if (codProvC <> 'J') then
          begin
               try
                  case dato of
                    1: try
                          valor := Eje_SP_Ret('Select VPN_HOST as RETORN From PROVEIDORS_IP Where CODI_PROVEIDOR = ' + QuotedStr(codProvC) + ' and VPN_IP_PRIORITAT = "1" ' );
                       except
                           valor := '';
                       end;
                    2: try
                          valor := Eje_SP_Ret('Select VPN_HOST as RETORN From PROVEIDORS_IP Where CODI_PROVEIDOR = ' + QuotedStr(codProvC) + ' and VPN_IP_PRIORITAT = "2" ' );
                       except
                          valor := '';
                       end;
                    3: try
                          valor := Eje_SP_Ret('Select CIF as RETORN From PROVEIDORS_MESTRE Where CODI_PROVEIDOR  = ' + QuotedStr(codProvC) );
                       except
                          valor := '';
                       end;
                  end;
               except
                  valor := '';
               end
          end;


          result := copy(trim(valor), 1, 40);
     end
     else
     begin
      result := '';
     end;


end;

end.
