object BI_Form1: TBI_Form1
  Left = 475
  Top = 437
  Align = alCustom
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  BorderStyle = bsSingle
  Caption = 'BI Fedintia Client'
  ClientHeight = 115
  ClientWidth = 546
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 546
    Height = 115
    Align = alClient
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clWhite
    TabOrder = 0
    object LB_Proceso: TLabel
      Left = 10
      Top = 55
      Width = 439
      Height = 22
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbEntorno: TLabel
      Left = 10
      Top = 6
      Width = 439
      Height = 22
      Alignment = taCenter
      AutoSize = False
      Caption = 'Ejecutando en producci'#243'n'
      Color = 14024703
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lbEnviara: TLabel
      Left = 10
      Top = 29
      Width = 439
      Height = 22
      Alignment = taCenter
      AutoSize = False
      Caption = 'Se enviar'#225' facturaci'#243'n'
      Color = 14024703
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object B_Day: TButton
      Left = 7
      Top = 84
      Width = 135
      Height = 23
      Hint = 'Envio de BI y MA'
      Caption = 'Iniciar BI + MA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = B_DayClick
    end
    object PB_BI: TProgressBar
      Left = 481
      Top = 4
      Width = 26
      Height = 103
      Max = 3
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 1
    end
    object PB_FTP: TProgressBar
      Left = 509
      Top = 4
      Width = 26
      Height = 103
      Max = 3
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 2
    end
    object B_Manual: TButton
      Left = 152
      Top = 84
      Width = 135
      Height = 23
      Hint = 'Env'#237'o del fichero indicado por fecha'
      Caption = 'BI Manual'
      TabOrder = 3
      OnClick = B_ManualClick
    end
    object dManual: TDateEdit
      Left = 294
      Top = 84
      Width = 91
      Height = 23
      NumGlyphs = 2
      TabOrder = 4
    end
    object PB_BICHECK: TProgressBar
      Left = 453
      Top = 4
      Width = 26
      Height = 103
      Max = 3
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 5
    end
    object LB_Proceso02: TStaticText
      Left = 10
      Top = 55
      Width = 439
      Height = 22
      AutoSize = False
      BorderStyle = sbsSingle
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
  end
  object btnBICheck: TButton
    Left = 129
    Top = 54
    Width = 201
    Height = 25
    Caption = 'Check cont.lineas FBD = cont.lineas BI'
    TabOrder = 1
    Visible = False
    OnClick = btnBICheckClick
  end
  object MenuBandeja: TPopupMenu
    Left = 433
    Top = 19
    object Mostrar: TMenuItem
      Caption = 'Mostrar'
      OnClick = MostrarClick
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 406
    Top = 19
  end
end
