unit FTP1;

interface

uses Classes, UDF, Forms, IdFTP, SysUtils, Windows, Dialogs, StrUtils,
      //SERGIO AGUADO, 12/05/2021. PARA CALCULAR EL MD5 CHECKSUM DE UN FICHERO
      IdHashMessageDigest, idHash,
      //PARA HTTP
      idMultipartFormData, IdHTTP, {IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,}
      //SERGIO AGUADO, 12/05/2021 PARA EL TEMA SSL (https)
      IdSSLOpenSSL
      ;

resourcestring
  ErrorEnElEnvio                   = 'Error en el envio del fichero';
  NosehapodidoestablecerlaConexion = 'No se ha podido realizar la conexion con el servidor FTP';
  NosehapodidoenviarficheroFIN     = 'No se ha podido enviar el fichero .FIN, pero si el fichero de datos por HTTTPS';
  AVISO                            = 'Aviso';
  FicheroSubidoNoCoincideTamano    = 'El fichero <%s> no se ha subido correctamente. Tama�o origen <%s> / Tama�o subido <%s>';

const
  ddmmyyyyhhssmm = 'dd.mm.yyyy hh:mm:ss';
  ERROR_ENVIO_FTP_500 = 500;

  //SERGIO AGUADO, 19/05/2021. Envios por HTTPS.
  ENVIOS_HTTPS_RESPUESTA_TAG_CODIGO = '{"status":'; //'{"codigo":';
  ENVIOS_HTTPS_RESPUESTA_TAG_ERROR = '{"status":'; //'{"error":';
  ENVIOS_HTTPS_RESPUESTA_TAG_DESCRIPCION = ', "descripcion"';
  ENVIOS_HTTPS_ENVIO_NOM_CAMPO_FICHERO =  'fichero';
  ENVIOS_HTTPS_ENVIO_NOM_CAMPO_LINEASFICHERO = 'ficheronlineas';
  ENVIOS_HTTPS_ENVIO_NOM_CAMPO_IMPORTE = 'totalimporte';
  ENVIOS_HTTPS_ENVIO_NOM_CAMPO_MD5 = 'ficheromd5';
  //SERGIO, 01/07/2021, AHORA LA URL LA TENEMOS EN BI_CONTROL.BI_FTP_ADRECA
  //ENVIOS_HTTPS_HOST = 'https://taf.fedefarma.com/bisualfarma/enviaventas';


function FTP_FEDE_SEND(NomFitxer: string; BI: Boolean = True): Boolean;
//SERGIO AGUADO, 19/05/2021. Envios por HTTPS.
function HTTPS_FEDE_SEND( NomFitxer:string; intContRegistros:integer; douImporteVentas:double ): boolean;
function TamanoFichero(sFileToExamine: string): Integer;


implementation

uses BI_DM, BI_Generator, INI_Read, BI, MA_GENERATOR, BI_Functions;

function FTP_FEDE_SEND(NomFitxer: string; BI: Boolean = True): Boolean;
var
  List1, Fin: TStringList;
  nIntentos, nIntentosMax, nComprobaciones, nComprobacionesMax, FileSizeLocal, FileSizeFTP: Integer;
  File_FIN, DirectoriDesti, FileFTP: string;
  lEnviado, bMismoTamano: Boolean;

  //Dani 02/07/2018 Averiguamos el tama�o del fichero local
  function TamanoFichero(sFileToExamine: string): Integer;
  var
    SearchRec: TSearchRec;
    sgPath: string;
    inRetval, I1: Integer;
  begin
    sgPath := ExpandFileName(sFileToExamine);
    try
      inRetval := FindFirst(ExpandFileName(sFileToExamine), faAnyFile, SearchRec);
      if inRetval = 0 then
        I1 := SearchRec.Size
      else
        I1 := -1;
    finally
      SysUtils.FindClose(SearchRec);
    end;
    Result := I1;
  end;

begin
     BI_DM.DataModule1.IBTS_BI.Close;
     BI_DM.DataModule1.IBTS_BI.Open;

     DataModule1.IBTS_IMS.Close;
     DataModule1.IBTS_IMS.Open;

     if (BI) then
     begin
          DataModule1.IdFTP1.Host         := BI_DM.DataModule1.IBTS_BIBI_FTP_ADRECA.AsString;
          DataModule1.IdFTP1.Port         := BI_DM.DataModule1.IBTS_BIBI_FTP_PORT.AsInteger;
          //SERGIO, Migraciion D7
          DataModule1.IdFTP1.Username         := RTrim(BI_DM.DataModule1.IBTS_BIBI_FTP_USUARI.AsString);
          DataModule1.IdFTP1.Password     := RTrim(BI_DM.DataModule1.IBTS_BIBI_FTP_USUARI_PASSW.AsString);
          DirectoriDesti                  := AllTrim(BI_DM.DataModule1.IBTS_BIBI_DIRECTORI_DESTI.AsString);
     end
     else
     begin
          DataModule1.IdFTP1.Host         := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_ADRECA').AsString);
          DataModule1.IdFTP1.Port         := DataModule1.IBTS_IMS.FieldByName('IMS_FTP_PORT').AsInteger;
          //SERGIO, Migraciion D7
          DataModule1.IdFTP1.Username         := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_USUARI').AsString);
          DataModule1.IdFTP1.Password     := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_USUARI_PASSW').AsString);
          DirectoriDesti                  := AllTrim(DataModule1.IBTS_IMS.FieldByName('IMS_DIRECTORI_DESTI').AsString);
     end;

     if (Trim(DirectoriDesti) <> '') then
     begin
          if (Copy(DirectoriDesti, Length(DirectoriDesti),1) <> '/') then
             DirectoriDesti := DirectoriDesti + '/';
     end;

     lEnviado :=False;

     Application.ProcessMessages;
     nIntentosMax := 2;
     nIntentos    := 0;
     //Dani 02/07/2018 N�mero de comprobaciones para el tama�o del fichero subido y local
     nComprobacionesMax := 10;
     nComprobaciones := 0;
     bMismoTamano := False;
     Result := False;

     while ( (nIntentos < nIntentosMax) and (not lEnviado) ) do
     begin
          Inc(nIntentos);
          BI_FORM1.PB_FTP.Position := 1;

          DataModule1.IdFTP1.Disconnect;
          try
             DataModule1.IdFTP1.Connect;
          except
                on E: Exception do
                begin
                     BILog(E.Message);
                     BI_Generator.lHayError := True;
                     MA_Generator.lHayError := True;
                     Result:= False;
                end;
          end;

          //Opcion Desarrollo, 15/02/2017 Deshabilitado por sistemas
//          if IsDeveloper then
//          begin
//               DataModule1.IdFTP1.ChangeDir('/Test/');
//          end;

          if DataModule1.IdFTP1.Connected then
          begin
               try   //Envio
                  ActualizaInformacion(NomFitxer + ' Enviando');


                  BI_FORM1.PB_FTP.StepIt;
//                  DataModule1.IdFTP1.put(DirNomFitxerBI, AllTrim(BI_DM.DataModule1.FIBTS_BIBI_DIRECTORI_DESTI.AsString)+ NomFitxer, False);
//                  DataModule1.IdFTP1.put(NomFitxer , DirectoriDesti+ ExtractFileName(NomFitxer), False);

                  //Dani 02/07/2018 Se comprueba el tama�o del fichero local y la del subido, tenemos 10 intentos, si no es igual abortamos
                  while ((nComprobaciones < nComprobacionesMax) and (not bMismoTamano)) do
                  begin
                       DataModule1.IdFTP1.Put(NomFitxer, DirectoriDesti + ExtractFileName(NomFitxer), False);

                       FileSizeLocal := TamanoFichero(NomFitxer);
                       FileFTP := Trim(DirectoriDesti) + ExtractFileName(NomFitxer);
                       FileSizeFTP := DataModule1.IdFTP1.Size(FileFTP);

                       bMismoTamano := (FileSizeFTP >= FileSizeLocal);
                       Inc(nComprobaciones);
                  end;


                  if bMismoTamano then
                  begin
                       NomFitxer := AnsiReplaceStr(NomFitxer, '.TXT', '.FIN' );
                       File_FIN := AnsiReplaceStr(NomFitxer, '.ZIP', '.FIN' );

                       Fin  := TStringList.Create;
                       Fin.Add(infoPrograma + ' ' + File_FIN + '  ' + FormatDateTime(ddmmyyyyhhssmm ,now)); //infoPrograma
                       Fin.SaveToFile(File_FIN);

                       nComprobaciones := 0;
                       bMismoTamano := False;

                       //Dani 02/07/2018 Se comprueba el tama�o del fichero local y la del subido, tenemos 10 intentos, si no es igual abortamos
                       while ((nComprobaciones < nComprobacionesMax) and (not bMismoTamano)) do
                       begin
                            DataModule1.IdFTP1.Put(File_FIN, DirectoriDesti + ExtractFileName(File_FIN), False);

                            FileSizeLocal := TamanoFichero(File_FIN);
                            FileFTP := Trim(DirectoriDesti) + ExtractFileName(File_FIN);
                            FileSizeFTP := DataModule1.IdFTP1.Size(FileFTP);

                            bMismoTamano := (FileSizeFTP >= FileSizeLocal);
                            Inc(nComprobaciones);
                       end;

                       if bMismoTamano then
                       begin
                            lEnviado := True;
                            Result   := True;

                            BI_FORM1.PB_FTP.StepIt;
                            Fin.Free;

                            BI_FORM1.PB_FTP.Position := 0;
                            ActualizaInformacion(NomFitxer + ' Enviado');
                       end
                       else
                       begin
                            BILog('Error en el env�o: ' + ErrorEnElEnvio + sLineBreak + Format(FicheroSubidoNoCoincideTamano, [ExtractFileName(File_FIN), IntToStr(FileSizeLocal), IntToStr(FileSizeFTP)]));
                            BILog('Fichero error: ' + ExtractFileName(File_FIN));
                       end;
                  end
                  else
                  begin
                       BILog('Error en el env�o: ' + ErrorEnElEnvio + sLineBreak + Format(FicheroSubidoNoCoincideTamano, [ExtractFileName(NomFitxer), IntToStr(FileSizeLocal), IntToStr(FileSizeFTP)]));
                       BILog('Fichero error: ' + ExtractFileName(NomFitxer));
                  end;
               except
                  on E:Exception do
                  begin
                       BILog(E.Message);
                       BI_Generator.lHayError := True;
                       MA_Generator.lHayError := True;
                       Result:= False;
                  end;
               end;
          end
          else
          begin
               BILog(NosehapodidoestablecerlaConexion);
               Result := False;
               BI_Generator.lHayError := True;
               MA_Generator.lHayError := True;
          end;
     end;

     try
        DataModule1.IdFTP1.Disconnect;
     except
     end;
     Application.ProcessMessages;

     //CIERRO
     BI_DM.DataModule1.IBTS_BI.Close;
end;



//SERGIO AGUADO, 12/05/2021. PARA ENVIAR EL FICHERO BI POR HTTPS Y NO POR FTP.
function HTTPS_FEDE_SEND( NomFitxer:string; intContRegistros:integer; douImporteVentas:double ): boolean;
var
   strMD5,respuestaEnvio,codRespuestaEnvio,descRespuestaEnvio: string;
   Fin: TStringList;
   nIntentos, nIntentosMax, nComprobaciones, nComprobacionesMax, FileSizeLocal,FileSizeFTP: integer;
   File_FIN, DirectoriDesti, FileFTP, host_envio_HTTPS: string;
   lEnviado, bMismoTamano: boolean;

    //SERGIO AGUADO, 12/05/2021. PARA CALCULAR EL MD5 CHECKSUM DE UN FICHERO.
    function DameMD5fichero( fichero:string ):string;
    var
      idmd5 : TIdHashMessageDigest5;
      fs : TFileStream;
      hash : T4x4LongWordRecord;
    begin
       try
         idmd5 := TIdHashMessageDigest5.Create;
         fs := TFileStream.Create(fichero, fmOpenRead OR fmShareDenyWrite) ;
         try
           result := lowercase( idmd5.AsHex(idmd5.HashValue(fs)) ) ;
         finally
           fs.Free;
           idmd5.Free;
         end;
       except
         on e:exception do
         begin
           raise exception.create('Error en DameMD5fichero(). '+e.message);
         end;
       end;
    end;

    //SERGIO AGUADOO, 12/05/2021. FUNCION PARA ENVIAR EL FICHERO POR HTTPS
    function EnvioPorHTTPS( strHost,strFichero:string; lineas:integer; importeVentas:double; strMD5fichero:string ): string;
    var
      datos: TIdMultiPartFormDataStream;
      LHandler: TIdSSLIOHandlerSocket;
      IdHTTP1: TidHTTP;
    begin
      result := '';
      try
        IdHTTP1 := TIdHTTP.create(nil);
        try
          datos := TIdMultiPartFormDataStream.Create;
          try
            LHandler := TIdSSLIOHandlerSocket.Create(nil);
            try
              LHandler.SSLOptions.Method := sslvSSLv23;
              LHandler.SSLOptions.Mode   := sslmUnassigned;
              IdHTTP1.IOHandler := LHandler;
              datos.AddFile(ENVIOS_HTTPS_ENVIO_NOM_CAMPO_FICHERO,strFichero,'');
              datos.AddFormField(ENVIOS_HTTPS_ENVIO_NOM_CAMPO_LINEASFICHERO, intToStr(lineas));
              datos.AddFormField(ENVIOS_HTTPS_ENVIO_NOM_CAMPO_IMPORTE, FormatFloat('000000.00',importeVentas));
              datos.AddFormField(ENVIOS_HTTPS_ENVIO_NOM_CAMPO_MD5, strMD5fichero);
              result := IdHTTP1.Post(strHost, datos);
            finally
              LHandler.Free;
              result := result + ' RESPONSE: CODE:'+intToStr(idhttp1.ResponseCode)+' TEXT:'+idhttp1.ResponseText;
            end;
          finally
            datos.Free;
          end;
        finally
          idHTTP1.Free;
        end;
      except
        on e:exception do
        begin
          raise exception.create('Error en EnvioPorHTTPS() '+e.message);
        end;
      end;
    end;

    //SERGIO AGUADO, 13/05/2021, EXTRAIGO CODIGO Y TEXTO DEL ERROR QUE NOS DEVUELVA https://taf.fedefarma.com/bisualfarma/enviaventas
    procedure ParseoError_BisualfarmaEnviaventas( respuesta:string; var codi:string; var desc:string );
    var posCodigo,posDescripcion: integer;
    begin
      try
        //'OK'  =>  OK, no error
        //'ERR-010'  =>  MD5 no coinciden
        //'ERR-012'  =>  Tipo de fichero no admitido
        posDescripcion := pos(ENVIOS_HTTPS_RESPUESTA_TAG_DESCRIPCION,respuesta);

        posCodigo := pos(ENVIOS_HTTPS_RESPUESTA_TAG_CODIGO,respuesta);
        if posCodigo=0 then posCodigo := pos(ENVIOS_HTTPS_RESPUESTA_TAG_ERROR,respuesta);

        Respuesta := rightStr(respuesta,length(respuesta)-posCodigo);
        desc := copy( respuesta, pos( ',', respuesta)+1, length(respuesta) );
        codi := copy( respuesta, 1, pos( ',', respuesta)-1);
        codi := copy( codi, pos( ':', codi )+1, length(codi) );
        desc := copy( desc, pos( ':', desc )+1, length(desc) );
        codi := trim( stringReplace( StringReplace( codi, '"', '', [rfReplaceAll]), '}', '', [rfReplaceAll]) );
        desc := trim( stringReplace( StringReplace( desc, '"', '', [rfReplaceAll]), '}', '', [rfReplaceAll]) );
      except
        on e:exception do
        begin
          raise exception.create('Error en ParseError_BisualfarmaEnviaventas(). '+e.message);
        end;
      end;
    end;

begin
     //ESTO ES SOLO PARA BI.
     //PARA IMS CAMBIAN LOS DATOS DE CONEXI�N AL FTP
     //NECESITAMOS FTP PARA ENVIAR EL FICHERO .FIN
     DataModule1.IdFTP1.Host         := BI_DM.DataModule1.IBTS_BIBI_FTP_ADRECA.AsString;
     DataModule1.IdFTP1.Port         := BI_DM.DataModule1.IBTS_BIBI_FTP_PORT.AsInteger;
     //SERGIO, Migraciion D7
     DataModule1.IdFTP1.Username     := RTrim(BI_DM.DataModule1.IBTS_BIBI_FTP_USUARI.AsString);
     DataModule1.IdFTP1.Password     := RTrim(BI_DM.DataModule1.IBTS_BIBI_FTP_USUARI_PASSW.AsString);
     DirectoriDesti                  := AllTrim(BI_DM.DataModule1.IBTS_BIBI_DIRECTORI_DESTI.AsString);

     (* ESTO SERIA PARA IMS
     DataModule1.IdFTP1.Host         := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_ADRECA').AsString);
     DataModule1.IdFTP1.Port         := DataModule1.IBTS_IMS.FieldByName('IMS_FTP_PORT').AsInteger;
     //SERGIO, Migraciion D7
     DataModule1.IdFTP1.Username     := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_USUARI').AsString);
     DataModule1.IdFTP1.Password     := Trim(DataModule1.IBTS_IMS.FieldByName('IMS_FTP_USUARI_PASSW').AsString);
     DirectoriDesti                  := AllTrim(DataModule1.IBTS_IMS.FieldByName('IMS_DIRECTORI_DESTI').AsString);
     *)

     host_envio_HTTPS := BI_DM.DataModule1.IBTS_BIBI_FTP_ADRECA.AsString;

     if (Trim(DirectoriDesti) <> '') then
     begin
          if (Copy(DirectoriDesti, Length(DirectoriDesti),1) <> '/') then
             DirectoriDesti := DirectoriDesti + '/';
     end;

     Application.ProcessMessages;
     nIntentosMax := 10;
     nIntentos    := 0;
     Result := False;

     //SERGIO AGUADO, 12/05/2021. AQUI ES DONDE ENVIO EL FICHERO BI POR HTTPS
     ActualizaInformacion(NomFitxer + ' Enviando');
     BI_FORM1.PB_FTP.StepIt;
     try
        strMD5 := DameMD5fichero( NomFitxer );
        while ((nIntentos < nIntentosMax) and (not result)) do
        begin
          BILog('** ENVIO POR HTTPS ** Enviando fichero '+NomFitxer);
          respuestaEnvio := EnvioPorHTTPS( host_envio_HTTPS{ENVIOS_HTTPS_HOST}, NomFitxer, intContRegistros, douImporteVentas, strMD5 );
          ParseoError_BisualfarmaEnviaventas( respuestaEnvio, codRespuestaEnvio, descRespuestaEnvio );
          BILog('** ENVIO POR HTTPS ** '+CodRespuestaEnvio+' - '+descRespuestaEnvio);
          result := sameText(codRespuestaEnvio,'OK') and (pos('RESPONSE: CODE:200',descRespuestaEnvio)>0);
          Inc(nIntentos);
        end;
     except
        on e:exception do
        begin
          BILog('** ENVIO POR HTTPS ** Error envio '+e.message);
        end;
     end;

     //SERGIO AGUADO, 12/05/2021. AHORA TOCARIA ENVIAR EL FICHERO .FIN (por FTP)
     //SI ESTA PARTE FALLA NO ES IMPORTANTE PORQUE SOLO ES EL FICHERO .FIN
     try
        if result and ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP then
        begin

           //BILog('** ENVIO POR HTTPS ** Envio correcto');
           lEnviado := false;
           nIntentosMax := 2;
           nIntentos    := 0;
           //N�mero de comprobaciones para el tama�o del fichero subido y local
           nComprobacionesMax := 10;
           nComprobaciones := 0;
           bMismoTamano := False;

           BILog('** ENVIO POR HTTPS ** Intentando enviar fichero .FIN (por ftp)');
           while ( (nIntentos < nIntentosMax) and (not lEnviado) ) do
           begin
                Inc(nIntentos);
                BI_FORM1.PB_FTP.Position := 1;

                DataModule1.IdFTP1.Disconnect;
                try
                   DataModule1.IdFTP1.Connect;
                except
                   on E: Exception do
                   begin
                      BILog(E.Message);
                      //BI_Generator.lHayError := True;
                      //MA_Generator.lHayError := True;
                      //Result:= False;
                   end;
                end;

                if DataModule1.IdFTP1.Connected then
                begin
                     try   //Envio archivo .FIN
                        NomFitxer := AnsiReplaceStr(NomFitxer, '.TXT', '.FIN' );
                        File_FIN := AnsiReplaceStr(NomFitxer, '.ZIP', '.FIN' );

                        Fin  := TStringList.Create;
                        Fin.Add(infoPrograma + ' ' + File_FIN + '  ' + FormatDateTime(ddmmyyyyhhssmm ,now)); //infoPrograma
                        Fin.Add( 'RESPUESTA DEL ENVIO HTTPS: ' );
                        Fin.Add( RespuestaEnvio );
                        Fin.SaveToFile(File_FIN);

                        nComprobaciones := 0;
                        bMismoTamano := False;

                        while ((nComprobaciones < nComprobacionesMax) and (not bMismoTamano)) do
                        begin
                             BILog('** ENVIO POR HTTPS ** Enviando '+File_FIN);

                             DataModule1.IdFTP1.Put(File_FIN, DirectoriDesti + ExtractFileName(File_FIN), False);

                             FileSizeLocal := TamanoFichero(File_FIN);
                             FileFTP := Trim(DirectoriDesti) + ExtractFileName(File_FIN);
                             FileSizeFTP := DataModule1.IdFTP1.Size(FileFTP);

                             bMismoTamano := (FileSizeFTP >= FileSizeLocal);
                             Inc(nComprobaciones);
                        end;

                        if bMismoTamano then
                        begin
                             lEnviado := True;

                             BI_FORM1.PB_FTP.StepIt;
                             Fin.Free;

                             BI_FORM1.PB_FTP.Position := 0;
                             ActualizaInformacion(NomFitxer + ' Enviado');
                             BILog('** ENVIO POR HTTPS ** Fichero enviado: ' + ExtractFileName(File_FIN));
                        end
                        else
                        begin
                             BILog('** ENVIO POR HTTPS ** Error en el env�o: ' + ErrorEnElEnvio + sLineBreak + Format(FicheroSubidoNoCoincideTamano, [ExtractFileName(File_FIN), IntToStr(FileSizeLocal), IntToStr(FileSizeFTP)]));
                             BILog('** ENVIO POR HTTPS ** Fichero error: ' + ExtractFileName(File_FIN));
                        end;

                     except
                        on E:Exception do
                        begin
                             BILog('** ENVIO POR HTTPS ** '+E.Message);
                             //BI_Generator.lHayError := True;
                             //MA_Generator.lHayError := True;
                             //Result:= False;
                        end;
                     end;
                end
                else
                begin
                     BILog(NosehapodidoenviarficheroFIN);
                end;
           end; //end while intentos envio fichero .FIN
        end
        else
        begin
           if (not ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP) and result then BILog('** ENVIO POR HTTPS ** No se envian ficheros .FIN. Constante  ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP=False !!!');
           if not result then BILog('** ENVIO POR HTTPS ** Envio FALLIDO !!!');
        end; //fin IF envio HTTPS correcto
     except //try-except ficheros .FIN. No elevo la excepcion porque no es importante, lo registro en log y sigo para delante.
        on e:exception do
        begin
           BILog(E.Message);
        end;
     end;
end;


//Dani 02/07/2018 Averiguamos el tama�o del fichero local
function TamanoFichero(sFileToExamine: string): Integer;
var
  SearchRec: TSearchRec;
  sgPath: string;
  inRetval, I1: Integer;
begin
  sgPath := ExpandFileName(sFileToExamine);
  try
    inRetval := FindFirst(ExpandFileName(sFileToExamine), faAnyFile, SearchRec);
    if inRetval = 0 then
      I1 := SearchRec.Size
    else
      I1 := -1;
  finally
    SysUtils.FindClose(SearchRec);
  end;
  Result := I1;
end;



end.
