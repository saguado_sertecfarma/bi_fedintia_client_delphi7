
{*******************************************************}
{                                                       }
{                   XML Data Binding                    }
{                                                       }
{         Generated on: 24/05/2021 11:41:31             }
{       Generated from: C:\Respuesta_Consulta_WEB.XML   }
{   Settings stored in: C:\Respuesta_Consulta_WEB.xdb   }
{                                                       }
{*******************************************************}

unit IRespuestaConsultaWEB;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRespuestaSOAP = interface;
  IXMLSqlinfoType = interface;
  IXMLSqltablesType = interface;
  IXMLSqltableType = interface;
  IXMLMetadataType = interface;
  IXMLColumnType = interface;
  IXMLRowsetType = interface;
  IXMLRType = interface;

{ IXMLRespuestaSOAP }

  IXMLRespuestaSOAP = interface(IXMLNode)
    ['{E7AB1C7F-6DF2-4225-BC04-8055CE79C0C3}']
    { Property Accessors }
    function Get_Country: WideString;
    function Get_Date: WideString;
    function Get_Dateformat: WideString;
    function Get_Dbms: WideString;
    function Get_From: WideString;
    function Get_Host: WideString;
    function Get_Lang: WideString;
    function Get_Numberformat: WideString;
    function Get_Rows: Integer;
    function Get_Test: WideString;
    function Get_Timeformat: WideString;
    function Get_Timestampformat: WideString;
    function Get_Type_: WideString;
    function Get_Sqlinfo: IXMLSqlinfoType;
    function Get_Metadata: IXMLMetadataType;
    function Get_Rowset: IXMLRowsetType;
    procedure Set_Country(Value: WideString);
    procedure Set_Date(Value: WideString);
    procedure Set_Dateformat(Value: WideString);
    procedure Set_Dbms(Value: WideString);
    procedure Set_From(Value: WideString);
    procedure Set_Host(Value: WideString);
    procedure Set_Lang(Value: WideString);
    procedure Set_Numberformat(Value: WideString);
    procedure Set_Rows(Value: Integer);
    procedure Set_Test(Value: WideString);
    procedure Set_Timeformat(Value: WideString);
    procedure Set_Timestampformat(Value: WideString);
    procedure Set_Type_(Value: WideString);
    { Methods & Properties }
    property Country: WideString read Get_Country write Set_Country;
    property Date: WideString read Get_Date write Set_Date;
    property Dateformat: WideString read Get_Dateformat write Set_Dateformat;
    property Dbms: WideString read Get_Dbms write Set_Dbms;
    property From: WideString read Get_From write Set_From;
    property Host: WideString read Get_Host write Set_Host;
    property Lang: WideString read Get_Lang write Set_Lang;
    property Numberformat: WideString read Get_Numberformat write Set_Numberformat;
    property Rows: Integer read Get_Rows write Set_Rows;
    property Test: WideString read Get_Test write Set_Test;
    property Timeformat: WideString read Get_Timeformat write Set_Timeformat;
    property Timestampformat: WideString read Get_Timestampformat write Set_Timestampformat;
    property Type_: WideString read Get_Type_ write Set_Type_;
    property Sqlinfo: IXMLSqlinfoType read Get_Sqlinfo;
    property Metadata: IXMLMetadataType read Get_Metadata;
    property Rowset: IXMLRowsetType read Get_Rowset;
  end;

{ IXMLSqlinfoType }

  IXMLSqlinfoType = interface(IXMLNode)
    ['{FE54C381-56FF-47CD-90B5-38DEBCAF33C4}']
    { Property Accessors }
    function Get_Stdout: WideString;
    function Get_Stderr: WideString;
    function Get_Sqlstmt: WideString;
    function Get_Sqlsort: WideString;
    function Get_Sqluser: WideString;
    function Get_Sqldbms: WideString;
    function Get_Sqlengine: WideString;
    function Get_Sqllang: WideString;
    function Get_Sqltype: WideString;
    function Get_Sqltime: Integer;
    function Get_Sqlcount: Integer;
    function Get_Sqlserial: Integer;
    function Get_Sqlmaxrows: Integer;
    function Get_Sqltables: IXMLSqltablesType;
    procedure Set_Stdout(Value: WideString);
    procedure Set_Stderr(Value: WideString);
    procedure Set_Sqlstmt(Value: WideString);
    procedure Set_Sqlsort(Value: WideString);
    procedure Set_Sqluser(Value: WideString);
    procedure Set_Sqldbms(Value: WideString);
    procedure Set_Sqlengine(Value: WideString);
    procedure Set_Sqllang(Value: WideString);
    procedure Set_Sqltype(Value: WideString);
    procedure Set_Sqltime(Value: Integer);
    procedure Set_Sqlcount(Value: Integer);
    procedure Set_Sqlserial(Value: Integer);
    procedure Set_Sqlmaxrows(Value: Integer);
    { Methods & Properties }
    property Stdout: WideString read Get_Stdout write Set_Stdout;
    property Stderr: WideString read Get_Stderr write Set_Stderr;
    property Sqlstmt: WideString read Get_Sqlstmt write Set_Sqlstmt;
    property Sqlsort: WideString read Get_Sqlsort write Set_Sqlsort;
    property Sqluser: WideString read Get_Sqluser write Set_Sqluser;
    property Sqldbms: WideString read Get_Sqldbms write Set_Sqldbms;
    property Sqlengine: WideString read Get_Sqlengine write Set_Sqlengine;
    property Sqllang: WideString read Get_Sqllang write Set_Sqllang;
    property Sqltype: WideString read Get_Sqltype write Set_Sqltype;
    property Sqltime: Integer read Get_Sqltime write Set_Sqltime;
    property Sqlcount: Integer read Get_Sqlcount write Set_Sqlcount;
    property Sqlserial: Integer read Get_Sqlserial write Set_Sqlserial;
    property Sqlmaxrows: Integer read Get_Sqlmaxrows write Set_Sqlmaxrows;
    property Sqltables: IXMLSqltablesType read Get_Sqltables;
  end;

{ IXMLSqltablesType }

  IXMLSqltablesType = interface(IXMLNode)
    ['{F9F1A097-485B-4070-8408-00C595F8CADD}']
    { Property Accessors }
    function Get_Sqltable: IXMLSqltableType;
    { Methods & Properties }
    property Sqltable: IXMLSqltableType read Get_Sqltable;
  end;

{ IXMLSqltableType }

  IXMLSqltableType = interface(IXMLNode)
    ['{910340EE-B670-459F-9FAA-9036344A336D}']
    { Property Accessors }
    function Get_Bestrowid: WideString;
    function Get_Delete: WideString;
    function Get_Insert: WideString;
    function Get_Table: WideString;
    function Get_Update: WideString;
    procedure Set_Bestrowid(Value: WideString);
    procedure Set_Delete(Value: WideString);
    procedure Set_Insert(Value: WideString);
    procedure Set_Table(Value: WideString);
    procedure Set_Update(Value: WideString);
    { Methods & Properties }
    property Bestrowid: WideString read Get_Bestrowid write Set_Bestrowid;
    property Delete: WideString read Get_Delete write Set_Delete;
    property Insert: WideString read Get_Insert write Set_Insert;
    property Table: WideString read Get_Table write Set_Table;
    property Update: WideString read Get_Update write Set_Update;
  end;

{ IXMLMetadataType }

  IXMLMetadataType = interface(IXMLNodeCollection)
    ['{B3E49770-2542-4DEA-9432-44FAA6D05A3C}']
    { Property Accessors }
    function Get_Column(Index: Integer): IXMLColumnType;
    { Methods & Properties }
    function Add: IXMLColumnType;
    function Insert(const Index: Integer): IXMLColumnType;
    property Column[Index: Integer]: IXMLColumnType read Get_Column; default;
  end;

{ IXMLColumnType }

  IXMLColumnType = interface(IXMLNode)
    ['{4311152E-F87F-45DD-83FB-623A68886B7D}']
    { Property Accessors }
    function Get_CatalogName: WideString;
    function Get_ColumnClassName: WideString;
    function Get_ColumnDisplaySize: Integer;
    function Get_ColumnLabel: WideString;
    function Get_ColumnName: WideString;
    function Get_ColumnType: Integer;
    function Get_ColumnTypeName: WideString;
    function Get_IsAutoIncrement: WideString;
    function Get_IsCaseSensitive: WideString;
    function Get_IsCurrency: WideString;
    function Get_IsDefinitelyWritable: WideString;
    function Get_IsHidden: WideString;
    function Get_IsNullable: Integer;
    function Get_IsNumeric: WideString;
    function Get_IsPercent: WideString;
    function Get_IsReadOnly: WideString;
    function Get_IsRowid: WideString;
    function Get_IsSigned: WideString;
    function Get_IsTotal: WideString;
    function Get_IsWritable: WideString;
    function Get_Precision: Integer;
    function Get_Scale: Integer;
    function Get_SchemaName: WideString;
    function Get_TableName: WideString;
    procedure Set_CatalogName(Value: WideString);
    procedure Set_ColumnClassName(Value: WideString);
    procedure Set_ColumnDisplaySize(Value: Integer);
    procedure Set_ColumnLabel(Value: WideString);
    procedure Set_ColumnName(Value: WideString);
    procedure Set_ColumnType(Value: Integer);
    procedure Set_ColumnTypeName(Value: WideString);
    procedure Set_IsAutoIncrement(Value: WideString);
    procedure Set_IsCaseSensitive(Value: WideString);
    procedure Set_IsCurrency(Value: WideString);
    procedure Set_IsDefinitelyWritable(Value: WideString);
    procedure Set_IsHidden(Value: WideString);
    procedure Set_IsNullable(Value: Integer);
    procedure Set_IsNumeric(Value: WideString);
    procedure Set_IsPercent(Value: WideString);
    procedure Set_IsReadOnly(Value: WideString);
    procedure Set_IsRowid(Value: WideString);
    procedure Set_IsSigned(Value: WideString);
    procedure Set_IsTotal(Value: WideString);
    procedure Set_IsWritable(Value: WideString);
    procedure Set_Precision(Value: Integer);
    procedure Set_Scale(Value: Integer);
    procedure Set_SchemaName(Value: WideString);
    procedure Set_TableName(Value: WideString);
    { Methods & Properties }
    property CatalogName: WideString read Get_CatalogName write Set_CatalogName;
    property ColumnClassName: WideString read Get_ColumnClassName write Set_ColumnClassName;
    property ColumnDisplaySize: Integer read Get_ColumnDisplaySize write Set_ColumnDisplaySize;
    property ColumnLabel: WideString read Get_ColumnLabel write Set_ColumnLabel;
    property ColumnName: WideString read Get_ColumnName write Set_ColumnName;
    property ColumnType: Integer read Get_ColumnType write Set_ColumnType;
    property ColumnTypeName: WideString read Get_ColumnTypeName write Set_ColumnTypeName;
    property IsAutoIncrement: WideString read Get_IsAutoIncrement write Set_IsAutoIncrement;
    property IsCaseSensitive: WideString read Get_IsCaseSensitive write Set_IsCaseSensitive;
    property IsCurrency: WideString read Get_IsCurrency write Set_IsCurrency;
    property IsDefinitelyWritable: WideString read Get_IsDefinitelyWritable write Set_IsDefinitelyWritable;
    property IsHidden: WideString read Get_IsHidden write Set_IsHidden;
    property IsNullable: Integer read Get_IsNullable write Set_IsNullable;
    property IsNumeric: WideString read Get_IsNumeric write Set_IsNumeric;
    property IsPercent: WideString read Get_IsPercent write Set_IsPercent;
    property IsReadOnly: WideString read Get_IsReadOnly write Set_IsReadOnly;
    property IsRowid: WideString read Get_IsRowid write Set_IsRowid;
    property IsSigned: WideString read Get_IsSigned write Set_IsSigned;
    property IsTotal: WideString read Get_IsTotal write Set_IsTotal;
    property IsWritable: WideString read Get_IsWritable write Set_IsWritable;
    property Precision: Integer read Get_Precision write Set_Precision;
    property Scale: Integer read Get_Scale write Set_Scale;
    property SchemaName: WideString read Get_SchemaName write Set_SchemaName;
    property TableName: WideString read Get_TableName write Set_TableName;
  end;

{ IXMLRowsetType }

  IXMLRowsetType = interface(IXMLNodeCollection)
    ['{C2232F6A-0727-48A9-8EA8-710C7FFA2856}']
    { Property Accessors }
    function Get_Encoding: WideString;
    function Get_Lang: WideString;
    function Get_R(Index: Integer): IXMLRType;
    procedure Set_Encoding(Value: WideString);
    procedure Set_Lang(Value: WideString);
    { Methods & Properties }
    function Add: IXMLRType;
    function Insert(const Index: Integer): IXMLRType;
    property Encoding: WideString read Get_Encoding write Set_Encoding;
    property Lang: WideString read Get_Lang write Set_Lang;
    property R[Index: Integer]: IXMLRType read Get_R; default;
  end;

{ IXMLRType }

  IXMLRType = interface(IXMLNodeCollection)
    ['{5A3509ED-5A93-4E67-8A5F-9DE98A567083}']
    { Property Accessors }
    function Get_C(Index: Integer): Integer;
    { Methods & Properties }
    function Add(const C: Integer): IXMLNode;
    function Insert(const Index: Integer; const C: Integer): IXMLNode;
    property C[Index: Integer]: Integer read Get_C; default;
  end;

{ Forward Decls }

  TXMLSqlresponseType = class;
  TXMLSqlinfoType = class;
  TXMLSqltablesType = class;
  TXMLSqltableType = class;
  TXMLMetadataType = class;
  TXMLColumnType = class;
  TXMLRowsetType = class;
  TXMLRType = class;

{ TXMLSqlresponseType }

  TXMLSqlresponseType = class(TXMLNode, IXMLRespuestaSOAP)
  protected
    { IXMLRespuestaSOAP }
    function Get_Country: WideString;
    function Get_Date: WideString;
    function Get_Dateformat: WideString;
    function Get_Dbms: WideString;
    function Get_From: WideString;
    function Get_Host: WideString;
    function Get_Lang: WideString;
    function Get_Numberformat: WideString;
    function Get_Rows: Integer;
    function Get_Test: WideString;
    function Get_Timeformat: WideString;
    function Get_Timestampformat: WideString;
    function Get_Type_: WideString;
    function Get_Sqlinfo: IXMLSqlinfoType;
    function Get_Metadata: IXMLMetadataType;
    function Get_Rowset: IXMLRowsetType;
    procedure Set_Country(Value: WideString);
    procedure Set_Date(Value: WideString);
    procedure Set_Dateformat(Value: WideString);
    procedure Set_Dbms(Value: WideString);
    procedure Set_From(Value: WideString);
    procedure Set_Host(Value: WideString);
    procedure Set_Lang(Value: WideString);
    procedure Set_Numberformat(Value: WideString);
    procedure Set_Rows(Value: Integer);
    procedure Set_Test(Value: WideString);
    procedure Set_Timeformat(Value: WideString);
    procedure Set_Timestampformat(Value: WideString);
    procedure Set_Type_(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSqlinfoType }

  TXMLSqlinfoType = class(TXMLNode, IXMLSqlinfoType)
  protected
    { IXMLSqlinfoType }
    function Get_Stdout: WideString;
    function Get_Stderr: WideString;
    function Get_Sqlstmt: WideString;
    function Get_Sqlsort: WideString;
    function Get_Sqluser: WideString;
    function Get_Sqldbms: WideString;
    function Get_Sqlengine: WideString;
    function Get_Sqllang: WideString;
    function Get_Sqltype: WideString;
    function Get_Sqltime: Integer;
    function Get_Sqlcount: Integer;
    function Get_Sqlserial: Integer;
    function Get_Sqlmaxrows: Integer;
    function Get_Sqltables: IXMLSqltablesType;
    procedure Set_Stdout(Value: WideString);
    procedure Set_Stderr(Value: WideString);
    procedure Set_Sqlstmt(Value: WideString);
    procedure Set_Sqlsort(Value: WideString);
    procedure Set_Sqluser(Value: WideString);
    procedure Set_Sqldbms(Value: WideString);
    procedure Set_Sqlengine(Value: WideString);
    procedure Set_Sqllang(Value: WideString);
    procedure Set_Sqltype(Value: WideString);
    procedure Set_Sqltime(Value: Integer);
    procedure Set_Sqlcount(Value: Integer);
    procedure Set_Sqlserial(Value: Integer);
    procedure Set_Sqlmaxrows(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSqltablesType }

  TXMLSqltablesType = class(TXMLNode, IXMLSqltablesType)
  protected
    { IXMLSqltablesType }
    function Get_Sqltable: IXMLSqltableType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSqltableType }

  TXMLSqltableType = class(TXMLNode, IXMLSqltableType)
  protected
    { IXMLSqltableType }
    function Get_Bestrowid: WideString;
    function Get_Delete: WideString;
    function Get_Insert: WideString;
    function Get_Table: WideString;
    function Get_Update: WideString;
    procedure Set_Bestrowid(Value: WideString);
    procedure Set_Delete(Value: WideString);
    procedure Set_Insert(Value: WideString);
    procedure Set_Table(Value: WideString);
    procedure Set_Update(Value: WideString);
  end;

{ TXMLMetadataType }

  TXMLMetadataType = class(TXMLNodeCollection, IXMLMetadataType)
  protected
    { IXMLMetadataType }
    function Get_Column(Index: Integer): IXMLColumnType;
    function Add: IXMLColumnType;
    function Insert(const Index: Integer): IXMLColumnType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLColumnType }

  TXMLColumnType = class(TXMLNode, IXMLColumnType)
  protected
    { IXMLColumnType }
    function Get_CatalogName: WideString;
    function Get_ColumnClassName: WideString;
    function Get_ColumnDisplaySize: Integer;
    function Get_ColumnLabel: WideString;
    function Get_ColumnName: WideString;
    function Get_ColumnType: Integer;
    function Get_ColumnTypeName: WideString;
    function Get_IsAutoIncrement: WideString;
    function Get_IsCaseSensitive: WideString;
    function Get_IsCurrency: WideString;
    function Get_IsDefinitelyWritable: WideString;
    function Get_IsHidden: WideString;
    function Get_IsNullable: Integer;
    function Get_IsNumeric: WideString;
    function Get_IsPercent: WideString;
    function Get_IsReadOnly: WideString;
    function Get_IsRowid: WideString;
    function Get_IsSigned: WideString;
    function Get_IsTotal: WideString;
    function Get_IsWritable: WideString;
    function Get_Precision: Integer;
    function Get_Scale: Integer;
    function Get_SchemaName: WideString;
    function Get_TableName: WideString;
    procedure Set_CatalogName(Value: WideString);
    procedure Set_ColumnClassName(Value: WideString);
    procedure Set_ColumnDisplaySize(Value: Integer);
    procedure Set_ColumnLabel(Value: WideString);
    procedure Set_ColumnName(Value: WideString);
    procedure Set_ColumnType(Value: Integer);
    procedure Set_ColumnTypeName(Value: WideString);
    procedure Set_IsAutoIncrement(Value: WideString);
    procedure Set_IsCaseSensitive(Value: WideString);
    procedure Set_IsCurrency(Value: WideString);
    procedure Set_IsDefinitelyWritable(Value: WideString);
    procedure Set_IsHidden(Value: WideString);
    procedure Set_IsNullable(Value: Integer);
    procedure Set_IsNumeric(Value: WideString);
    procedure Set_IsPercent(Value: WideString);
    procedure Set_IsReadOnly(Value: WideString);
    procedure Set_IsRowid(Value: WideString);
    procedure Set_IsSigned(Value: WideString);
    procedure Set_IsTotal(Value: WideString);
    procedure Set_IsWritable(Value: WideString);
    procedure Set_Precision(Value: Integer);
    procedure Set_Scale(Value: Integer);
    procedure Set_SchemaName(Value: WideString);
    procedure Set_TableName(Value: WideString);
  end;

{ TXMLRowsetType }

  TXMLRowsetType = class(TXMLNodeCollection, IXMLRowsetType)
  protected
    { IXMLRowsetType }
    function Get_Encoding: WideString;
    function Get_Lang: WideString;
    function Get_R(Index: Integer): IXMLRType;
    procedure Set_Encoding(Value: WideString);
    procedure Set_Lang(Value: WideString);
    function Add: IXMLRType;
    function Insert(const Index: Integer): IXMLRType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRType }

  TXMLRType = class(TXMLNodeCollection, IXMLRType)
  protected
    { IXMLRType }
    function Get_C(Index: Integer): Integer;
    function Add(const C: Integer): IXMLNode;
    function Insert(const Index: Integer; const C: Integer): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetRespuestaSOAP(Doc: IXMLDocument): IXMLRespuestaSOAP;
function LoadRespuestaSOAP(const FileName: WideString): IXMLRespuestaSOAP;
function NewRespuestaSOAP: IXMLRespuestaSOAP;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRespuestaSOAP(Doc: IXMLDocument): IXMLRespuestaSOAP;
begin
  Result := Doc.GetDocBinding('RespuestaSOAP', TXMLSqlresponseType, TargetNamespace) as IXMLRespuestaSOAP;
end;

function LoadRespuestaSOAP(const FileName: WideString): IXMLRespuestaSOAP;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('RespuestaSOAP', TXMLSqlresponseType, TargetNamespace) as IXMLRespuestaSOAP;
end;

function NewRespuestaSOAP: IXMLRespuestaSOAP;
begin
  Result := NewXMLDocument.GetDocBinding('RespuestaSOAP', TXMLSqlresponseType, TargetNamespace) as IXMLRespuestaSOAP;
end;

{ TXMLSqlresponseType }

procedure TXMLSqlresponseType.AfterConstruction;
begin
  RegisterChildNode('sqlinfo', TXMLSqlinfoType);
  RegisterChildNode('metadata', TXMLMetadataType);
  RegisterChildNode('rowset', TXMLRowsetType);
  inherited;
end;

function TXMLSqlresponseType.Get_Country: WideString;
begin
  Result := AttributeNodes['country'].Text;
end;

procedure TXMLSqlresponseType.Set_Country(Value: WideString);
begin
  SetAttribute('country', Value);
end;

function TXMLSqlresponseType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLSqlresponseType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLSqlresponseType.Get_Dateformat: WideString;
begin
  Result := AttributeNodes['date-format'].Text;
end;

procedure TXMLSqlresponseType.Set_Dateformat(Value: WideString);
begin
  SetAttribute('date-format', Value);
end;

function TXMLSqlresponseType.Get_Dbms: WideString;
begin
  Result := AttributeNodes['dbms'].Text;
end;

procedure TXMLSqlresponseType.Set_Dbms(Value: WideString);
begin
  SetAttribute('dbms', Value);
end;

function TXMLSqlresponseType.Get_From: WideString;
begin
  Result := AttributeNodes['from'].Text;
end;

procedure TXMLSqlresponseType.Set_From(Value: WideString);
begin
  SetAttribute('from', Value);
end;

function TXMLSqlresponseType.Get_Host: WideString;
begin
  Result := AttributeNodes['host'].Text;
end;

procedure TXMLSqlresponseType.Set_Host(Value: WideString);
begin
  SetAttribute('host', Value);
end;

function TXMLSqlresponseType.Get_Lang: WideString;
begin
  Result := AttributeNodes['lang'].Text;
end;

procedure TXMLSqlresponseType.Set_Lang(Value: WideString);
begin
  SetAttribute('lang', Value);
end;

function TXMLSqlresponseType.Get_Numberformat: WideString;
begin
  Result := AttributeNodes['number-format'].Text;
end;

procedure TXMLSqlresponseType.Set_Numberformat(Value: WideString);
begin
  SetAttribute('number-format', Value);
end;

function TXMLSqlresponseType.Get_Rows: Integer;
begin
  Result := AttributeNodes['rows'].NodeValue;
end;

procedure TXMLSqlresponseType.Set_Rows(Value: Integer);
begin
  SetAttribute('rows', Value);
end;

function TXMLSqlresponseType.Get_Test: WideString;
begin
  Result := AttributeNodes['test'].Text;
end;

procedure TXMLSqlresponseType.Set_Test(Value: WideString);
begin
  SetAttribute('test', Value);
end;

function TXMLSqlresponseType.Get_Timeformat: WideString;
begin
  Result := AttributeNodes['time-format'].Text;
end;

procedure TXMLSqlresponseType.Set_Timeformat(Value: WideString);
begin
  SetAttribute('time-format', Value);
end;

function TXMLSqlresponseType.Get_Timestampformat: WideString;
begin
  Result := AttributeNodes['timestamp-format'].Text;
end;

procedure TXMLSqlresponseType.Set_Timestampformat(Value: WideString);
begin
  SetAttribute('timestamp-format', Value);
end;

function TXMLSqlresponseType.Get_Type_: WideString;
begin
  Result := AttributeNodes['type'].Text;
end;

procedure TXMLSqlresponseType.Set_Type_(Value: WideString);
begin
  SetAttribute('type', Value);
end;

function TXMLSqlresponseType.Get_Sqlinfo: IXMLSqlinfoType;
begin
  Result := ChildNodes['sqlinfo'] as IXMLSqlinfoType;
end;

function TXMLSqlresponseType.Get_Metadata: IXMLMetadataType;
begin
  Result := ChildNodes['metadata'] as IXMLMetadataType;
end;

function TXMLSqlresponseType.Get_Rowset: IXMLRowsetType;
begin
  Result := ChildNodes['rowset'] as IXMLRowsetType;
end;

{ TXMLSqlinfoType }

procedure TXMLSqlinfoType.AfterConstruction;
begin
  RegisterChildNode('sqltables', TXMLSqltablesType);
  inherited;
end;

function TXMLSqlinfoType.Get_Stdout: WideString;
begin
  Result := ChildNodes['stdout'].Text;
end;

procedure TXMLSqlinfoType.Set_Stdout(Value: WideString);
begin
  ChildNodes['stdout'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Stderr: WideString;
begin
  Result := ChildNodes['stderr'].Text;
end;

procedure TXMLSqlinfoType.Set_Stderr(Value: WideString);
begin
  ChildNodes['stderr'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlstmt: WideString;
begin
  Result := ChildNodes['sqlstmt'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqlstmt(Value: WideString);
begin
  ChildNodes['sqlstmt'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlsort: WideString;
begin
  Result := ChildNodes['sqlsort'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqlsort(Value: WideString);
begin
  ChildNodes['sqlsort'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqluser: WideString;
begin
  Result := ChildNodes['sqluser'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqluser(Value: WideString);
begin
  ChildNodes['sqluser'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqldbms: WideString;
begin
  Result := ChildNodes['sqldbms'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqldbms(Value: WideString);
begin
  ChildNodes['sqldbms'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlengine: WideString;
begin
  Result := ChildNodes['sqlengine'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqlengine(Value: WideString);
begin
  ChildNodes['sqlengine'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqllang: WideString;
begin
  Result := ChildNodes['sqllang'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqllang(Value: WideString);
begin
  ChildNodes['sqllang'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqltype: WideString;
begin
  Result := ChildNodes['sqltype'].Text;
end;

procedure TXMLSqlinfoType.Set_Sqltype(Value: WideString);
begin
  ChildNodes['sqltype'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqltime: Integer;
begin
  Result := ChildNodes['sqltime'].NodeValue;
end;

procedure TXMLSqlinfoType.Set_Sqltime(Value: Integer);
begin
  ChildNodes['sqltime'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlcount: Integer;
begin
  Result := ChildNodes['sqlcount'].NodeValue;
end;

procedure TXMLSqlinfoType.Set_Sqlcount(Value: Integer);
begin
  ChildNodes['sqlcount'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlserial: Integer;
begin
  Result := ChildNodes['sqlserial'].NodeValue;
end;

procedure TXMLSqlinfoType.Set_Sqlserial(Value: Integer);
begin
  ChildNodes['sqlserial'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqlmaxrows: Integer;
begin
  Result := ChildNodes['sqlmaxrows'].NodeValue;
end;

procedure TXMLSqlinfoType.Set_Sqlmaxrows(Value: Integer);
begin
  ChildNodes['sqlmaxrows'].NodeValue := Value;
end;

function TXMLSqlinfoType.Get_Sqltables: IXMLSqltablesType;
begin
  Result := ChildNodes['sqltables'] as IXMLSqltablesType;
end;

{ TXMLSqltablesType }

procedure TXMLSqltablesType.AfterConstruction;
begin
  RegisterChildNode('sqltable', TXMLSqltableType);
  inherited;
end;

function TXMLSqltablesType.Get_Sqltable: IXMLSqltableType;
begin
  Result := ChildNodes['sqltable'] as IXMLSqltableType;
end;

{ TXMLSqltableType }

function TXMLSqltableType.Get_Bestrowid: WideString;
begin
  Result := AttributeNodes['bestrowid'].Text;
end;

procedure TXMLSqltableType.Set_Bestrowid(Value: WideString);
begin
  SetAttribute('bestrowid', Value);
end;

function TXMLSqltableType.Get_Delete: WideString;
begin
  Result := AttributeNodes['delete'].Text;
end;

procedure TXMLSqltableType.Set_Delete(Value: WideString);
begin
  SetAttribute('delete', Value);
end;

function TXMLSqltableType.Get_Insert: WideString;
begin
  Result := AttributeNodes['insert'].Text;
end;

procedure TXMLSqltableType.Set_Insert(Value: WideString);
begin
  SetAttribute('insert', Value);
end;

function TXMLSqltableType.Get_Table: WideString;
begin
  Result := AttributeNodes['table'].Text;
end;

procedure TXMLSqltableType.Set_Table(Value: WideString);
begin
  SetAttribute('table', Value);
end;

function TXMLSqltableType.Get_Update: WideString;
begin
  Result := AttributeNodes['update'].Text;
end;

procedure TXMLSqltableType.Set_Update(Value: WideString);
begin
  SetAttribute('update', Value);
end;

{ TXMLMetadataType }

procedure TXMLMetadataType.AfterConstruction;
begin
  RegisterChildNode('column', TXMLColumnType);
  ItemTag := 'column';
  ItemInterface := IXMLColumnType;
  inherited;
end;

function TXMLMetadataType.Get_Column(Index: Integer): IXMLColumnType;
begin
  Result := List[Index] as IXMLColumnType;
end;

function TXMLMetadataType.Add: IXMLColumnType;
begin
  Result := AddItem(-1) as IXMLColumnType;
end;

function TXMLMetadataType.Insert(const Index: Integer): IXMLColumnType;
begin
  Result := AddItem(Index) as IXMLColumnType;
end;

{ TXMLColumnType }

function TXMLColumnType.Get_CatalogName: WideString;
begin
  Result := AttributeNodes['catalogName'].Text;
end;

procedure TXMLColumnType.Set_CatalogName(Value: WideString);
begin
  SetAttribute('catalogName', Value);
end;

function TXMLColumnType.Get_ColumnClassName: WideString;
begin
  Result := AttributeNodes['columnClassName'].Text;
end;

procedure TXMLColumnType.Set_ColumnClassName(Value: WideString);
begin
  SetAttribute('columnClassName', Value);
end;

function TXMLColumnType.Get_ColumnDisplaySize: Integer;
begin
  Result := AttributeNodes['columnDisplaySize'].NodeValue;
end;

procedure TXMLColumnType.Set_ColumnDisplaySize(Value: Integer);
begin
  SetAttribute('columnDisplaySize', Value);
end;

function TXMLColumnType.Get_ColumnLabel: WideString;
begin
  Result := AttributeNodes['columnLabel'].Text;
end;

procedure TXMLColumnType.Set_ColumnLabel(Value: WideString);
begin
  SetAttribute('columnLabel', Value);
end;

function TXMLColumnType.Get_ColumnName: WideString;
begin
  Result := AttributeNodes['columnName'].Text;
end;

procedure TXMLColumnType.Set_ColumnName(Value: WideString);
begin
  SetAttribute('columnName', Value);
end;

function TXMLColumnType.Get_ColumnType: Integer;
begin
  Result := AttributeNodes['columnType'].NodeValue;
end;

procedure TXMLColumnType.Set_ColumnType(Value: Integer);
begin
  SetAttribute('columnType', Value);
end;

function TXMLColumnType.Get_ColumnTypeName: WideString;
begin
  Result := AttributeNodes['columnTypeName'].Text;
end;

procedure TXMLColumnType.Set_ColumnTypeName(Value: WideString);
begin
  SetAttribute('columnTypeName', Value);
end;

function TXMLColumnType.Get_IsAutoIncrement: WideString;
begin
  Result := AttributeNodes['isAutoIncrement'].Text;
end;

procedure TXMLColumnType.Set_IsAutoIncrement(Value: WideString);
begin
  SetAttribute('isAutoIncrement', Value);
end;

function TXMLColumnType.Get_IsCaseSensitive: WideString;
begin
  Result := AttributeNodes['isCaseSensitive'].Text;
end;

procedure TXMLColumnType.Set_IsCaseSensitive(Value: WideString);
begin
  SetAttribute('isCaseSensitive', Value);
end;

function TXMLColumnType.Get_IsCurrency: WideString;
begin
  Result := AttributeNodes['isCurrency'].Text;
end;

procedure TXMLColumnType.Set_IsCurrency(Value: WideString);
begin
  SetAttribute('isCurrency', Value);
end;

function TXMLColumnType.Get_IsDefinitelyWritable: WideString;
begin
  Result := AttributeNodes['isDefinitelyWritable'].Text;
end;

procedure TXMLColumnType.Set_IsDefinitelyWritable(Value: WideString);
begin
  SetAttribute('isDefinitelyWritable', Value);
end;

function TXMLColumnType.Get_IsHidden: WideString;
begin
  Result := AttributeNodes['isHidden'].Text;
end;

procedure TXMLColumnType.Set_IsHidden(Value: WideString);
begin
  SetAttribute('isHidden', Value);
end;

function TXMLColumnType.Get_IsNullable: Integer;
begin
  Result := AttributeNodes['isNullable'].NodeValue;
end;

procedure TXMLColumnType.Set_IsNullable(Value: Integer);
begin
  SetAttribute('isNullable', Value);
end;

function TXMLColumnType.Get_IsNumeric: WideString;
begin
  Result := AttributeNodes['isNumeric'].Text;
end;

procedure TXMLColumnType.Set_IsNumeric(Value: WideString);
begin
  SetAttribute('isNumeric', Value);
end;

function TXMLColumnType.Get_IsPercent: WideString;
begin
  Result := AttributeNodes['isPercent'].Text;
end;

procedure TXMLColumnType.Set_IsPercent(Value: WideString);
begin
  SetAttribute('isPercent', Value);
end;

function TXMLColumnType.Get_IsReadOnly: WideString;
begin
  Result := AttributeNodes['isReadOnly'].Text;
end;

procedure TXMLColumnType.Set_IsReadOnly(Value: WideString);
begin
  SetAttribute('isReadOnly', Value);
end;

function TXMLColumnType.Get_IsRowid: WideString;
begin
  Result := AttributeNodes['isRowid'].Text;
end;

procedure TXMLColumnType.Set_IsRowid(Value: WideString);
begin
  SetAttribute('isRowid', Value);
end;

function TXMLColumnType.Get_IsSigned: WideString;
begin
  Result := AttributeNodes['isSigned'].Text;
end;

procedure TXMLColumnType.Set_IsSigned(Value: WideString);
begin
  SetAttribute('isSigned', Value);
end;

function TXMLColumnType.Get_IsTotal: WideString;
begin
  Result := AttributeNodes['isTotal'].Text;
end;

procedure TXMLColumnType.Set_IsTotal(Value: WideString);
begin
  SetAttribute('isTotal', Value);
end;

function TXMLColumnType.Get_IsWritable: WideString;
begin
  Result := AttributeNodes['isWritable'].Text;
end;

procedure TXMLColumnType.Set_IsWritable(Value: WideString);
begin
  SetAttribute('isWritable', Value);
end;

function TXMLColumnType.Get_Precision: Integer;
begin
  Result := AttributeNodes['precision'].NodeValue;
end;

procedure TXMLColumnType.Set_Precision(Value: Integer);
begin
  SetAttribute('precision', Value);
end;

function TXMLColumnType.Get_Scale: Integer;
begin
  Result := AttributeNodes['scale'].NodeValue;
end;

procedure TXMLColumnType.Set_Scale(Value: Integer);
begin
  SetAttribute('scale', Value);
end;

function TXMLColumnType.Get_SchemaName: WideString;
begin
  Result := AttributeNodes['schemaName'].Text;
end;

procedure TXMLColumnType.Set_SchemaName(Value: WideString);
begin
  SetAttribute('schemaName', Value);
end;

function TXMLColumnType.Get_TableName: WideString;
begin
  Result := AttributeNodes['tableName'].Text;
end;

procedure TXMLColumnType.Set_TableName(Value: WideString);
begin
  SetAttribute('tableName', Value);
end;

{ TXMLRowsetType }

procedure TXMLRowsetType.AfterConstruction;
begin
  RegisterChildNode('r', TXMLRType);
  ItemTag := 'r';
  ItemInterface := IXMLRType;
  inherited;
end;

function TXMLRowsetType.Get_Encoding: WideString;
begin
  Result := AttributeNodes['encoding'].Text;
end;

procedure TXMLRowsetType.Set_Encoding(Value: WideString);
begin
  SetAttribute('encoding', Value);
end;

function TXMLRowsetType.Get_Lang: WideString;
begin
  Result := AttributeNodes['lang'].Text;
end;

procedure TXMLRowsetType.Set_Lang(Value: WideString);
begin
  SetAttribute('lang', Value);
end;

function TXMLRowsetType.Get_R(Index: Integer): IXMLRType;
begin
  Result := List[Index] as IXMLRType;
end;

function TXMLRowsetType.Add: IXMLRType;
begin
  Result := AddItem(-1) as IXMLRType;
end;

function TXMLRowsetType.Insert(const Index: Integer): IXMLRType;
begin
  Result := AddItem(Index) as IXMLRType;
end;

{ TXMLRType }

procedure TXMLRType.AfterConstruction;
begin
  ItemTag := 'c';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLRType.Get_C(Index: Integer): Integer;
begin
  Result := List[Index].NodeValue;
end;

function TXMLRType.Add(const C: Integer): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := C;
end;

function TXMLRType.Insert(const Index: Integer; const C: Integer): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := C;
end;

end.