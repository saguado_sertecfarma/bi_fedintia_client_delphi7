# BI

IMPORTANTE: No trabajar sobre **master**!!

#Gitflow

<iframe src="https://drive.google.com/file/d/17EFHFUGDXcpxxXD6dObMBpcqKyfq34lN/preview" width="640" height="250"></iframe>


## En el momento de trabajar:

   * Rama **master**: cualquier commit que pongamos en esta rama debe estar preparado para subir a producción.
   	- Todos los commits tienen que ser óptimos para subir a producción.
	- Cada vez que se incorpora código a master se genera una nueva versión.	
   * Rama **develop**: rama en la que está el código que conformará la siguiente versión planificada del proyecto.
    - Trabajar en rama develop, para uso interno, rama de trabajo de donde salen las todas las ramas que existentes.
    
   ----
	* Feature, rama que parte y se incorpora siempre a develop.
        * Desarrollo de nuevas características.  
	    * Nombre: si no se usa git flow, fte_...
   ----
    * Release, se origina en develop y se incorpora a master y develop.
   	   * Corrigen el el código antes de pasar a master.
	   * Nombre: si no se usa git flow, rle_...  
       nombre rls_xxx
   ----
	* Hotfix, se origina a partir de la rama master, se incorpora a master y develop.
   	   * Corrigen errores en producción.
	   * Nombre: si no se usa git flow, htx_...
   ----
	* Test, rama para testng
        *  Nombre: No esta en gitflow, tsg_---
   ----


-
<iframe src="https://drive.google.com/file/d/1Q9BxQGVtnFVDnhT5OKlLVj27rg6lo2pr/preview" width="360" height="480"></iframe>
   
## En el momento de realizar commit:

   * Al realizar commit, asegurarse que solo se incluyen los ficheros que son necesarios.
   * Realizar siempre un comentario descriptivo y constructivo.
   * En Master realizar los commits empezando ‘v4.1.62.62XX Descripción de la versión, y realizando una etiqueta de ese v4… (TAG).
   * En el develop, realizar los commit empezando siempre por ‘v4.1.62.XX Descripción de lo que se ha hecho’.
       - La X es un +1, para que en su fin al realizar merge sobre master poder tener claro la versión que toca, de manera visual.

## Etiquetas:

   * Versiones finales, en master.
   * Versiones de desarrollo, en develop.

## Otros:

**Consensuar el .ignoregit y preguntar antes de editar.**

Tenemos una pequeña guía en el repositorio, bajar para consultar: 
IOFWIN GIT.docx

***Antes de subir si no se esta seguro preguntar.***

# Versión
- V1.1.1.1 Noviembre 2018  
- V1.1.1.2 19/11/2018  
- V1.1.1.3 22/11/2018  
	* Solucionado error codi_prescrito nulo.  
- V1.1.1.4 30/12/2018
	* Envío diario manual.  
	* Mejora en el cierre.
- V1.1.1.4 08/03/2019
	* Se baja a la mitad el tiempo de espera, Timer 77776 * 25  
- V1.1.1.5 15/05/2020
	* Se incorporan los nuevos campos IP1,IP2 y CIF de proveedor
	* Se cambia la versión del fichero BI a 03 
- V1.1.1.5 18/05/2020
	* Se cambia la versión del fichero BI a 04, lo pide Joan
- V1.1.1.5 19/05/2020
	* Se cambia la versión del fichero BI a 01, lo pide Joan

	

#Variables
## Modo
1 - Envío Semanal (No se usa)  
2 - Diario, pulsando el botón manual se envían todos los días hasta el día actual menos hoy 
3 - Diario, solo se envía el día que indiquen

## BIModo
1 - Desde el botón del formulario.  
3 - Desde el botón del formulario.  
2 - Envío escalonado, lo activa IofWin.  
99 - Se lo activa Iofwin, para cierres de viernes, sábados a ciertas horas, envía lo que tienen del día. Se volverá a enviar el siguiente día de forma completa.  


---
Javier Sánchez Gómez  
<jsanchez@fedefarma.com>