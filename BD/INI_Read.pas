unit INI_Read;

interface

uses
 IniFiles, Dialogs, Classes, SysUtils, FORMS, Windows, shellapi, strUtils;
 // Otras configuraciones
const
   VERSION_BI_PRO = '01'; //Javier 19/05/2020, numero de linea
  //VERSION_BI_PRO = '04'; //Javier 15/05/2020, numero de linea
  //VERSION_BI_PRO = '03'; //Javier 14/05/2020, numero de linea
  //VERSION_BI_PRO = '02';//Javier 28/07/2017, numero de linea
  VERSION_MA_PRO = '01';

var
 DIR_BD, DIR_IMS, BIBotiquin :string;
 IsDeveloper                 :Boolean;
 infoPrograma                :String;

 procedure Read(Modo :integer);
 function SeDebeEnviar: Boolean;
 // Sergio Aguado, 08/06/2021, Llama al ejecutable que actualiza los PVP_ONLINE con los PVP WEB.
 function Actualizar_pvps_web( strDirExe:string ): boolean;
 // Sergio Aguado, 08/06/2021, Llamar a un ejecutable y esperar que finalice.
 procedure RunAndWaitShell(Ejecutable, Argumentos: string; Visibilidad: Integer);

implementation

 uses BI_DM, BI_Generator, BI, Calculate, IMS_Generator,IBQuery, BI_Functions, BI_Check;


 function ProcesarIMS:Boolean;
 var
    dDiaInicial:TDateTime;
 begin
      Result := SameText(UpperCase(DataModule1.IBTS_IMS.FieldByName('IMS_ACTIU').AsString),'S');
      if (Result) then
      begin
        dDiaInicial := DataModule1.ibts_IMSIMS_DATA_ENVIADA.AsDateTime;
        Result := ((dDiaInicial + 7) < Now);
        if not result then
           BILog('Hace menos de una semana que se envio.');
      end
      else
      begin
        BILog('IMS_ACTIU = "N"');
      end;
 end;

 //NOS DEVULEVE SI DEBE ENVIARSE ALGO O NO DEBE HACERSE NADA
 function SeDebeEnviar: boolean;
 var BI_Control_Activo: string;
     FechaEnviada,Hoy: TDatetime;
 begin
    result := false;
    //valores iniciales, con estos seguro que result=false
    FechaEnviada := now;
    BI_Control_Activo := 'N';

    //no necesitamos try porque Eje_SP_Ret no eleva las excepciones, las anota en log y sigue.
    BI_Control_Activo := bi_functions.Eje_SP_Ret('select BI_ACTIU as Retorn from  BI_CONTROL');
    FechaEnviada := bi_functions.Eje_SP_Ret('select BI_DATA_ENVIADA as Retorn from BI_CONTROL');

    //hoy no lo enviamos
    Hoy := Trunc(Now);
    result := ((FechaEnviada < (Hoy-1)) and (BI_Control_Activo = 'S'));
    if not result then begin
      if (BI_Control_Activo='N') then
         BILog('BI_ACTIU = "N"')
      else
         BILog('Ya se envio ayer');
    end;
 end;

 // SERGIO AGUADO, 08/06/2021.
 // Lo que hace este procedimiento es ejecutar con "ShellExecuteEx" el fichero que se le pasa "Ejecutable"
 // pasandole los parametros "Argumentos" y hasta que no termine no sale del procedimiento.
 procedure RunAndWaitShell(Ejecutable, Argumentos: string; Visibilidad: Integer);
 var
   Info: TShellExecuteInfo;
   pInfo: PShellExecuteInfo;
   exitCode: DWord;
 begin
   pInfo := @Info;
   with Info do
   begin
     cbSize := SizeOf(Info);
     fMask := SEE_MASK_NOCLOSEPROCESS;
     wnd := Application.Handle;
     lpVerb := nil;
     lpFile := PChar(Ejecutable);
     lpParameters := PChar(Argumentos + #0);
     lpDirectory := nil;
     nShow := Visibilidad;
     hInstApp := 0;
   end;

   ShellExecuteEx(pInfo);

   repeat
     exitCode  :=  WaitForSingleObject(Info.hProcess, 500);
     Application.ProcessMessages;
   until (exitCode <> WAIT_TIMEOUT);
 end;


 //SERGIO AGUADO, 08/06/2021. Llamar al ejecutable que mete los PVPs Web en ARTICLES_MESTRE
 //Originalmente el exe se llamaba PImagenesWEP_PVP_TO_ArticlesMestre_PVP_ONLINE.exe
 //se renombró como IMAGESPVP_TO_PVPONLINE.EXE
 function Actualizar_pvps_web( strDirExe:string ): boolean;
 var nomExe: string;
 begin
    try
      result := false;
      nomExe := strDirExe+'IMAGESPVP_TO_PVPONLINE.EXE';
      if Fileexists( nomExe ) then
      begin
        BILog( 'Ejecutando '+nomExe );
        RunAndWaitShell( PChar(nomExe), '2', SW_SHOWNORMAL);
        BILog( 'Finalizado '+nomExe );
        BILog( 'Ahora borramos el ejecutable.' );
        if fileexists( nomExe ) then
        begin
          try
            deletefile( pchar(nomExe) );
          except
            on e:Exception do
            begin
              raise exception.Create('Error borrando ejecutable. '+e.message);
            end;
          end;
        end;
        BILog( 'OK borrado satisfactorio.' );
        result := True;
      end;
    except
      on e:exception do
      begin
        BILog( e.message );
        raise exception.create('Error al ejecutar '+ nomExe +' '+ e.Message );
      end;
    end;
 end;

procedure Read(Modo :integer);
var
   sDefServer, sDefRuta, sDefBD, sDefUnidad, sDefPuesto,
   sServer, sRuta, sBD, sUnidad, sPuesto, sCadena_Conexion_BD,
   DIR_EXE, DIR_TEMPORAL, msgerror                             :String;
   Ini, IniConfig                                              :TiniFile;
   DB_Param                                                    :tstringlist;
   NombreIni                                                   :string;
   DIR_IOFWIN, DIR_BD, UNIDAD_IOFWIN                           :STRING;
   EsLocalHost                                                 :boolean;
   ServerContinene2puntos                                      :boolean;
   enviado                                                     :Boolean;
   strCURSOR        :string;
   ResultadoBiCheck :integer;
   txtResultado     :string;
begin
     strCURSOR := 'Inicialización';
     sDefServer := 'localhost';
     sDefRuta   := '\IOFWIN\';
     sDefBD     := 'IOFWIN.GDB';
     sDefUnidad := 'C:';
     sDefPuesto := '1';

     sServer    := sDefServer;
     sRuta      := sDefRuta;
     sBD        := sDefBD;
     sUnidad    := sDefUnidad;
     sPuesto    := sDefPuesto;

     sCadena_Conexion_BD:= '';

     NombreIni  := 'IOFWIN.INI';

//      If ParamCount > 1 Then
//         NombreIni := ParamStr(2);

     //Javier 01/02/2017

     infoPrograma:= GetAppVersion();

     if FileExists('C:\WINDOWS\IOFWIN.INI')then
     begin
          try
             strCURSOR := 'Crear TIniFile IOFWIN.INI';
             Ini := TIniFile.create('C:\WINDOWS\IOFWIN.INI');
             //Ini := TIniFile.create(NombreIni);

             strCURSOR := 'Obtener SERVER';
             if not Ini.ValueExists('Datos','SERVER') then
             begin
                  Ini.WriteString('Datos','SERVER',sDefServer);
                  sServer := Ini.readString('Datos','SERVER',sServer);
             end
             else
             begin
                  sServer := Ini.readString('Datos','SERVER',sServer);
             end;


             strCURSOR := 'Obtener RUTA';
             if Ini.ValueExists('Datos','RUTA') then
             begin
                  sRuta := Ini.readString('Datos','RUTA',sBD);
             end;

             strCURSOR := 'Obtener BD';
             if not Ini.ValueExists('Datos','BD') then
             BEGIN
                  Ini.WriteString('Datos','BD',sDefBD);
                  sBD := Ini.readString( 'Datos','BD',sBD);
             END
             else
             begin
                  sBD := Ini.readString( 'Datos','BD',sBD);
             end;

             strCURSOR := 'Obtener UNIDAD';
             if not Ini.ValueExists('Datos','UNIDAD') then
             BEGIN
                  Ini.WriteString('Datos','UNIDAD',sDefUnidad);
                  sUnidad := Ini.readString('Datos','UNIDAD',sUnidad);
             END
             else
             begin
                  sUnidad := Ini.readString('Datos','UNIDAD',sUnidad);
             end;

             strCURSOR := 'Obtener PUESTO';
             if not Ini.ValueExists('Datos','PUESTO') then
             BEGIN
                  Ini.WriteString('Datos','PUESTO',sDefPuesto);
                  sPuesto := Ini.readString('Datos','PUESTO',sPuesto);
             END
             else
             begin
                  sPuesto := Ini.readString('Datos','PUESTO',sPuesto);
             end;

             strCURSOR := 'Obtener DIR_IOFWIN';
             DIR_IOFWIN := sUnidad + sRuta;
             DIR_BD  := sServer+':'+ DIR_IOFWIN + 'DATOS\' + sBD;
             DIR_IMS := DIR_IOFWIN + 'IMS\';
             DIR_TEMPORAL := DIR_IOFWIN+'TEMPORAL\';

             strCURSOR := 'Obtener DIR_EXE';
             DIR_EXE := sUnidad+sRuta;

             strCURSOR := 'Crear DIR_IMS';
             if not DirectoryExists(DIR_IMS)then
             begin
                  MKdir(DIR_IMS);
             end;

             strCURSOR := 'Comprobar LOCALHOST';
             //Javier 07/02/2017
             // Si es un receptor normal, que no tiene server a localhost y la unidad es distinta a c
             EsLocalHost := (Pos('LOCALHOST', UpperCase(sServer)) > 0);
             IF ((SUNIDAD <> 'C:') and (NOT EsLocalHost)) THEN
             BEGIN
                  UNIDAD_IOFWIN:='C:';
                  DIR_BD  := sServer+':'+ UNIDAD_IOFWIN + sRuta + 'DATOS\' + sBD;
             end;

             strCURSOR := 'Obtener RUTA';
             //Javier 14/02/2017
             // El server tiene dos puntos, indica existe letra. Puede que la bd esta en otro lugar... utilizamos solo la ruta del server ya que contiene la letra
             ServerContinene2puntos := (Pos(':', UpperCase(sServer)) > 0);
             if(ServerContinene2puntos) then
             begin
                  DIR_BD := sServer + ':' + sRuta + 'DATOS\' + sBD;
             end;

             strCURSOR := 'Contruir DB_Param';
             db_param  := TStringList.Create;
             DB_Param.Add('user_name=SYSDBA');
             DB_Param.Add('password=masterkey');

             strCURSOR := 'Cerrar conexion BD';
             If bi_dm.DataModule1.DataBase1.Connected then
             BEGIN
                  bi_dm.DataModule1.DataBase1.Close;
             end;

             strCURSOR := 'Conectar BD';
             bi_dm.DataModule1.DataBase1.Params.Clear;
             bi_dm.DataModule1.DataBase1.Params.Assign(DB_Param);

             BI_DM.DataModule1.DataBase1.DatabaseName  := DIR_BD;
             //BI_DM.DataModule1.HYDataBase1.DBName    := 'SERVIDOR:C:\IOFWIN\DATOS\IOFWIN.GDB';
             BI_DM.DATAMODULE1.DataBase1.Connected := True;
             BI_DM.DataModule1.Transaccion1.Active := True;

             // ojo
             strCURSOR := 'Reabrir IBTS_BI';
             BI_DM.DataModule1.IBTS_BI.Close;
             BI_DM.DataModule1.IBTS_BI.open;

             //Javier 18/01/2017, buscamos el numero de botiquin


             strCURSOR := 'Crear TInifile IOFWIN_CONFIG.INI';
             IniConfig := TInifile.create(DIR_IOFWIN + 'IOFWIN_CONFIG.ini');
             if not(IniConfig.ValueExists('Test', 'BIBotiquin')) then
             begin
                 IniConfig.WriteString('Test', 'BIBotiquin', '000');
             end;
             BIBotiquin := IniConfig.ReadString('Test', 'BIBotiquin', '000');
             BIBotiquin := RightStr('000'+BIBotiquin,3);

             {
             Propiedades para Desarrollo, si existe _devQuili
             1- Que se envie el fichero al directorio de test del FTP de Fede
             }

             BILog( 'Inicio proceso' );
             BILog( 'Versión: '+infoPrograma );
             BILog( 'Const PRUEBAS: '+BoolToStr(PRUEBAS,true)+', const PRUEBAS_ENVIAR: '+BoolToStr(PRUEBAS_ENVIAR,true)+', const ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP: '+BoolToStr(ENVIOS_HTTPS_ENVIAR_FILES_FIN_AL_FTP,true) );

             strCURSOR := 'Actualizar Form Caption';
             BI_Form1.Caption:= 'IOFWIN ' + infoPrograma;

             strCURSOR := 'Verificar c:\dev.js';
             IsDeveloper:=False;
             if FileExists('C:\dev.js')then
             begin
                  IsDeveloper:=True;
                  BI_Form1.Caption:='Opciones de Desarrollo activadas ' +  infoPrograma;
             end;


             //SERGIO, AQUI COMPRUEBO DE NUEVO SI SE DEBE HACER ALGO O TODAVIA NO TOCA ENVIAR NADA
             if SeDebeEnviar then
             begin

                 //SERGIO AGUADO, 08/06/2021. AQUI LLAMO AL EJECUTABLE QUE METE LOS PVP WEB EN ARTICLES_MESTRE.PVP_ONLINE
                 strCURSOR := 'PVP_WEB_TO_PP_ONLINE';
                 actualizar_pvps_web( DIR_EXE );

                 //SERGIO AGUADO, 27/05/2021. AQUI COMPRUEBO QUE LOS TOTALES EN FARMACIA COINCIDEN CON LOS TOTALES EN BI
                 strCURSOR := 'BI_CHECK';
                 BILog('Inicio proceso comparación de lineas de ventas IOFWIN con ventas BISUALFARMA.');
                 BI.ActualizaInformacion( 'Comparando total lineas BD farmacia <-> BD BI.' );
                 //ResultadoBiCheck := CompararNumLineasVentaFDB_y_BI( DIR_IMS, DIR_EXE, txtResultado );
                 ResultadoBiCheck := CompararNumLineasVentaIOFWIN_y_BI_V2( DIR_IMS, DIR_EXE, txtResultado );
                 if ResultadoBiCheck = ERROR_COMPARA_FDB_BI then
                    BILog('No se ha podido terminar el proceso de comparación BI_CHECK.')
                 else
                 begin
                    BILog( txtResultado );
                    BILog('Proceso de comparación BI_CHECK finalizado correctamente.');
                 end;
                 {
                 Modos Posibles
                 1 Semanal     DESHABILITADO
                 2 Diario      LLEGA DESDE IOFWIN
                 }
                 strCURSOR := 'Iniciar envio';
                 BILog('Inicio proceso de envio.');
                 case Modo of
                     1:begin
                            Week_Calculate(Modo);
                       end;
                     2,3:begin
                            Day_Calculate(Modo);
                        end;
                 end;

                 //Oliver
//                  DataModule1.IBTS_IMS.Open;
//                  if (ProcesarIMS) then
//                  begin
//                       Repeat
//                             enviado := IMS_Generator.GenerarIMS(DIR_TEMPORAL, DIR_IMS);
//                       Until ((Now -14) < DataModule1.IBTS_IMS.FieldByName('IMS_DATA_ENVIADA').AsDateTime) or not enviado
//                  end;
//                  DataModule1.IBTS_IMS.Close;

             end
             else
             begin
                 BILog('Todavia no toca enviar nada.');
             end; //fin se debe enviar?

             strCURSOR := 'Cerrar Form';
             BILog('FIN PROCESO');
             ActualizaInformacion('FIN PROCESO');
             BI_Form1.codigo_resultado_proceso := BI.RESULTADO_OK;
             BI_Form1.texto_resultado_proceso  := 'Proceso finalizado correctamente.';
             BI_Form1.Close;
          except
             on e:exception do
             begin
                  //msgerror   :=    e.message;
                  //SERGIO AGUADO, 17/05/2021. Añado el texto de la excepción al mensaje de error para que no sea tan genérico.
                  //BI_Form1.Caption := 'No se puede conectar con la BD. '+e.message;
                  BILog('Error en INI_Read.Read(), en '+strCURSOR+'. '+e.message);
                  BILog('FIN PROCESO');
                  ActualizaInformacion('FIN PROCESO');
                  BI_Form1.Caption := 'Error en INI_Read.Read(), en '+strCURSOR+'. '+e.message;

                  if ParamCount = 0 then
                     Sleep(5000);

                  BI_Form1.codigo_resultado_proceso := BI.RESULTADO_ERROR;
                  BI_Form1.texto_resultado_proceso  := 'Error en INI_Read.Read(), en '+strCURSOR+'. '+e.message;
                  BI_Form1.Close;
             end;
          end;
     end
     else
     begin
          BILog('No existe el fichero de configuración');
          BI_Form1.Caption := 'No existe el fichero de configuración';
          Sleep(5000);
          BI_Form1.codigo_resultado_proceso := BI.RESULTADO_ERROR;
          BI_Form1.texto_resultado_proceso  := 'No existe el fichero de configuración';
          BI_Form1.Close;
     end;
end;

end.



