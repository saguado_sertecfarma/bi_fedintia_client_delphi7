object DataModule1: TDataModule1
  OldCreateOrder = False
  Left = 823
  Top = 195
  Height = 495
  Width = 476
  object IdFTP1: TIdFTP
    MaxLineAction = maException
    ReadTimeout = 0
    Passive = True
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 192
    Top = 112
  end
  object Database1: TIBDatabase
    Connected = True
    DatabaseName = 'localhost:c:\iofwin\datos\iofwin.fdb'
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = Transaccion1
    IdleTimer = 0
    SQLDialect = 1
    TraceFlags = []
    Left = 40
    Top = 48
  end
  object Transaccion1: TIBTransaction
    Active = False
    DefaultDatabase = Database1
    AutoStopAction = saNone
    Left = 112
    Top = 48
  end
  object IBQ_BI: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 192
    Top = 48
  end
  object IBQ_MA: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 272
    Top = 48
  end
  object IBTS_BI: TIBTable
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'BI_CONTROL'
    Left = 40
    Top = 120
    object IBTS_BIBI_COMPTADOR: TSmallintField
      FieldName = 'BI_COMPTADOR'
      Required = True
    end
    object IBTS_BIBI_ANTECODI_FARMACIA: TIBStringField
      FieldName = 'BI_ANTECODI_FARMACIA'
      Size = 1
    end
    object IBTS_BIBI_CODI_FARMACIA: TIBStringField
      FieldName = 'BI_CODI_FARMACIA'
      Size = 5
    end
    object IBTS_BIBI_DATA_ENVIADA: TDateTimeField
      FieldName = 'BI_DATA_ENVIADA'
    end
    object IBTS_BIBI_DATA_ENVIO: TDateTimeField
      FieldName = 'BI_DATA_ENVIO'
    end
    object IBTS_BIBI_FTP_ADRECA: TIBStringField
      DisplayWidth = 160
      FieldName = 'BI_FTP_ADRECA'
      Size = 160
    end
    object IBTS_BIBI_FTP_PORT: TSmallintField
      FieldName = 'BI_FTP_PORT'
    end
    object IBTS_BIBI_FTP_USUARI: TIBStringField
      FieldName = 'BI_FTP_USUARI'
      Size = 30
    end
    object IBTS_BIBI_FTP_USUARI_PASSW: TIBStringField
      FieldName = 'BI_FTP_USUARI_PASSW'
      Size = 16
    end
    object IBTS_BIBI_LLOC_ENVIAMENT: TSmallintField
      FieldName = 'BI_LLOC_ENVIAMENT'
    end
    object IBTS_BIBI_DIA_ENVIAMENT: TSmallintField
      FieldName = 'BI_DIA_ENVIAMENT'
    end
    object IBTS_BIBI_HORA_ENVIAMENT: TIBStringField
      FieldName = 'BI_HORA_ENVIAMENT'
      Size = 5
    end
    object IBTS_BIBI_TIPUS_ENVIAMENT: TIBStringField
      FieldName = 'BI_TIPUS_ENVIAMENT'
      Size = 1
    end
    object IBTS_BIBI_DIRECTORI_DESTI: TIBStringField
      FieldName = 'BI_DIRECTORI_DESTI'
    end
    object IBTS_BIBI_ACTIU: TIBStringField
      FieldName = 'BI_ACTIU'
      Size = 1
    end
    object IBTS_BIBI_ENVIAMENT_FEDE: TIBStringField
      FieldName = 'BI_ENVIAMENT_FEDE'
      Size = 1
    end
  end
  object IBQ_LA: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 272
    Top = 112
  end
  object IBQ_Ejecuta_SP: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 112
    Top = 120
  end
  object IBTS_IMS: TIBTable
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'IMS_COMPTADOR'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'IMS_ANTECODI_FARMACIA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'IMS_CODI_FARMACIA'
        Attributes = [faFixed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'IMS_DATA_ENVIADA'
        DataType = ftDateTime
      end
      item
        Name = 'IMS_DATA_ENVIO'
        DataType = ftDateTime
      end
      item
        Name = 'IMS_FTP_ADRECA'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'IMS_FTP_PORT'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'IMS_FTP_USUARI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'IMS_FTP_USUARI_PASSW'
        DataType = ftString
        Size = 16
      end
      item
        Name = 'IMS_LLOC_ENVIAMENT'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'IMS_DIA_ENVIAMENT'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'IMS_TIPUS_ENVIAMENT'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'IMS_DIRECTORI_DESTI'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'IMS_ACTIU'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'IMS_ENVIAMENT_FEDE'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'IMS_HORA_ENVIAMENT'
        Attributes = [faFixed]
        DataType = ftString
        Size = 5
      end>
    IndexDefs = <
      item
        Name = 'RDB$PRIMARY296'
        Fields = 'IMS_COMPTADOR'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'IMS_CONTROL'
    Left = 48
    Top = 192
    object IBTS_IMSIMS_COMPTADOR: TSmallintField
      FieldName = 'IMS_COMPTADOR'
      Required = True
    end
    object IBTS_IMSIMS_ANTECODI_FARMACIA: TIBStringField
      FieldName = 'IMS_ANTECODI_FARMACIA'
      Size = 1
    end
    object IBTS_IMSIMS_CODI_FARMACIA: TIBStringField
      FieldName = 'IMS_CODI_FARMACIA'
      Size = 5
    end
    object IBTS_IMSIMS_DATA_ENVIADA: TDateTimeField
      FieldName = 'IMS_DATA_ENVIADA'
    end
    object IBTS_IMSIMS_DATA_ENVIO: TDateTimeField
      FieldName = 'IMS_DATA_ENVIO'
    end
    object IBTS_IMSIMS_FTP_ADRECA: TIBStringField
      FieldName = 'IMS_FTP_ADRECA'
      Size = 40
    end
    object IBTS_IMSIMS_FTP_PORT: TSmallintField
      FieldName = 'IMS_FTP_PORT'
    end
    object IBTS_IMSIMS_FTP_USUARI: TIBStringField
      FieldName = 'IMS_FTP_USUARI'
      Size = 30
    end
    object IBTS_IMSIMS_FTP_USUARI_PASSW: TIBStringField
      FieldName = 'IMS_FTP_USUARI_PASSW'
      Size = 16
    end
    object IBTS_IMSIMS_LLOC_ENVIAMENT: TSmallintField
      FieldName = 'IMS_LLOC_ENVIAMENT'
    end
    object IBTS_IMSIMS_DIA_ENVIAMENT: TSmallintField
      FieldName = 'IMS_DIA_ENVIAMENT'
    end
    object IBTS_IMSIMS_TIPUS_ENVIAMENT: TIBStringField
      FieldName = 'IMS_TIPUS_ENVIAMENT'
      Size = 1
    end
    object IBTS_IMSIMS_DIRECTORI_DESTI: TIBStringField
      FieldName = 'IMS_DIRECTORI_DESTI'
    end
    object IBTS_IMSIMS_ACTIU: TIBStringField
      FieldName = 'IMS_ACTIU'
      Size = 1
    end
    object IBTS_IMSIMS_ENVIAMENT_FEDE: TIBStringField
      FieldName = 'IMS_ENVIAMENT_FEDE'
      Size = 1
    end
    object IBTS_IMSIMS_HORA_ENVIAMENT: TIBStringField
      FieldName = 'IMS_HORA_ENVIAMENT'
      Size = 5
    end
  end
  object IBQ_IMS: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 192
  end
  object IBQ_Generico: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 48
    Top = 296
  end
  object IBQ_Ejecuta_SP_biCheck: TIBQuery
    Database = Database1
    Transaction = Transaccion1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 312
  end
end
