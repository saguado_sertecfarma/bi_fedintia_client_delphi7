unit BI_DM;

interface

uses
  SysUtils, Classes, FIBDatabase, UFIBModificados, DB, FIBDataSet,
  FIBTableDataSet, FIBQuery, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdFTP, IBCustomDataSet, IBQuery, IBDatabase, IBTable;

type
  TDataModule1 = class(TDataModule)
    IdFTP1: TIdFTP;
    Database1: TIBDatabase;
    Transaccion1: TIBTransaction;
    IBQ_BI: TIBQuery;
    IBQ_MA: TIBQuery;
    IBTS_BI: TIBTable;
    IBQ_LA: TIBQuery;
    IBTS_BIBI_COMPTADOR: TSmallintField;                                      
    IBTS_BIBI_ANTECODI_FARMACIA: TIBStringField;
    IBTS_BIBI_CODI_FARMACIA: TIBStringField;
    IBTS_BIBI_DATA_ENVIADA: TDateTimeField;
    IBTS_BIBI_DATA_ENVIO: TDateTimeField;
    IBTS_BIBI_FTP_ADRECA: TIBStringField;
    IBTS_BIBI_FTP_PORT: TSmallintField;
    IBTS_BIBI_FTP_USUARI: TIBStringField;
    IBTS_BIBI_FTP_USUARI_PASSW: TIBStringField;
    IBTS_BIBI_LLOC_ENVIAMENT: TSmallintField;
    IBTS_BIBI_DIA_ENVIAMENT: TSmallintField;
    IBTS_BIBI_HORA_ENVIAMENT: TIBStringField;
    IBTS_BIBI_TIPUS_ENVIAMENT: TIBStringField;
    IBTS_BIBI_DIRECTORI_DESTI: TIBStringField;
    IBTS_BIBI_ACTIU: TIBStringField;
    IBTS_BIBI_ENVIAMENT_FEDE: TIBStringField;
    IBQ_Ejecuta_SP: TIBQuery;
    IBTS_IMS: TIBTable;
    IBTS_IMSIMS_COMPTADOR: TSmallintField;
    IBTS_IMSIMS_ANTECODI_FARMACIA: TIBStringField;
    IBTS_IMSIMS_CODI_FARMACIA: TIBStringField;
    IBTS_IMSIMS_DATA_ENVIADA: TDateTimeField;
    IBTS_IMSIMS_DATA_ENVIO: TDateTimeField;
    IBTS_IMSIMS_FTP_ADRECA: TIBStringField;
    IBTS_IMSIMS_FTP_PORT: TSmallintField;
    IBTS_IMSIMS_FTP_USUARI: TIBStringField;
    IBTS_IMSIMS_FTP_USUARI_PASSW: TIBStringField;
    IBTS_IMSIMS_LLOC_ENVIAMENT: TSmallintField;
    IBTS_IMSIMS_DIA_ENVIAMENT: TSmallintField;
    IBTS_IMSIMS_TIPUS_ENVIAMENT: TIBStringField;
    IBTS_IMSIMS_DIRECTORI_DESTI: TIBStringField;
    IBTS_IMSIMS_ACTIU: TIBStringField;
    IBTS_IMSIMS_ENVIAMENT_FEDE: TIBStringField;
    IBTS_IMSIMS_HORA_ENVIAMENT: TIBStringField;
    IBQ_IMS: TIBQuery;
    IBQ_Generico: TIBQuery;
    IBQ_Ejecuta_SP_biCheck: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    DIR_TEMPORAL:String;      // Directori de fitxers temporals
  end;

var
  DataModule1: TDataModule1;

implementation

{$R *.dfm}

end.
