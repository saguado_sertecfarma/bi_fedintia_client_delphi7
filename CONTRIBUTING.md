# BI

23/08/2018 Javier
En construcción

IMPORTANTE: No trabajar sobre master!!

## En el momento de trabajar:

   * En master solo versiones para producción ya versionadas. 
   * Trabajar en rama Develop, para uso interno 
   * Trabajar en rama Release, para desarrollo especÍfico
   * Trabajar en rama 'HotFIX + Descripción del fix'

## En el momento de realizar commit:

   * Al realizar commit, asegurarse que solo se incluyen los ficheros que interesan.
   * Realizar siempre un comentario descriptivo y constructivo

## Etiquetas:

   * De momento para versiones finales, en master

**Consensuar el .ignoregit y preguntar antes de editar.**

Tenemos una pequeña guía en el repositorio, bajar para consultar: 
IOFWIN GIT.docx

***Antes de trabajar si no se esta seguro preguntar.***

Gracias!!