unit BI_Generator;
// Javier 26/01/2017
// Encargado de generar el fichero BI
// Dos modalidades
// Modo = 1 Semanal  Desactivada
// Modo = 2 Diaria
interface

 uses Dialogs, Controls, DateUtils, SysUtils, UDF, Math, Variants, Forms, StrUtils;

 var
    NomFitxerBI, DirNomFitxerBI,
    NomFitxerBIZIP, DirNomFitxerBIZIP,
    DirNomFitxerAenviar                :string;
    dDiaInicial                        :Tdate ;
    dDiaFinal                          :Tdate ;
    lHayError                          :Boolean;

 procedure P1;
 Function BI_Generar(Modo :Integer): Boolean;
// Function LimpiaCaracteres(Cadena: String): string;
// Function Eje_SP_Ret(cadena:string):variant;
// Function obtenerSiglas (cadena : string) : string;

 implementation

 uses BI_DM, INI_Read, BI, FTP1, BI_Functions, CALCULATE;

 procedure P1;
 begin
      ShowMessage('Test BI');
 end;


 function BI_Generar(Modo :Integer): Boolean;
 var
    DesdeAAAAMMDD, FinsAAAAMMDD,
    cCodiFarmacia, cCodiBotiquin                                :String;
    SS,Any,MM,DD, cCadena, cCRLF                                :String;
    nRegistros                                                  :Integer;
    nSemanaTransmessa,nAnnyInicial, nAnnyFinal                  :integer;
    Fitxer                                                      :textFile;
    Data_Operac,
    Codi_Article, msgerror,
    UNI_VEN_CON_REC,VENTAS_CON_REC, UNI_VEN_SIN_REC,
    VENTAS_SIN_REC,UNI_COMPRA,
    CODI_PROVEIDOR,NOM_PROV,
    STOCK,P_COST,PVP_FITXA,
    PVP_VENDA,CODI_EAN13,NOM_PROD,
    FEDINTIA_CLIENT,CODI_FIDELITZACIO,CODI_PRESCRITO,
    CODI_VDA_CREUADA, CODI_USUARI,CODI_CATEGORIA,
    CODI_LABORATORI,TIPUS_PRODUCTE,
    PTOS_EXTRA,EUROS_EXTRA, ID_Operacion, Hora_Operacio,
    Tipus_Venda, Num_Empresa, Nom_Empresa, Porcen_IVA,
    CODI_CATEGORIA_IMS,
    PUNTS_VENDA, PUNTS_BESCANVIATS_VDA, CODI_FAMILIA,
    DESCRIPCIO_FAMILIA, CODI_SUBFAMILIA,
    DESCRIPCIO_SUBFAMILIA, DESCRIPCIO_LABORATORI,
    NOM_CLIENT, CODI_CLIENT, NOM_EMPLEAT, LINEAL                :string;
    lContinuar                                                  :Boolean;
    n,i                                                         :integer;
    cDiaIni, cDiaFin                                            :string;
    CODI_FAMILIAvariant                                         :Variant;
    aSemana                                                     :Word;
    dDiaInicial                                                 :Tdate;
    dDiaFinal                                                   :Tdate;
    LINEAL_ANCHO, LINEAL_PROFUNDIDAD,
    PUC, PMC, PC_FIFO,
    DESC_TIPUS_PRODUCTE,
    RECARGO_EQUIVALENCIA, MARGEN_COMERCIAL                      :String;
    cEstadoBI, JSGTEST                                          :string;
    CODI_SUPERFAMILIA, DESCRIPCIO_SUPERFAMILIA                  :string;
    NUM_LINIA                                                   :string;
    NUM_LINIA_INT                                               :INTEGER;
    fpuc                                                        :Double;
    provDirecIP01, provDirecIP02, proveedorCIF                  :string;
    codProv                                                     :integer;
    //SERGIO AGUADO, 11/05/2021. A�ADIR UN ULTIMO REGISTRO AL FINAL DEL FICHERO
    //CON CONTADORES Y SUMATORIOS
    registroCTRL                                                   :string;
    contLineasVenta,contLineasCompra,contUnidsVenta,contUnidsCompra:integer;
    contRegistros                                               :Integer;
    importeVentas                                               :double;
    //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
    PVP_ONLINE                                                  :string;

    //SERGIO AGUADO, 11/05/2021. CALCULO CONTADORES Y SUMATORIOS DEL REGISTRO DE CONTROL.
    procedure IncrementarContadores;
    begin
       //Contador registros
       Inc( contRegistros );
       //Sumatorio unidades venta
       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger <0 then begin
          contUnidsVenta := contUnidsVenta + Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger);
          importeVentas  := importeVentas + (Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger) * BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat);
       end else begin
          contUnidsVenta := contUnidsVenta + Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger);
          importeVentas  := importeVentas + (Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger) * BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat);
       end;
       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger <0 then begin
          contUnidsVenta := contUnidsVenta + Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger);
          importeVentas  := importeVentas + (Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger) * BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat);
       end else begin
          contUnidsVenta := contUnidsVenta + Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger);
          importeVentas  := importeVentas + (Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger) * BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat);
       end;
       //Sumatorio unidades compra
       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger <0 then
          contUnidsCompra := contUnidsCompra + Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger)
       else
          contUnidsCompra := contUnidsCompra + Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger);
       //Contador lineas compra y contador lineas venta
       if BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger <> 0 then
          Inc(contLineasCompra)
       else
          Inc(contLineasVenta);
    end;

    //SERGIO AGUADO, 11/05/2021. A�ADIR UN ULTIMO REGISTRO AL FINAL DEL FICHERO
    //CON CONTADORES Y SUMATORIOS
    procedure InsertarRegistroControl;
    var any,mes,dia,hora,minu,segu,mili: Word;
    begin
      DecodeDate(now, any, mes, dia);
      DecodeTime(now, hora, minu, segu, mili);
      cCadena := StringOfChar('0',9)+
                 IntToStr(any)+PadL(IntToStr(mes),2,'0')+PadL(IntToStr(dia),2,'0')+
                 PadL(IntToStr(hora),2,'0')+PadL(IntToStr(minu),2,'0')+
                 StringOfChar('0',1)+
                 StringOfChar('0',2)+
                 StringOfChar('0',20)+
                 StringOfChar('0',6)+
                 StringOfChar('0',4)+
                 StringOfChar('0',1)+
                 StringOfChar('0',4)+
                 StringOfChar('0',1)+
                 StringOfChar('0',4)+
                 StringOfChar('0',5)+
                 StringOfChar('0',30)+
                 StringOfChar('0',5)+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringOfChar('0',13)+
                 //Aprovechamos la columna NOM_PROD para meter todos los contadores y sumatorios
                  FormatFloat('0000000000',contRegistros)+
                  FormatFloat('0000000000',contLineasVenta)+
                  FormatFloat('0000000000',contLineasCompra)+
                  FormatFloat('0000000000',contUnidsVenta)+
                  FormatFloat('0000000000',contUnidsCompra)+
                  StringOfChar(' ',50)+
                 //--
                 StringOfChar('0',1)+
                 StringOfChar('0',13)+
                 StringOfChar('0',6)+
                 StringOfChar('0',6)+
                 StringOfChar('0',1)+
                 StringOfChar('0',5)+
                 StringOfChar('0',4)+
                 StringOfChar('0',2)+
                 StringOfChar('0',4)+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringReplace(FormatFloat('00.00',0), ',','.',[])+
                 StringReplace(FormatFloat('00.00',0), ',','.',[])+
                 StringOfChar('0',6)+
                 StringOfChar('0',6)+
                 StringOfChar('0',6)+
                 StringOfChar('0',2)+
                 StringOfChar('0',30)+
                 StringOfChar('0',2)+
                 StringOfChar('0',30)+
                 StringOfChar('0',2)+
                 StringOfChar('0',30)+
                 StringOfChar('0',40)+
                 StringOfChar('0',6)+
                 StringOfChar('0',30)+
                 StringOfChar('0',20)+
                 StringOfChar('0',1)+
                 StringOfChar('0',3)+
                 StringOfChar('0',3)+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringReplace(FormatFloat('0000.00',0), ',','.',[])+
                 StringOfChar('0',30)+
                 StringReplace(FormatFloat('00.00',0), ',','.',[])+
                 StringOfChar('0',40)+
                 StringOfChar('0',40)+
                 StringOfChar('0',10)+
                 'S' //Esta columna nos indica si se trata del registro control o no (por defecto NO)
      ;
      Write(Fitxer, cCadena + cCRLF);
    end;

begin
      Result   := False;
      cCRLF    := #13#10;
      //SERGIO AGUADO, CIERRO A VER SI AS� REFRESCA LOS DATOS.
      BI_DM.DataModule1.IBTS_BI.Close;
      BI_DM.DataModule1.IBTS_BI.Open;

    //dDiaInicial      := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime;
      dDiaInicial      := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime+1;

      if(Modo=3)then
      begin
           dDiaInicial := BI_Form1.fecha;
      end;

      cEstadoBI := 'Preparando datos BI ' + FormatDateTime('dd/mm/yyy', dDiaInicial);
      BI.ActualizaInformacion(cEstadoBI);

      bi.BI_Form1.PB_BI.Position:= 0;
      bi.BI_Form1.PB_BI.StepIt;
      {   Modos Posibles
          1 Semanal  <-- DEPRECATED
          2 Diario}

      cCodiFarmacia :=(BI_DM.DataModule1.IBTS_BIBI_CODI_FARMACIA.AsString);
      cCodiFarmacia := StringReplace(cCodiFarmacia,'S','',[]);
      if PRUEBAS then cCodiFarmacia := PRUEBAS_NOM_FICHERO_CODIGO_SOCIO;



      cCodiBotiquin := '000';
      cCodiBotiquin := BIBotiquin;
      case Modo of
          1:begin
                 dDiaInicial      := StartOfTheWeek( dDiaInicial);  // Coge el primer dia de la semana a enviar
                 dDiaFinal        := dDiaInicial + 6;
                 nAnnyInicial  := YearOf( dDiaInicial ) mod 1000;

                 aSemana := WeekOfTheYear(dDiaInicial);

                 Any := StrZero(YearOf(StartOfTheWeek(dDiaInicial)) mod 1000,2); //A�o que pertenecen los datos
                 SS  := StrZero(WeekOfTheYear(dDiaInicial),2);                   //Semana que pertenecen los datos

                 If  YearOf(dDiaInicial)<>YearOf(dDiaFinal) Then
                 Begin
                      If ASemana = 1 Then //Los ultimos x dias del a�o, pertenecen a la semana 1 del siguiente, ya que el 1 de Enero es Jueves o anterior
                      Begin
                           Any := StrZero(YearOf(dDiaFinal) mod 1000,2);         //A�o que pertenecen los datos
                           SS := '01';
                      end;
                 end;

                 NomFitxerBI := 'BI' + VERSION_BI_PRO + '_'+ cCodiFarmacia + cCodiBotiquin +'_' + Any +SS + '.TXT';
            end;
          2:begin
                 dDiaFinal        := dDiaInicial; //enviamos el mismo dia

                 if (BIModo = 99) then
                 begin
                    dDiaFinal := trunc(now);
                    dDiaInicial := trunc(now);
                 end;

                 Any :=   StrZero(YearOF(dDiaInicial),4);
                 MM  :=   StrZero(MonthOf(dDiaInicial),2);
                 DD  :=   StrZero(DayOf(dDiaInicial),2);

                 NomFitxerBI := 'BI' + VERSION_BI_PRO + '_' +  cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.TXT';
                 NomFitxerBIZIP := 'BI' + VERSION_BI_PRO + '_' +  cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.ZIP';
            end;
          3:begin
                 dDiaFinal        := dDiaInicial; //enviamos el mismo dia

                 Any :=   StrZero(YearOF(dDiaInicial),4);
                 MM  :=   StrZero(MonthOf(dDiaInicial),2);
                 DD  :=   StrZero(DayOf(dDiaInicial),2);

                 NomFitxerBI := 'BI' + VERSION_BI_PRO + '_' +  cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.TXT';
                 NomFitxerBIZIP := 'BI' + VERSION_BI_PRO + '_' +  cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.ZIP';
            end;
      end;
      //Para los dos modos es el mismo nombre
      DirNomFitxerBI := INI_Read.DIR_IMS + NomFitxerBI;
      DirNomFitxerBIZIP := INI_Read.DIR_IMS + NomFitxerBIZIP;

      //Inicio
      begin
           Try
              DeleteFile(INI_Read.DIR_IMS + NomFitxerBI);
           Except
              BILog('No se ha podido eliminar ' + NomFitxerBI);
           End;
           BI.ActualizaInformacion(NomFitxerBI + ' Procesando datos BI');

           bi.BI_Form1.PB_BI.StepIt;


           BI_DM.DataModule1.IBQ_BI.Close;
           BI_DM.DataModule1.IBQ_BI.SQL.Clear;
           BI_DM.DataModule1.IBQ_BI.SQL.Add('SELECT ID_OPERACION, DATA_OPERAC, TIPUS_VENDA, NUM_EMPRESA, NOM_EMPRESA, CODI_ARTICLE,UNI_VEN_CON_REC,VENTAS_CON_REC,UNI_VEN_SIN_REC,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('VENTAS_SIN_REC, UNI_COMPRA, CODI_PROVEIDOR, NOM_PROV, STOCK, P_COST, PVP_FITXA, PVP_VENDA, CODI_EAN13, NOM_PROD, FEDINTIA_CLIENT, CODI_FIDELITZACIO,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('CODI_PRESCRITO, CODI_VDA_CREUADA, CODI_USUARI, CODI_LABORATORI, CODI_CATEGORIA, TIPUS_PRODUCTE, PTOS_EXTRA, EUROS_EXTRA, PORCEN_IVA, CODI_CATEGORIA_IMS,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('PUNTS_VENDA, PUNTS_BESCANVIATS_VDA, CODI_FAMILIA, DESCRIPCIO_FAMILIA, CODI_SUBFAMILIA, DESCRIPCIO_SUBFAMILIA, DESCRIPCIO_LABORATORI,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('CODI_CLIENT, NOM_CLIENT, NOM_EMPLEAT, LINEAL,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('LINEAL_ANCHO, LINEAL_PROFUNDIDAD, PUC, PMC, PC_FIFO, DESC_TIPUS_PRODUCTE,');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('RECARGO_EQUIVALENCIA, MARGEN_COMERCIAL, CODI_SUPERFAMILIA, DESCRIPCIO_SUPERFAMILIA, PVP_ONLINE');
           //BI_DM.DataModule1.IBQ_BI.SQL.Add('NUM_LINIA ');
           BI_DM.DataModule1.IBQ_BI.SQL.Add('FROM IMS_NEW_7("'+FormatDateTime('dd.mm.yyyy',dDiaInicial)+'","'+FormatDateTime('dd.mm.yyyy 23:59:59',dDiaFinal)+'")');

           try
              BI_DM.DataModule1.IBQ_BI.Open;
           except
                 On E:Exception do
                 begin
                      BILog(DirNomFitxerBI + E.Message);
                 end;
           end;

           lContinuar := true;
           //n := 0;
           if (BI_DM.DataModule1.IBQ_BI.RecordCount > 0 ) then
           begin
                AssignFile(Fitxer, INI_Read.DIR_IMS + NomFitxerBI);
                Rewrite(Fitxer);

                contRegistros    := 0;
                contLineasVenta  := 0;
                contLineasCompra := 0;
                contUnidsVenta   := 0;
                contUnidsCompra  := 0;
                importeVentas    := 0;

                while ((not BI_DM.DataModule1.IBQ_BI.EOF) AND (lContinuar)) do
                begin
                    //Inc(n);
                    //Try construcci�n linea para el fichero
                    try



                       ID_Operacion       :=   FormatFloat('000000000', BI_DM.DataModule1.IBQ_BI.FieldByName('ID_OPERACION').AsInteger);
                       Data_Operac        :=   FormatDateTime('YYYYMMDD', BI_DM.DataModule1.IBQ_BI.FieldByName('DATA_OPERAC').AsDateTime);
                       Hora_Operacio      :=   FormatDateTime('HHNN', BI_DM.DataModule1.IBQ_BI.FieldByName('DATA_OPERAC').AsDateTime);

                       Tipus_Venda        :=   BI_DM.DataModule1.IBQ_BI.FieldByName('TIPUS_VENDA').AsString;
                       Num_Empresa        :=   FormatFloat('00',BI_DM.DataModule1.IBQ_BI.FieldByName('NUM_EMPRESA').AsInteger);
                       Nom_Empresa        :=   PadR(Trim(UpperCase(BI_DM.DataModule1.IBQ_BI.FieldByName('NOM_EMPRESA').AsString)),20,' ');
                       Nom_Empresa        :=   LimpiaCaracteres(Nom_Empresa);

                       Codi_Article       :=   FormatFloat('000000',BI_DM.DataModule1.IBQ_BI.FieldByName('CODI_ARTICLE').AsInteger);


                       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger <0 then
                          UNI_VEN_CON_REC :=   FormatFloat('000',Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger))
                       else
                          UNI_VEN_CON_REC :=   FormatFloat('0000',Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_CON_REC').AsInteger));

                       VENTAS_CON_REC :=   StringReplace(FormatFloat('0',BI_DM.DataModule1.IBQ_BI.FieldByName('VENTAS_CON_REC').AsInteger),'-','',[]);

                       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger <0 then
                          UNI_VEN_SIN_REC  :=   FormatFloat('000',Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger))
                       else
                          UNI_VEN_SIN_REC  :=   FormatFloat('0000',Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_VEN_SIN_REC').AsInteger));

                       VENTAS_SIN_REC      :=   StringReplace(FormatFloat('0',BI_DM.DataModule1.IBQ_BI.FieldByName('VENTAS_SIN_REC').AsInteger), '-','',[]);

                       IF BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger <0 then
                          UNI_COMPRA          :=   FormatFloat('000',Max(-999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger))
                       else
                          UNI_COMPRA          :=   FormatFloat('0000',Min(9999, BI_DM.DataModule1.IBQ_BI.FieldByName('UNI_COMPRA').AsInteger));

                       CODI_PROVEIDOR      :=   FormatFloat('00000',BI_DM.DataModule1.IBQ_BI.FieldByName('CODI_PROVEIDOR').AsInteger);
                       NOM_PROV            :=   PadR(Trim(UpperCase(BI_DM.DataModule1.IBQ_BI.FieldByName('NOM_PROV').AsString)), 30, ' ');
                       NOM_PROV            :=   LimpiaCaracteres(NOM_PROV);

                       IF BI_DM.DataModule1.IBQ_BI.FieldByName('STOCK').AsInteger < 0 THEN
                          STOCK            :=   FormatFloat('0000',  Max(-9999, BI_DM.DataModule1.IBQ_BI.FieldByName('STOCK').AsInteger) )
                       ELSE
                          STOCK            :=   FormatFloat('00000', Min(99999, BI_DM.DataModule1.IBQ_BI.FieldByName('STOCK').AsInteger)    );

                       if BI_DM.DataModule1.IBQ_BI.FieldByName('P_COST').AsFloat < 0 THEN
                          P_COST := StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.IBQ_BI.FieldByName('P_COST').AsFloat)), ',','.',[])
                       else
                          P_COST := StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.IBQ_BI.FieldByName('P_COST').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_FITXA').AsFloat < 0 THEN
                          PVP_FITXA := StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_FITXA').AsFloat)), ',','.',[])
                       else
                          PVP_FITXA := StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_FITXA').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat < 0 THEN
                          PVP_VENDA           :=   StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.IBQ_BI.FieldByName('PVP_VENDA').AsFloat)), ',','.',[])
                       else
                          PVP_VENDA           :=   StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PVP_VENDA').AsFloat)), ',','.',[]);

                       CODI_EAN13          :=   PadL(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_EAN13').AsString)), 13, '0');
                       NOM_PROD            :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('NOM_PROD').AsString)), 100, ' ');
                       NOM_PROD            :=   LimpiaCaracteres(NOM_PROD);

                       FEDINTIA_CLIENT     :=   BI_DM.DataModule1.ibq_bi.FieldByName('FEDINTIA_CLIENT').AsString;

                       //ORIGINAL
                       //CODI_FIDELITZACIO   :=   Padl(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_FIDELITZACIO').AsString)), 13, '0');
                       //Javier 21/08/2017, Por codigo interno de Rosa Roig,
                       //CODI_FIDELITZACIO   :=   Padl(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_FIDELITZACIO').AsString)), 2, '0');

                       CODI_FIDELITZACIO := LimpiaCaracteres(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_FIDELITZACIO').AsString)));
                       CODI_FIDELITZACIO := TrimLeft(CODI_FIDELITZACIO); //El TRIM NORMAL NO ELIMINA 2 ESPACIOS DE LA IZQUIERDA
                       CODI_FIDELITZACIO := PADL(CODI_FIDELITZACIO, 2, '0');
                       CODI_FIDELITZACIO := CODI_FIDELITZACIO + '00000000000';

                       //Javier 22/11/2018
                       try
                          CODI_PRESCRITO      :=   FormatFloat('000000',BI_DM.DataModule1.ibq_bi.FieldByName('CODI_PRESCRITO').AsInteger);
                       except
                          CODI_PRESCRITO      := '000000';
                       end;

                       //Javier 22/11/2018
                       try
                          CODI_VDA_CREUADA    :=   FormatFloat('000000',BI_DM.DataModule1.ibq_bi.FieldByName('CODI_VDA_CREUADA').AsInteger);
                       except
                          CODI_VDA_CREUADA    := '000000;'
                       end;

                       CODI_USUARI         :=   PadL(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_USUARI').AsString)), 1, '0');
                       CODI_LABORATORI     :=   FormatFloat('00000',BI_DM.DataModule1.ibq_bi.FieldByName('CODI_LABORATORI').AsInteger);
                       CODI_CATEGORIA      :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_CATEGORIA').AsString)), 4, ' ');
                       CODI_CATEGORIA      :=   stringreplace(CODI_CATEGORIA, #0, ' ', [rfReplaceAll]);

                       TIPUS_PRODUCTE      :=   FormatFloat('00',BI_DM.DataModule1.ibq_bi.FieldByName('TIPUS_PRODUCTE').AsInteger);

                       if BI_DM.DataModule1.ibq_bi.FieldByName('PTOS_EXTRA').AsInteger < 0 THEN
                           PTOS_EXTRA           :=   FormatFloat('000',Max(-999,BI_DM.DataModule1.ibq_bi.FieldByName('PTOS_EXTRA').AsInteger))
                       ELSE
                           PTOS_EXTRA           :=   FormatFloat('0000',Min(9999, BI_DM.DataModule1.ibq_bi.FieldByName('PTOS_EXTRA').AsInteger));

                       if BI_DM.DataModule1.ibq_bi.FieldByName('EUROS_EXTRA').AsFloat < 0 THEN
                           EUROS_EXTRA           :=   StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.ibq_bi.FieldByName('EUROS_EXTRA').AsFloat)), ',','.',[])
                       ELSE
                           EUROS_EXTRA           :=   StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('EUROS_EXTRA').AsFloat)), ',','.',[]);

                       Porcen_IVA                :=   StringReplace(FormatFloat('00.00',Min(99.99, BI_DM.DataModule1.ibq_bi.FieldByName('Porcen_IVA').AsFloat)), ',','.',[]);
                       RECARGO_EQUIVALENCIA      :=   StringReplace(FormatFloat('00.00',Min(99.99, BI_DM.DataModule1.ibq_bi.FieldByName('RECARGO_EQUIVALENCIA').AsFloat)), ',','.',[]);

                       CODI_CATEGORIA_IMS        := PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_CATEGORIA_IMS').AsString)), 6, ' ');

                       if BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_VENDA').AsInteger < 0 THEN
                           PUNTS_VENDA           :=   FormatFloat('00000',Max(-99999,BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_VENDA').AsInteger))
                       ELSE
                           PUNTS_VENDA           :=   FormatFloat('000000',Min(999999, BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_VENDA').AsInteger));

                       if BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_BESCANVIATS_VDA').AsInteger < 0 THEN
                           PUNTS_BESCANVIATS_VDA           :=   FormatFloat('00000',Max(-99999,BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_BESCANVIATS_VDA').AsInteger))
                       ELSE
                           PUNTS_BESCANVIATS_VDA           :=   FormatFloat('000000',Min(999999, BI_DM.DataModule1.ibq_bi.FieldByName('PUNTS_BESCANVIATS_VDA').AsInteger));


                       CODI_SUPERFAMILIA        :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_SUPERFAMILIA').AsString)), 2, ' ');
                       CODI_SUPERFAMILIA        :=   stringreplace(CODI_SUPERFAMILIA, #0, ' ', [rfReplaceAll]);

                       DESCRIPCIO_SUPERFAMILIA  :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('DESCRIPCIO_SUPERFAMILIA').AsString)), 30, ' ');
                       DESCRIPCIO_SUPERFAMILIA  :=   LimpiaCaracteres(DESCRIPCIO_SUPERFAMILIA);


                       CODI_FAMILIA             :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_FAMILIA').AsString)), 2, ' ');
                       codi_familia             :=   stringreplace(codi_familia, #0, ' ', [rfReplaceAll]);

                       CODI_FAMILIAvariant      := Eje_SP_Ret('Select CODI_FAMILIA as RETORN From ARTICLES_MESTRE Where CODI_ARTICLE=' + QuotedStr(Codi_Article) );

                       DESCRIPCIO_FAMILIA       :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('DESCRIPCIO_FAMILIA').AsString)), 30, ' ');
                       DESCRIPCIO_FAMILIA       :=   LimpiaCaracteres(DESCRIPCIO_FAMILIA);

                       CODI_SUBFAMILIA          :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('CODI_SUBFAMILIA').AsString)), 2, ' ');
                       CODI_SUBFAMILIA          :=   stringreplace(CODI_SUBFAMILIA, #0, ' ', [rfReplaceAll]);

                       DESCRIPCIO_SUBFAMILIA    :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('DESCRIPCIO_SUBFAMILIA').AsString)), 30, ' ');
                       DESCRIPCIO_SUBFAMILIA    :=   LimpiaCaracteres(DESCRIPCIO_SUBFAMILIA);

                       DESCRIPCIO_LABORATORI    :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('DESCRIPCIO_LABORATORI').AsString)), 40, ' ');
                       DESCRIPCIO_LABORATORI    :=   LimpiaCaracteres(DESCRIPCIO_LABORATORI);

                       CODI_CLIENT       :=   FormatFloat('000000',BI_DM.DataModule1.ibq_bi.FieldByName('CODI_CLIENT').AsInteger);
                       NOM_CLIENT        :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('NOM_CLIENT').AsString)), 30, ' ');
                       NOM_CLIENT        :=   ObtenerSiglas(NOM_CLIENT);
                       NOM_CLIENT        :=   PadR(NOM_CLIENT, 30, ' ');
                       NOM_CLIENT        :=   LimpiaCaracteres(NOM_CLIENT);

                       if ((CODI_FAMILIAvariant = null) OR
                           (not (CODI_FAMILIA[1] in ['0'..'9','A'..'Z','a'..'z'])))
                       then
                       Begin
                            CODI_FAMILIA := '  ';
                            CODI_SUBFAMILIA := '  ';
                       end;
                       CODI_SUBFAMILIA := LimpiaCaracteres(CODI_SUBFAMILIA);     //16/10/2017

                       NOM_EMPLEAT       :=    PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('NOM_EMPLEAT').AsString)), 20, ' ');
                       NOM_EMPLEAT       :=    LimpiaCaracteres(NOM_EMPLEAT);
                       LINEAL            :=    PadL(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL').AsString)), 1, ' ');

                       if BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_ANCHO').AsInteger < 0 THEN
                           LINEAL_ANCHO           :=   FormatFloat('00',Max(-999,BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_ANCHO').AsInteger))
                       ELSE
                           LINEAL_ANCHO           :=   FormatFloat('000',Min(999, BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_ANCHO').AsInteger));

                       if BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_PROFUNDIDAD').AsInteger < 0 THEN
                           LINEAL_PROFUNDIDAD           :=   FormatFloat('00',Max(-999,BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_PROFUNDIDAD').AsInteger))
                       ELSE
                           LINEAL_PROFUNDIDAD           :=   FormatFloat('000',Min(999, BI_DM.DataModule1.ibq_bi.FieldByName('LINEAL_PROFUNDIDAD').AsInteger));

                       //07/11/2017
                       fpuc := BI_DM.DataModule1.ibq_bi.FieldByName('PUC').AsFloat;
                       if fpuc < 0 THEN
                          PUC := StringReplace(FormatFloat('000.00',Max(-999.99, fpuc)), ',','.',[])
                       else
                          PUC := StringReplace(FormatFloat('0000.00',Min(9999.99, fpuc)), ',','.',[]);

//                       if BI_DM.DataModule1.ibq_bi.FieldByName('PUC').AsFloat < 0 THEN
//                          PUC := StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PUC').AsFloat)), ',','.',[])
//                       else
//                          PUC := StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PUC').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.ibq_bi.FieldByName('PMC').AsFloat < 0 THEN
                          PMC := StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PMC').AsFloat)), ',','.',[])
                       else
                          PMC := StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PMC').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.ibq_bi.FieldByName('PC_FIFO').AsFloat < 0 THEN
                          PC_FIFO := StringReplace(FormatFloat('000.00',Max(-999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PC_FIFO').AsFloat)), ',','.',[])
                       else
                          PC_FIFO := StringReplace(FormatFloat('0000.00',Min(9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PC_FIFO').AsFloat)), ',','.',[]);

                       DESC_TIPUS_PRODUCTE    :=   PadR(Trim(UpperCase(BI_DM.DataModule1.ibq_bi.FieldByName('DESC_TIPUS_PRODUCTE').AsString)), 30, ' ');
                       DESC_TIPUS_PRODUCTE    :=   LimpiaCaracteres(DESC_TIPUS_PRODUCTE);
                       MARGEN_COMERCIAL       :=   StringReplace(FormatFloat('00.00',Min(99.99, BI_DM.DataModule1.ibq_bi.FieldByName('MARGEN_COMERCIAL').AsFloat)), ',','.',[]);

                       //NUM_LINIA_INT := BI_DM.DataModule1.ibq_bi.FieldByName('NUM_LINIA').AsInteger;
                       {
                       IF (NUM_LINIA_INT > 9999) THEN
                       BEGIN
                            NUM_LINIA := INTTOSTR(NUM_LINIA_INT);
                            NUM_LINIA := right(NUM_LINIA,4);
                       end
                       ELSE
                       BEGIN
                            NUM_LINIA := FormatFloat('0000',BI_DM.DataModule1.ibq_bi.FieldByName('NUM_LINIA').AsInteger);
                       end;
                       }
                       try
                          codProv := BI_DM.DataModule1.IBQ_BI.FieldByName('CODI_PROVEIDOR').AsInteger;
                          if (codProv = 4) then
                          begin
//                               ShowMessage('alto');
                          end;
                       except
                           codProv := 0;
                       end;

                       provDirecIP01 := PadR(Trim(UpperCase(dameDatoProveedor(codProv, 1))), 40, ' ');
                       provDirecIP02 := PadR(Trim(UpperCase(dameDatoProveedor(codProv, 2))), 40, ' ');
                       proveedorCIF  := PadR(Trim(UpperCase(dameDatoProveedor(codProv, 3))), 10, ' ');

                       //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
                       if BI_DM.DataModule1.ibq_bi.FieldByName('PVP_ONLINE').AsFloat < 0 THEN
                          PVP_ONLINE := StringReplace(FormatFloat('0000.00',Max(-9999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PVP_ONLINE').AsFloat)), ',','.',[])
                       else
                          PVP_ONLINE := StringReplace(FormatFloat('00000.00',Min(99999.99, BI_DM.DataModule1.ibq_bi.FieldByName('PVP_ONLINE').AsFloat)), ',','.',[]);
                       //SERGIO AGUADO, 11/05/2021. PARA INDICAR SI SE TRATA DE UN REGISTRO DE CONTROL O NO
                       //registroCtrl  := 'N'; //Esta columna nos indica si se trata del registro control o no (por defecto NO)


                    //except try construcci�n linea para el fichero
                    except
                          on e:exception do
                          begin
                               BILog(E.Message);
                               BILog('Error Operacion ' + ID_Operacion + ', con fecha ' + Data_Operac + ', CN ' + Codi_Article );
                               //msgerror   :=    e.message;
                               lContinuar :=    False;
                               lHayError  :=    True;

                               //ShowMessage('Error en Operacion: ' + ID_Operacion + ', con fecha ' + Data_Operac + ', para el codigo '+ codi_article);
                               //ShowMessage(IntToStr(n));  //Linea de error
                          end;
                    end;

                    if ( not lHayError) then
                    begin
                         //Javier 29/07/2016 Orden salida del fichero
                         cCadena :=  ID_Operacion
                                   + Data_Operac
                                   + Hora_Operacio
                                   + Tipus_Venda
                                   + Num_Empresa
                                   + Nom_Empresa
                                   + Codi_Article
                                   + UNI_VEN_CON_REC
                                   + VENTAS_CON_REC
                                   + UNI_VEN_SIN_REC
                                   + VENTAS_SIN_REC
                                   + UNI_COMPRA
                                   + CODI_PROVEIDOR
                                   + NOM_PROV
                                   + STOCK
                                   + P_COST
                                   + PVP_FITXA
                                   + PVP_VENDA
                                   + CODI_EAN13
                                   + NOM_PROD
                                   + FEDINTIA_CLIENT
                                   + CODI_FIDELITZACIO
                                   + CODI_PRESCRITO
                                   + CODI_VDA_CREUADA
                                   + CODI_USUARI
                                   + CODI_LABORATORI
                                   + CODI_CATEGORIA
                                   + TIPUS_PRODUCTE
                                   + PTOS_EXTRA
                                   + EUROS_EXTRA
                                   + Porcen_IVA
                                   + RECARGO_EQUIVALENCIA
                                   + CODI_CATEGORIA_IMS
                                   + PUNTS_VENDA
                                   + PUNTS_BESCANVIATS_VDA
                                   + CODI_SUPERFAMILIA
                                   + DESCRIPCIO_SUPERFAMILIA
                                   + CODI_FAMILIA
                                   + DESCRIPCIO_FAMILIA
                                   + CODI_SUBFAMILIA
                                   + DESCRIPCIO_SUBFAMILIA
                                   + DESCRIPCIO_LABORATORI
                                   + CODI_CLIENT
                                   + NOM_CLIENT
                                   + NOM_EMPLEAT
                                   + LINEAL
                                   + LINEAL_ANCHO
                                   + LINEAL_PROFUNDIDAD
                                   + PUC
                                   + PMC
                                   + PC_FIFO
                                   + DESC_TIPUS_PRODUCTE
                                   + MARGEN_COMERCIAL
//                                 + NUM_LINIA
                                   + provDirecIP01
                                   + provDirecIP02
                                   + proveedorCIF
                                   //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
                                   + PVP_ONLINE
                                   //SERGIO AGUADO 11/05/2021. COLUMNA QUE INDICA SI ES EL REGISTRO DE CONTROL O NO.
                                   //+ registroCtrl
                                   ;

                         i := Length(cCadena);

                    //SERGIO AGUADO, 21/05/2021. MUEVO ESTE END MAS ABAJO. SI EL PROCESO A FALLADO NO QUEREMOS QUE SE HAGA TAMPOCO NADA DE LO DE ABAJO
                    //end;

//                       if (i = 660) then
                         //SERGIO AGUADO 11/05/2021. A�ADIR UN ULTIMO REGISTRO DE CONTROL AL FINAL DEL FICHERO.
                         //if (i = 661) then
                         //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
                         if (i = 668) then
                         begin
                              Write(Fitxer, cCadena + cCRLF);

                              //SERGIO AGUADO, 11/05/2021.
                              //Incremento contadores y sumatorios justo despu�s de guardar la linea del fichero.
                              //Ya que si no se llega a guardar no deber�a contar.
                              IncrementarContadores;
                         end;

                         BI_DM.DataModule1.ibq_bi.Next;

                         if IsDeveloper then
                         begin
                              //If (i <> 660) then
                              //SERGIO AGUADO 11/05/2021. A�ADIR UN ULTIMO REGISTRO DE CONTROL AL FINAL DEL FICHERO.
                              //if (i <> 661) then
                              //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
                              If (i <> 668) then
                              begin
                                   ShowMessage('Cadena de: ' + IntToStr(i));
                              end;
                         end;
                    end; //if if not lhayError

                end; //fin while

                bi.BI_Form1.PB_BI.StepIt;

                CloseFile( Fitxer );

                //SERGIO AGUADO, 25/05/2021. METO TODO EN UN IF, SI EL PROCESO FALL� EN ALG�N PUNTO NO DEBE GENERARSE EL FICHERO NI ENVIARSE.
                if not lHayError then
                begin
                    //SERGIO AGUADO, 11/05/2021. A�ADIR UN ULTIMO REGISTRO DE CONTROL AL FINAL DEL FICHERO.
                    //CON CONTADORES Y SUMATORIOS
                    //InsertarRegistroControl;

                    BI.ActualizaInformacion(NomFitxerBI + ' Fichero Generado');
                    AssignFile(Fitxer, '');

                    //if (lBIenZIP) then
                    if (BIEnvioZIP = 'S') then
                    begin
                         ComprimirFicheroZIP(DirNomFitxerBI,DirNomFitxerBIZIP );
                         DirNomFitxerAEnviar:= DirNomFitxerBIZIP;
                    end
                    else
                    begin
                         DirNomFitxerAEnviar := DirNomFitxerBI;
                    end;

                    //SERGIO AGUADO. 11/05/2021. ENVIAR?.
                    if (not PRUEBAS) or (PRUEBAS and PRUEBAS_ENVIAR)then
                    begin
                      //SERGIO AGUADO, 13/05/2021, ENVIAR FICHEROS POR HTTPS EN LUGAR DE POR FTP
                      //if(FTP_FEDE_SEND(DirNomFitxerAEnviar)) then
                      if HTTPS_FEDE_SEND( DirNomFitxerAEnviar, contRegistros, importeVentas ) then
                      begin
                           Result:= True;
                      end
                      else
                      begin
                           lHayError := True;
                      end;
                    end
                    else
                    begin
                      BILog('NO SE ENVIAN DATOS. CONSTANTES PRUEBAS=TRUE,  PRUEBAS_ENVIAR=FALSE!!!');
                      Result:=True;
                    end;

                    if (BI_DM.DataModule1.IBQ_BI.RecordCount > 0 ) then
                    begin
                         if(BI.BI_Day_Auto) then
                         begin
                              //Sleep(10000);
                              //Sleep(50000);
                              //SERGIO AGUADO, 12/05/2021
                              Sleep(5000);
                         end;
                    end;
                end
                else
                begin
                    BI.ActualizaInformacion(NomFitxerBI + ' Error generando fichero');
                end;

           end
           else //else if hay datos para enviar
           begin
                //Javier 26/04/2017
                //Enga�amos el Result, no se envia si no hay registros, pero se graba como procesado
                Result:= True;
                cEstadoBI := 'No existen movimentos ' + FormatDateTime('dd.mm.yyyy',dDiaInicial);
                BILog( cEstadoBI );
                BI.ActualizaInformacion(cEstadoBI);
           end;

      end;
      //Fin
 end;

end.
