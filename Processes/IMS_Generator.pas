unit IMS_Generator;

interface

uses Dialogs, Controls, DateUtils, SysUtils, UDF, Math, Variants, Forms, BI_DM, Classes;

 var
    dDiaInicial                   :Tdate ;
    dDiaFinal                     :Tdate ;
    lHayError                     :Boolean;


function GenerarIMS(DIR_TEMPORAL, DIR_IMS:String):Boolean;

implementation

//SERGIO AGUADO. 11/05/2021. A�ADIDA UNIT BI.
uses BI, BI_Functions, FTP1, IBQuery;


function GenerarIMS(DIR_TEMPORAL, DIR_IMS:String):Boolean;
var List1:TStringList;
var cCadena                                                     :String;
var cProveidor1,cProveidor2,cProveidor3,cProveidor4,cProveidor5 :String;
var nRegistros                                                  :Integer;
var Fitxer                                                      :textFile;
var ss,any                                                      :String;
var DesdeAAAAMMDD,FinsAAAAMMDD,cCodiFarmacia                    :String;
var cCodigo                                                     :string;
////////////////////////////////////////////////////////////////////////////////
var nSemanaActual,nAnnyActual        : integer;
var nSemanaTransmessa,nAnnyTransmess : integer;
var nPuestoConfigurado               : integer;
var xml                              :TstringList;
var cprecio, sData, cT, sImport, sImportV, sStock :string;
var nStock, iP                       :Integer;
  cVar1, cVar2, cVar3, cVar4, cVar5, cVar6, cVar7, cVar8, cVar9: string;
  i, nTipoEAN: Integer;
  cCadena9999999:string;
  cCRLF:string;
  NomFitxerIMS:String;
begin
  //If FileExists(dir_temporal+'imsFede.txt') Then cCRLF := #13#10;
    // NOTA: Germ�n (c�digo para pasar a IOFConnect)
    Result := False;
    If EmptyString(DataModule1.IBTS_IMSIMS_CODI_FARMACIA.asString) Then Exit;


    // generem fitxer ims amb format nou
    dDiaInicial      := DataModule1.ibts_IMSIMS_DATA_ENVIADA.AsDateTime + 7;
    dDiaInicial      := BoW( dDiaInicial);  // Coge el primer dia de la semana a enviar
    dDiaFinal        := dDiaInicial                                  + 6;

    nAnnyTransmess   := Year  ( dDiaInicial )-2000;

    nRegistros    := 0;
//    cCodiFarmacia := Ajusta(DM_Main.Fibts_IMSIMS_CODI_FARMACIA.asString,'I',6,'0');
    cCodiFarmacia :=(DataModule1.ibts_IMSIMS_CODI_FARMACIA.asString);//'CCCCC';
    if PRUEBAS then cCodiFarmacia := PRUEBAS_NOM_FICHERO_CODIGO_SOCIO;
    Any           := StrZero(nAnnyTransmess,2);                   //A�o que pertenecen los datos
    SS            := StrZero(WeekNr(dDiaFinal),2);                //Semana que pertenecen los datos
    NomFitxerIMS  := 'F'+cCodiFarmacia + Any +'.'+SS;

    {
    If ( (not FileExists(DM_Main.DIR_IMS + NomFitxerIMS)       or
         ((FileExists(DM_Main.DIR_IMS + NomFitxerIMS)) and
          (application.MessageBox(PChar(sFicheroGenerado),'',MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2)=ID_YES) )))
    Then
    }
    Begin
      Try
        DeleteFile(DIR_IMS + NomFitxerIMS);
      Except
      End;
      DataModule1.IBQ_IMS.Close;
      DataModule1.IBQ_IMS.SQL.Clear;
      DataModule1.IBQ_IMS.SQL.Add('select CODI_ARTICLE,UNI_VEN_CON_REC,VENTAS_CON_REC,UNI_VEN_SIN_REC,VENTAS_SIN_REC,');
      DataModule1.IBQ_IMS.SQL.Add('    UNI_COM_PROV1,UNI_COM_PROV2,UNI_COM_PROV3,UNI_COM_PROV4,UNI_COM_PROV5,');
      DataModule1.IBQ_IMS.SQL.Add('    NOM_PROV1,NOM_PROV2,NOM_PROV3,NOM_PROV4,NOM_PROV5,NUM_COM_PROV1,');
      DataModule1.IBQ_IMS.SQL.Add('    NUM_COM_PROV2,NUM_COM_PROV3,NUM_COM_PROV4,NUM_COM_PROV5,STOCK,PVP,');
      DataModule1.IBQ_IMS.SQL.Add('    NOM_PROD,PVP_V,CODI_PROD,DATA_TRANS from ims_new_3("'+FormatDateTime('dd.mm.yyyy',dDiaInicial)+'","'+FormatDateTime('dd.mm.yyyy 23:59:59',dDiaFinal)+'")');
      DataModule1.IBQ_IMS.Open;

      Application.ProcessMessages;
      AssignFile(Fitxer,DIR_IMS + NomFitxerIMS);
      Rewrite(Fitxer);

      // creamos registro tipo 1
      cCadena := '1' + DataModule1.ibts_IMSIMS_ANTECODI_FARMACIA.AsString + cCodiFarmacia + FormatDateTime('YYYYMMDD', dDiaInicial) + FormatDateTime('YYYYMMDD', dDiaFinal) + 'E' + space(233);
      Writeln(Fitxer,cCadena);

      // creamos registros tipo 2
      while not DataModule1.IBQ_IMS.EOF do
      begin
        try
          for i := 1 to 9 do
          begin
            if DataModule1.IBQ_IMS.Fields[i].AsInteger < 0 then
            begin
              case i of
                1: cVar1 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                2: cVar2 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                3: cVar3 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                4: cVar4 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                5: cVar5 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                6: cVar6 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                7: cVar7 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                8: cVar8 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
                9: cVar9 := '-' + Ajusta(IntToStr(Abs(DataModule1.IBQ_IMS.Fields[i].AsInteger)), 'I', 3, '0');
              end
            end else
            begin
              case i of
                1: cVar1 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                2: cVar2 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                3: cVar3 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                4: cVar4 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                5: cVar5 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                6: cVar6 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                7: cVar7 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                8: cVar8 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
                9: cVar9 := StrZero(DataModule1.IBQ_IMS.Fields[i].AsInteger, 4);
              end;
            end;
          end;

          if DataModule1.IBQ_IMS.Fields[20].AsInteger < 0 then
            sStock := '0000'
          else
          if DataModule1.IBQ_IMS.Fields[20].AsInteger > 9999 then
            sStock := '9999'
          else
            sStock := DataModule1.IBQ_IMS.Fields[20].AsString;

          sImport := SUBSTR(FormatFloat('0000.00',DataModule1.IBQ_IMS.Fields[21].AsFloat),1,4)+
                     SUBSTR(FormatFloat('0000.00',DataModule1.IBQ_IMS.Fields[21].AsFloat),6,2);

          sImportV := SUBSTR(FormatFloat('0000.00',DataModule1.IBQ_IMS.Fields[23].AsFloat),1,4)+
                      SUBSTR(FormatFloat('0000.00',DataModule1.IBQ_IMS.Fields[23].AsFloat),6,2);

          //___________________________________________________________________
          if (DataModule1.IBQ_IMS.Fields[0].AsString >= '600000') then
          begin
            cCodigo        := '000000' + DameEan7(DataModule1.IBQ_IMS.Fields[0].AsString);
            cCadena9999999 := '847000' + DameEan7(DataModule1.IBQ_IMS.Fields[0].AsString);
          end
          else
          begin
            // mirar si es ean8 o ean13
            if Length(FloatToStr(strtofloat(DataModule1.IBQ_IMS.Fields[24].AsString))) = 8 then
              nTipoEAN := 8
            else
              nTipoEAN := 13;

            if (DataModule1.IBQ_IMS.Fields[24].AsString > '0000000999999') then
            begin
              if ValidaEAN13(DataModule1.IBQ_IMS.Fields[24].AsString, nTipoEAN) then
              begin
                cCodigo := DataModule1.IBQ_IMS.Fields[24].AsString;
                cCadena9999999 := DataModule1.IBQ_IMS.Fields[24].AsString;
              end else
              begin
                cCodigo := DataModule1.IBQ_IMS.Fields[24].AsString;
                cCadena9999999 := '0000000999999';
              end;
            end else
            begin
              cCodigo := '000000' + DameEan7(DataModule1.IBQ_IMS.Fields[0].AsString);
              cCadena9999999 := DataModule1.IBQ_IMS.Fields[24].AsString;
            end;
          end;
          //____________________________________________________________________

          if DataModule1.IBQ_IMS.Fields[25].AsDateTime <> 0 then
            sData := FormatDateTime('YYYYMMDD', DataModule1.IBQ_IMS.Fields[25].AsDateTime)
          else
            sData := '00000000';
        except
          sData := '00000000';
        end;

        cCadena := '2' + Ajusta(cCodigo, 'I', 13, '0') +                         // codi article
                      cVar1 +         // unitats venudes amb recepta
                      cVar2 +         // vendes amb recepta
                      cVar3 +         // unitats venudes sense recepta
                      cVar4 +         // vendes sense recepta
                      cVar5 +         // unitats comprades proveidor
                      cVar6 +         // unitats comprades proveidor
                      cVar7 +         // unitats comprades proveidor
                      cVar8 +         // unitats comprades proveidor
                      cVar9 +         // unitats comprades proveidor
                      Ajusta(DataModule1.IBQ_IMS.Fields[10].AsString, 'D', 20, ' ') +       // nom proveidor
                      Ajusta(DataModule1.IBQ_IMS.Fields[11].AsString, 'D', 20, ' ') +       // nom proveidor
                      Ajusta(DataModule1.IBQ_IMS.Fields[12].AsString, 'D', 20, ' ') +       // nom proveidor
                      Ajusta(DataModule1.IBQ_IMS.Fields[13].AsString, 'D', 20, ' ') +       // nom proveidor
                      Ajusta(DataModule1.IBQ_IMS.Fields[14].AsString, 'D', 20, ' ') +       // nom proveidor
                      StrZero(DataModule1.IBQ_IMS.Fields[15].AsInteger, 2) +        // num compres proveidor
                      StrZero(DataModule1.IBQ_IMS.Fields[16].AsInteger, 2) +        // num compres proveidor
                      StrZero(DataModule1.IBQ_IMS.Fields[17].AsInteger, 2) +        // num compres proveidor
                      StrZero(DataModule1.IBQ_IMS.Fields[18].AsInteger, 2) +        // num compres proveidor
                      StrZero(DataModule1.IBQ_IMS.Fields[19].AsInteger, 2) +        // num compres proveidor
                      StrZero(StrToInt(sStock), 4) +                               // stock
                      sImport +                                                    // pvp
                      Ajusta(DataModule1.IBQ_IMS.Fields[22].AsString, 'D', 60, ' ') +       // nom producte
                      sImportV +                                                 // pvp venda
                      Ajusta(cCadena9999999              , 'I', 13, '0') +       // codi producte
                      sData;                                                     // data
        Writeln(Fitxer,cCadena+cCRLF);

        Inc(nRegistros);
        DataModule1.IBQ_IMS.Next;
      end;
      // creamos registro tipo 3
      cCadena := '9' + Ajusta(IntToStr(nRegistros), 'I', 5, '0') + space(251);
      Writeln(Fitxer,cCadena+cCRLF);
      CloseFile(Fitxer);

      //SERGIO AGUADO. 11/05/2021. ENVIAR?.
      if (not PRUEBAS) or (PRUEBAS and PRUEBAS_ENVIAR)then
      begin
        if (FTP_FEDE_SEND(DIR_IMS + NomFitxerIMS, False)) then
        begin
             Result := True;
             DataModule1.IBTS_IMS.Open;
             DataModule1.IBTS_IMS.Edit;
             DataModule1.IBTS_IMS.FieldByName('IMS_DATA_ENVIADA').AsDateTime := dDiaInicial;
             DataModule1.IBTS_IMS.Post;
        end;
      end
      else
      begin
        BILog('NO SE ENVIAN DATOS. CONSTANTES PRUEBAS=TRUE,  PRUEBAS_ENVIAR=FALSE!!!');
        Result := True;
      end;
    end;
end;

end.
