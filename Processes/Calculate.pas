unit Calculate;

interface

uses Controls, UDF, SysUtils, Dialogs, ExtCtrls;

var
BIEnvioZIP                                 :string;

procedure Week_Calculate(Modo :integer);
procedure Day_Calculate(Modo :integer);

{         Modos Posibles
          1 Semanal
          2 Diario
}

implementation

uses BI_DM, BI_Generator, BI, Forms, MA_Generator, BI_Functions, INI_READ;

procedure Week_Calculate(Modo :integer);
var
   FechaEnviada, DiaFinal, IniSemACtual       :TDate;
   BIActivo, BI_ENVIAMENT_FEDE, cEstadoBI    :STRING;

begin
     //SERGIO AGUADO, 28/05/2021. REFRESCAR.
     BI_DM.DataModule1.IBTS_BI.Close;
     BI_DM.DataModule1.IBTS_BI.Open;
     FechaEnviada := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime+7;

     //SERGIO AGUADO, 26/05/2010. A�ADO CONTROL PARA QUE NO PUEDAN PROCESAR FECHAS INFERIORES A 01/01/2019
     if Fechaenviada<StrToDateTime('01.01.2019') then Fechaenviada := StrToDateTime('01.01.2019');

     FechaEnviada := BoW(FechaEnviada);
     IniSemACtual := trunc(BoW(Now));

     BIActivo := 'N';
     BIActivo := BI_DM.DataModule1.IBTS_BIBI_ACTIU.AsString;

   //SERGIO AGUADO, 25/05/2021. (NOT lHayError) hace referencia a BI_MA, pero el importante el BI_BI, utilizamos el de BI_MA podr�amos dejar sin enviar algun fichero de facturaci�n
   //While ((NOT lHayError) and (BIActivo = 'S') and ((FechaEnviada + 6) < IniSemACtual)) do
     While ((NOT BI_Generator.lHayError) and (BIActivo = 'S') and ((FechaEnviada + 6) < IniSemACtual)) do
     begin

          cEstadoBI := 'Preparando datos BI ' + FormatDateTime('dd/mm/yyy', FechaEnviada);
          BI.ActualizaInformacion(cEstadoBI);

          If NOT BI_Generator.lHayError Then
          Begin
               BI_Generator.BI_Generar(Modo);
               //SERGIO AGUADO, 25/05/2021. ENVIAR MA SOLO SI LA FACTURACION (BI) SE HA PODIDO ENVIAR
               if (NOT BI_Generator.lHayError) then
               begin
                 MA_GENERATOR.MA_Generar(Modo);
               end;

               //SERGIO AGUADO, 25/05/2021. CONTROLAR QUE NO HAY ERRORES ANTES DE GUARDAR LA FECHA_ENVIADA, SINO, EL FICHERO NO SE ENVIAR PERO LA FECHA SI SE ACTUALIZA Y ESA FACTURACI�N QUEDAR� SIN ENVIAR
               If NOT BI_Generator.lHayError Then
               begin
                  try
                     BI_DM.DataModule1.IBTS_BI.Open;
                     BI_DM.DataModule1.IBTS_BI.Edit;
                     //SERGIO, NO TIENE LOGICA GUARDAR EN DATA_ENVIADA UNA FECHA QUE NO SE HA ENVIADO.
                     //BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime:=FechaEnviada+7;
                     BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime:=FechaEnviada;
                     BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIO.AsDateTime:=Now;
                     BI_DM.DataModule1.IBTS_BI.Post;
                  except
                     on e:exception do
                     begin
                       BILog('Errores actualizando fecha de envio ( BI_CONTROL.BI_DATA_ENVIO y BI_CONTROL.BI_DATA_ENVIADA ).');
                       raise;
                     end;
                  end;
               end;
          End;
          FechaEnviada := BoW(BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime+7);
     end;

     if ((FechaEnviada + 6) > (IniSemACtual)) then
     begin
          BI.ActualizaInformacion('Proceso Finalizado');
     end;
end;

procedure Day_Calculate(Modo :integer);
var
   FechaEnviada, DiaFinal, IniSemACtual, Hoy      :TDate;
   BIActivo, BI_ENVIAMENT_FEDE, cEstadoBI,
   CodiFarmacia                                   :string;
   //SERGIO, 07/06/2021, ESTO YA NO ES NECESARIO
   //TiempoCodiFarmacia                             :integer;
   TimerBIEScalonado                              :TTimer;
begin
     //SERGIO AGUADO, 28/05/2021. REFRESCAR.
     BI_DM.DataModule1.IBTS_BI.Close;
     BI_DM.DataModule1.IBTS_BI.Open;
     FechaEnviada := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime+1;

     //SERGIO AGUADO, 26/05/2010. A�ADO CONTROL PARA QUE NO PUEDAN PROCESAR FECHAS INFERIORES A 01/01/2019
     if Fechaenviada<StrToDateTime('01/01/2019 00:00:00') then Fechaenviada := StrToDateTime('01/01/2019 00:00:00');

     Hoy := Trunc(Now);
     lHayError := False;
     BIActivo := 'N';
     BIActivo := BI_DM.DataModule1.IBTS_BIBI_ACTIU.AsString;
     BIEnvioZIP := 'N';

     //zip
     TRY
        //BIEnvioZIP := BI_DM.DataModule1.IBTS_BIBI_ENVIO_ZIP.AsString;
        BIEnvioZIP := Eje_SP_Ret('Select BI_ENVIO_ZIP as RETORN From BI_CONTROL' );
     EXCEPT
        BIEnvioZIP := 'N';
     END;

     if(BIEnvioZIP = '') then
     begin
          BIEnvioZIP := 'N';
     end;

     // Envio Escalonado
     CodiFarmacia := BI_DM.DataModule1.IBTS_BIBI_CODI_FARMACIA.AsString;
     CodiFarmacia := StringReplace(CodiFarmacia,'S','',[]);

     if (CodiFarmacia = '     ') then
     begin
          BI.ActualizaInformacion('BI sin c�digo de socio.');
          BILog('BI sin c�digo de socio.');
          Sleep(2000);
          exit;
     end;

     case BIModo of
       1,3:begin
                if (modo = 2)then
                begin
                     // Llamada desde BI Click o 00:30
                     //SERGIO AGUADO, 25/05/2021. (NOT lHayError) hace referencia a BI_MA, pero el importante el BI_BI, utilizamos el de BI_MA podr�amos dejar sin enviar algun fichero de facturaci�n
                     //While ((NOT lHayError) and (BIActivo = 'S') and (FechaEnviada < (Hoy )) and (NOT BI_Form1.Salida)) do
                     While ((NOT BI_Generator.lHayError) and (BIActivo = 'S') and (FechaEnviada < (Hoy )) and (NOT BI_Form1.Salida)) do
                     begin
                          //if(BI_Form1.Salida = False) then
                          begin
                               If NOT BI_Generator.lHayError Then
                               Begin
                                    BI_Generator.BI_Generar(Modo);
                                    //SERGIO AGUADO, 25/05/2021. LO MISMO, lHayErrores HACIA REFERENCIA A BI_MA, ESTABA MAL
                                    //if ((FechaEnviada = (hoy-1)) and (NOT lHayError)) then
                                    if ((FechaEnviada = (hoy-1)) and (NOT BI_Generator.lHayError)) then
                                    begin
                                         MA_Generator.MA_Generar(Modo);
                                    end;

                                    If NOT BI_Generator.lHayError Then
                                    begin
                                       try
                                          BI_DM.DataModule1.IBTS_BI.Open;
                                          BI_DM.DataModule1.IBTS_BI.Edit;
                                          //SERGIO, NO TIENE LOGICA GUARDAR EN DATA_ENVIADA UNA FECHA QUE NO SE HA ENVIADO.
                                          //BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime := FechaEnviada +1;
                                          BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime := FechaEnviada;
                                          BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIO.AsDateTime:=Now;
                                          BI_DM.DataModule1.IBTS_BI.Post;
                                          BI_DM.DataModule1.IBTS_BI.Transaction.CommitRetaining;
                                       except
                                          on e:exception do
                                          begin
                                            BILog('Errores actualizando fecha de envio ( BI_CONTROL.BI_DATA_ENVIO y BI_CONTROL.BI_DATA_ENVIADA ).');
                                            raise;
                                          end;
                                       end;
                                    end;
                               End;
                               FechaEnviada := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime +1;

                               //SERGIO AGUADO, 25/05/2021. QUE QUEDE CLARO A QUE lHayErrores hacemos referencia
                               //if ((lHayError) or (BI_Generator.lHayError) ) then
                               if ((MA_Generator.lHayError) or (BI_Generator.lHayError) ) then
                               begin
                                    BILog('Errores en el proceso.');
                                    exit;
                               end;
                          end;
                     end;
                end;
                if (modo = 3) then //Javier 28/11/2018 Vamos a enviar un d�a
                begin
                     BI_Generator.BI_Generar(Modo);
                end;
           end;
         2:begin
                //SERGIO, 07/06/2021, ESTO YA NO ES NECESARIO.
                 (*//Envio Escalonado
                 TiempoCodiFarmacia := StrToInt(CodiFarmacia) * 25;
                 if IsDeveloper then
                 begin
                   TiempoCodiFarmacia := 5000; //5s
                 end;
                 //SERGIO, 04/06/2021, PARA CODIGOS DE FARMACIA MUY GRANDES PODEMOS LLEGAR A TENER UN SLEEP DE 41 MINUTOS. LO LIMITO A 10 MIN.
                 if ( ((TiempoCodiFarmacia/1000)/60)>10) then
                    TiempoCodiFarmacia := 600000; //10 min.
                 //SERGIO. 04/06/2021, SI TENEMOS EL FICHERO PONGO UN SLEEP DE SOLO 5s.
                 if FileExists( '.\binosleep.txt' ) then
                    TiempoCodiFarmacia := 5000; //5s
                 BILog('Esperamos '+FloatToStr((TiempoCodiFarmacia/1000)/60)+' minutos (para no coincidir todas las farmacias enviando).');
                 Sleep(TiempoCodiFarmacia);
                 *)

                //SERGIO AGUADO, 25/05/2021. (NOT lHayError) hace referencia a BI_MA, pero el importante el BI_BI, utilizamos el de BI_MA podr�amos dejar sin enviar algun fichero de facturaci�n
                //While ((NOT lHayError) and (BIActivo = 'S') and (FechaEnviada < (Hoy ))) do
                While ((NOT BI_Generator.lHayError) and (BIActivo = 'S') and (FechaEnviada < (Hoy ))) do
                begin
                     If NOT BI_Generator.lHayError Then
                     Begin
                          BI_Generator.BI_Generar(Modo);
                          //SERGIO AGUADO, 25/05/2021. LO MISMO, lHayErrores HACIA REFERENCIA A BI_MA, ESTABA MAL
                          //if ((FechaEnviada = (hoy-1)) and (NOT lHayError)) then
                          if ((FechaEnviada = (hoy-1)) and (NOT BI_Generator.lHayError)) then
                          begin
                               MA_Generator.MA_Generar(Modo);
                          end;

                          If NOT BI_Generator.lHayError Then
                          begin
                            try
                               BI_DM.DataModule1.IBTS_BI.Open;
                               BI_DM.DataModule1.IBTS_BI.Edit;
                               //SERGIO, NO TIENE LOGICA GUARDAR EN DATA_ENVIADA UNA FECHA QUE NO SE HA ENVIADO.
                               //BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime := FechaEnviada +1;
                               BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime := FechaEnviada;
                               BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIO.AsDateTime:=Now;
                               BI_DM.DataModule1.IBTS_BI.Post;
                            except
                               on e:exception do
                               begin
                                 BILog('Errores actualizando fecha de envio ( BI_CONTROL.BI_DATA_ENVIO y BI_CONTROL.BI_DATA_ENVIADA ).');
                                 raise;
                               end;
                            end;
                          end;
                          BI_DM.DataModule1.IBTS_BI.Transaction.CommitRetaining;

                          //SERGIO AGUADO, 25/05/2021. QUE QUEDE CLARO A QUE lHayErrores hacemos referencia
                          //if ((lHayError) or (BI_Generator.lHayError) ) then
                          if ((MA_Generator.lHayError) or (BI_Generator.lHayError) ) then
                          begin
                               BILog('Errores en el proceso.');
                               exit;
                          end;

                     End;
                     FechaEnviada := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime +1;
                end;
           end;
        99:begin  //El resto que no estan controlados, para el cierre, iofwin nos envia su handle
                if((BIActivo = 'S') and (trunc(FechaEnviada) = Hoy )) then
                begin
                     BI_Day_Auto := False;  //Sin tiempo de espera
                     If NOT BI_Generator.lHayError Then
                     Begin
                          BI_Generator.BI_Generar(Modo);
                          MA_Generator.MA_Generar(Modo);
                          EnviarMSNIOFWIN(HandleIOFWIN);
                          //enviamos mensaje a iofwin
                     End;
                end;
           end;

     end;

     if(BIActivo = 'N') then
     begin

          BI.ActualizaInformacion('BI NO ACTIVADO');
          Sleep(2000);
          //BI_Form1.Close;
     end;

     if ((FechaEnviada ) >= (Hoy)) then
     begin
          BI.ActualizaInformacion('Proceso Finalizado');

          //Javier 13/04/2017, eliminamos ficheros
          try
             //BorrarArchivosBI;      Pet
          finally
             Sleep(2000);
             //BI_Form1.Close;
          end
     end;
end;

end.


