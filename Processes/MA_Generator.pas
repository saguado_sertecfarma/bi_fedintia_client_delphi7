unit MA_Generator;
// Javier 26/01/2017
// Encargado de generar el fichero MA

interface


 uses Dialogs, Controls, DateUtils, SysUtils, UDF, Math, Variants, Forms;

 var
    NomFitxerMA, DirNomFitxerMA,
    NomFitxerMAZIP, DirNomFitxerMAZIP,
    DirNomFitxerAEnviar                     :string;
    lHayError, lMAenZIP                     :Boolean;
 procedure P2;
 Function MA_Generar(Modo :Integer): Boolean;
 Function MA_Maestro_articulos(Tipo :Integer): Boolean;
 Function MA_Lista_articulos(Tipo :Integer): Boolean;

implementation

 uses BI_DM, INI_Read, BI, FTP1, BI_Functions, calculate;

 procedure P2;
 begin
      ShowMessage('Test MA');
 end;

 Function MA_Generar(Modo :Integer): Boolean;
 var
    SS,Any,MM,DD, cCadena, cCRLF                                :String;
    cCodiFarmacia, cCodiBotiquin                                :String;
    dDiaInicial                                                 :Tdate;
    dDiaFinal                                                   :Tdate;
    Fitxer                                                      :textFile;
    lContinuar                                                  :Boolean;
    i                                                           :Integer;
    CODI_ARTICLE, NUMERO_LISTA, DESCRIPCION_LISTA,
    STOCK, CODI_SINONIMO,
    DATA_ULTIMA_VENTA , DATA_ULTIMA_COMPRA, CABECERA_MA,
    CABECERA_LA, LINEAL, LINEAL_ANCHO, LINEAL_PROFUNDIDAD       :string;
    msgerror                                                    :string;
    //SERGIO AGUADO, 11/05/2021.
    //CABECERA_CTRL                                             :string;
    contRegistros                                               :integer;
    //SERGIO AGUADO, 07/06/2021. A�ADIR PVP_ONLINE
    PVP, PVP_COSTE_ULTIMO, PVP_ONLINE                           :string;
 begin
      lMAenZIP := False;
      Result := False;
      cCRLF := #13#10;
      CABECERA_MA := '1';
      CABECERA_LA := '2';
      //SERGIO AGUADO, 11/05/2021. A�ADIR REGISTRO DE CONTROL.
      //CABECERA_CTRL := '9';

      //SERGIO AGUADO, 28/05/2021, REFRESCAR.
      BI_DM.DataModule1.IBTS_BI.Close;
      BI_DM.DataModule1.IBTS_BI.Open;

      dDiaInicial      := BI_DM.DataModule1.IBTS_BIBI_DATA_ENVIADA.AsDateTime+1;

      cCodiFarmacia :=(BI_DM.DataModule1.IBTS_BIBI_CODI_FARMACIA.AsString);
      cCodiFarmacia := StringReplace(cCodiFarmacia,'S','',[]);
      if PRUEBAS then cCodiFarmacia := PRUEBAS_NOM_FICHERO_CODIGO_SOCIO;

      cCodiBotiquin := '000';
      cCodiBotiquin := BIBotiquin;

      dDiaFinal        := dDiaInicial; //enviamos el mismo dia

      Any :=   StrZero(YearOF(dDiaInicial),4);
      MM  :=   StrZero(MonthOf(dDiaInicial),2);
      DD  :=   StrZero(DayOf(dDiaInicial),2);

      NomFitxerMA := 'MA'+ VERSION_MA_PRO + '_' +cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.TXT';
      NomFitxerMAZIP := 'MA' + VERSION_MA_PRO + '_' +  cCodiFarmacia + cCodiBotiquin + '_' + Any + MM + DD + '.ZIP';
            //Para los dos modos es el mismo nombre
      DirNomFitxerMA := INI_Read.DIR_IMS + NomFitxerMA;
      DirNomFitxerMAZIP := INI_Read.DIR_IMS + NomFitxerMAZIP;


      bi.BI_Form1.PB_BI.Position:= 0;
      bi.BI_Form1.PB_BI.StepIt;

      //SERGIO AGUADO, 11/05/2021. CALUCLO DEL NUMERO DE REGISTROS PARA EL ENVIO POR HTTPS.
      contRegistros := 0;

      begin

           Try
              DeleteFile(INI_Read.DIR_IMS + NomFitxerMA);
           Except
              BILog('No se ha podido eliminar el fichero ' + NomFitxerMA);
           End;
           BI.ActualizaInformacion( NomFitxerMA + ' Procesando datos BI MA ' );
           // BI_MA
           BI_DM.DataModule1.IBQ_MA.Close;
           BI_DM.DataModule1.IBQ_MA.SQL.Clear;
           BI_DM.DataModule1.IBQ_MA.SQL.Add('SELECT CODI_ARTICLE, STOCK, DATA_ULTIMA_VENTA, DATA_ULTIMA_COMPRA, CODI_SINONIMO, LINEAL, LINEAL_ANCHO, LINEAL_PROFUNDIDAD, P_COSTE_ULTIMO, PVP, PVP_ONLINE ');
           BI_DM.DataModule1.IBQ_MA.SQL.Add('FROM BI_MA');

           BI_DM.DataModule1.IBQ_MA.Open;

           if (BI_DM.DataModule1.IBQ_MA.RecordCount > 0 ) then
           begin


               AssignFile(Fitxer, INI_Read.DIR_IMS + NomFitxerMA);
               Rewrite(Fitxer);



               lContinuar := true;


               while ((not BI_DM.DataModule1.IBQ_MA.EOF) AND (lContinuar)) do
               begin
                    //try contruir linea para guardar en fichero
                    try
                       Codi_Article       :=   FormatFloat('000000',BI_DM.DataModule1.IBQ_MA.FieldByName('CODI_ARTICLE').AsInteger);

                       IF BI_DM.DataModule1.IBQ_MA.FieldByName('STOCK').AsInteger < 0 THEN
                         STOCK            :=   FormatFloat('0000',  Max(-9999, BI_DM.DataModule1.IBQ_MA.FieldByName('STOCK').AsInteger) )
                       ELSE
                         STOCK            :=   FormatFloat('00000', Min(99999, BI_DM.DataModule1.IBQ_MA.FieldByName('STOCK').AsInteger)    );

                       DATA_ULTIMA_VENTA  :=   FormatDateTime('YYYYMMDD', BI_DM.DataModule1.IBQ_MA.FieldByName('DATA_ULTIMA_VENTA').AsDateTime);
                       DATA_ULTIMA_COMPRA :=   FormatDateTime('YYYYMMDD', BI_DM.DataModule1.IBQ_MA.FieldByName('DATA_ULTIMA_COMPRA').AsDateTime);
                       CODI_SINONIMO      :=   FormatFloat('000000',BI_DM.DataModule1.IBQ_MA.FieldByName('CODI_SINONIMO').AsInteger);
                       LINEAL             :=   PadL(Trim(UpperCase(BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL').AsString)), 1, ' ');

                       if BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_ANCHO').AsInteger < 0 THEN
                           LINEAL_ANCHO           :=   FormatFloat('00',Max(-999,BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_ANCHO').AsInteger))
                       ELSE
                           LINEAL_ANCHO           :=   FormatFloat('000',Min(999, BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_ANCHO').AsInteger));

                       if BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_PROFUNDIDAD').AsInteger < 0 THEN
                           LINEAL_PROFUNDIDAD     :=   FormatFloat('00',Max(-999,BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_PROFUNDIDAD').AsInteger))
                       ELSE
                           LINEAL_PROFUNDIDAD     :=   FormatFloat('000',Min(999, BI_DM.DataModule1.IBQ_MA.FieldByName('LINEAL_PROFUNDIDAD').AsInteger));

                       //SERGIO AGUADO, 07/06/2021. A�ADIR COLUMNA PVP_ONLINE AL MAESTRO.
                       if BI_DM.DataModule1.IBQ_MA.FieldByName('PVP_ONLINE').AsInteger < 0 THEN
                           PVP_ONLINE     :=   StringReplace(FormatFloat('0000.00',Max(-9999.99,BI_DM.DataModule1.IBQ_MA.FieldByName('PVP_ONLINE').AsFloat)), ',','.',[])
                       ELSE
                           PVP_ONLINE     :=   StringReplace(FormatFloat('00000.00',Min(99999.99, BI_DM.DataModule1.IBQ_MA.FieldByName('PVP_ONLINE').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.IBQ_MA.FieldByName('P_COSTE_ULTIMO').AsInteger < 0 THEN
                           PVP_COSTE_ULTIMO:=   StringReplace(FormatFloat('0000.00',Max(-9999.99,BI_DM.DataModule1.IBQ_MA.FieldByName('P_COSTE_ULTIMO').AsFloat)), ',','.',[])
                       ELSE
                           PVP_COSTE_ULTIMO:=   StringReplace(FormatFloat('00000.00',Min(99999.99, BI_DM.DataModule1.IBQ_MA.FieldByName('P_COSTE_ULTIMO').AsFloat)), ',','.',[]);

                       if BI_DM.DataModule1.IBQ_MA.FieldByName('PVP').AsInteger < 0 THEN
                           PVP :=   StringReplace(FormatFloat('0000.00',Max(-9999.99,BI_DM.DataModule1.IBQ_MA.FieldByName('PVP').AsFloat)), ',','.',[])
                       ELSE
                           PVP :=   StringReplace(FormatFloat('00000.00',Min(99999.99, BI_DM.DataModule1.IBQ_MA.FieldByName('PVP').AsFloat)), ',','.',[]);

                    //except try contruir linea
                    except
                         on e:exception do
                         begin
                              BILog(E.Message);
                              msgerror   :=    e.message;
                              lContinuar :=    False;
                              lHayError  :=    True;
                              //ShowMessage('Error en Articulo: ' + Codi_Article + ', con stock ' + STOCK + ', con sinonimo '+ CODI_SINONIMO);
                              //ShowMessage(IntToStr(n));  //Linea de error
                         end;
                    end;

                    if not lHayError then
                    begin
                        cCadena :=  CABECERA_MA
                                +   Codi_article
                                +   Stock
                                +   DATA_ULTIMA_VENTA
                                +   DATA_ULTIMA_COMPRA
                                +   CODI_SINONIMO
                                +   LINEAL
                                +   LINEAL_ANCHO
                                +   LINEAL_PROFUNDIDAD
                                //SERGIO AGUADO, 07/06/2021. A�ADIR COLUMNA PVP_ONLINE AL MAESTRO.
                                +   PVP_COSTE_ULTIMO
                                +   PVP
                                +   PVP_ONLINE
                                ;

                        i := Length(cCadena);
                        //if (i = 41) then
                        //SERGIO AGUADO, 07/06/2021. A�ADIR COLUMNA PVP_ONLINER, PVP y PVP_COSTE_ULTIMO AL MAESTRO.
                        if (i = 65) then
                        begin
                             Write(Fitxer, cCadena + cCRLF);
                             //SERGIO AGUADO, 11/05/2021. CALCULO DEL NUMJERO DE REGISTROS PARA EL ENVIO POR HTTPS.
                             Inc(contRegistros);
                        end;

                        BI_DM.DataModule1.IBQ_MA.Next;

                        if IsDeveloper then
                        begin
                             //If (i <> 34) then
                             //If (i <> 41) then
                             //SERGIO AGUADO, 07/06/2021. A�ADIR COLUMNA PVP_ONLINER, PVP y PVP_COSTE_ULTIMO AL MAESTRO.
                             if (i <> 65) then
                             begin
                                  ShowMessage('Cadena de Maestro de Articulos: ' + IntToStr(i));
                             end;
                        end;
                    end; //fin if not lHayError
               end; //Fin while

               bi.BI_Form1.PB_BI.StepIt;

               //Si se pudo generar el MA_BI...
               if not lHayError then
               begin

                   // BI_LA
                   BI.ActualizaInformacion( NomFitxerMA + ' Procesando datos BI LA ' );
                   BI_DM.DataModule1.IBQ_LA.Close;
                   BI_DM.DataModule1.IBQ_LA.SQL.Clear;
                   BI_DM.DataModule1.IBQ_LA.SQL.Add('SELECT CODI_ARTICLE, NUMERO_LISTA, DESCRIPCION_LISTA');
                   BI_DM.DataModule1.IBQ_LA.SQL.Add('FROM BI_LA');

                   BI_DM.DataModule1.IBQ_LA.Open;

                   lContinuar := true;

                   while ((not BI_DM.DataModule1.IBQ_LA.EOF) AND (lContinuar)) do
                   begin
                        try
                           Codi_Article       :=   FormatFloat('000000',BI_DM.DataModule1.IBQ_LA.FieldByName('CODI_ARTICLE').AsInteger);
                           NUMERO_LISTA       :=   FormatFloat('00000',BI_DM.DataModule1.IBQ_LA.FieldByName('NUMERO_LISTA').AsInteger);
                           DESCRIPCION_LISTA  :=   PadR(Trim(UpperCase(BI_DM.DataModule1.IBQ_LA.FieldByName('DESCRIPCION_LISTA').AsString)),30,' ');
                           DESCRIPCION_LISTA  :=   LimpiaCaracteres(DESCRIPCION_LISTA);
                        except
                             on e:exception do
                             begin
                                  BILog(E.Message);
                                  msgerror   :=    e.message;
                                  lContinuar :=    False;
                                  lHayError  :=    True;
                                  //ShowMessage('Error en Articulo: ' + Codi_Article + ', en lote ' + NUMERO_LISTA + ', con descripcion '+ DESCRIPCION_LISTA);
                                  //ShowMessage(IntToStr(n));  //Linea de error
                             end;
                        end;

                        if not lHayError then
                        begin
                             cCadena :=  CABECERA_LA
                                     +   Codi_article
                                     +   NUMERO_LISTA
                                     +   DESCRIPCION_LISTA;

                            i := Length(cCadena);
                            if (i = 42) then
                            begin
                                 Write(Fitxer, cCadena + cCRLF);
                                 //SERGIO AGUADO, 11/05/2021.  CALCULO DEL NUMJERO DE REGISTROS PARA EL ENVIO POR HTTPS.
                                 Inc(contRegistros);
                            end;

                            if IsDeveloper then
                            begin
                                 //If (i <> 471) then
                                 If (i <> 42) then
                                 begin
                                      ShowMessage('Cadena de Lista de Articulos de: ' + IntToStr(i));
                                 end;
                            end;

                            BI_DM.DataModule1.IBQ_LA.Next;
                        end;

                   end; //fin while BI_LA

                   //SERGIO AGUADO, 11/05/2021. A�ADIR REGISTRO DE CONTROL.
                   //cCadena :=  CABECERA_CTRL
                   //        +   PadL(IntToStr(contRegistros),6,'0');
                   //Write(Fitxer, cCadena + cCRLF);


                   bi.BI_Form1.PB_BI.StepIt;
                   CloseFile( Fitxer );

                   if not lHayError then
                   begin
                       BI.ActualizaInformacion( NomFitxerMA + ' Fichero Generado ' );

                       AssignFile(Fitxer, '');

                       //test
                       //lMAenZIP:= True;
                       if (BIEnvioZIP = 'S') then
                       begin
                            ComprimirFicheroZIP(DirNomFitxerMA,DirNomFitxerMAZIP );
                            DirNomFitxerAEnviar:= DirNomFitxerMAZIP;
                       end
                       else
                       begin
                            DirNomFitxerAEnviar := DirNomFitxerMA;
                       end;


                       //SERGIO AGUADO. 11/05/2021. ENVIAR?.
                       if (not PRUEBAS) or (PRUEBAS and PRUEBAS_ENVIAR)then
                       begin
                         //SERGIO AGUADO, 13/05/2021, ENVIAR FICHEROS POR HTTPS EN LUGAR DE POR FTP
                         //if( FTP_FEDE_SEND(DirNomFitxerAEnviar)) then
                         if HTTPS_FEDE_SEND( DirNomFitxerAEnviar, contRegistros, 0 ) then
                         begin
                              Result:= True;
                         end
                         else
                         begin
                              lHayError := True;
                         end;
                       end
                       else
                       begin
                         BILog('NO SE ENVIAN DATOS. CONSTANTES PRUEBAS=TRUE,  PRUEBAS_ENVIAR=FALSE!!!');
                         result := True;
                       end;
                   end
                   else //else if not lHayError BI_LA
                   begin
                       BI.ActualizaInformacion( NomFitxerMA + ' Error Generando Fichero ' );
                   end;
                   BI_DM.DataModule1.IBQ_LA.close;
               end
               else //else if not lHayError BI_MA
               begin
                   CloseFile( Fitxer );
                   bi.BI_Form1.PB_BI.StepIt;
                   BI.ActualizaInformacion( NomFitxerMA + ' Error Generando Fichero. ' );
               end;
           end
           else //else if sql tiene datos
           begin
                BI.ActualizaInformacion( 'No existen movimentos ');
           end;

           BI_DM.DataModule1.IBQ_MA.Close;
      end;
 end;

 Function MA_Maestro_articulos(Tipo :Integer): Boolean;
 begin

 end;

 Function MA_Lista_articulos(Tipo :Integer): Boolean;
 begin

 end;

end.
