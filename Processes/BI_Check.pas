//======================================================================================================================
//
// 27/05/2021
// FUNCIONES PARA REALIZAR LAS COMPARACIONES DE FACTURACI�N DE LA BD DE LA FARMACIA CONTRA LA BD DE BI
//
//======================================================================================================================
unit BI_Check;

interface

uses
   Forms, Classes, sysUtils, xmldom, XMLIntf, msxmldom, XMLDoc, StdCtrls, strUtils, variants, FIBQuery, IBQuery, Math,
   Respuesta_Consulta_WEB, BI_Functions, BI_DM, BI;

type
   //Para las duplas Dia,Total de Bisual
   TTotalDiaBisual = record
     Fecha: TDateTime;
     Total: Integer;
   end;
   TarrTotalesPorDiaBisual = array of TTotalDiaBisual;

const
   NOM_LOG_BICHECK = 'BI_Fedintia_Client_BI_CHECK';
   NOM_CSV_VENTAS_BI = 'Ventas_Fede_Bisual';
   STR_EMPTY: String ='';
   ERROR_DAME_TOTAL_BISUAL  = -2;
   ERROR_DAME_TOTALES_FEDE  = -999;
   DAME_TOTALES_FEDE_ERROR  = -999;
   DAME_TOTALES_POR_DIA_FEDE_OK     = 0;
   DAME_TOTALES_POR_DIA_FEDE_ERROR  = 0;
   ERROR_COMPARA_FDB_BI = -1;
   OK_COMPARA_FDB_BI_SIN_DIFERENCIAS = 0;
   OK_COMPARA_FDB_BI_CON_DIFERENCIAS = 1;
   TipusCredencialsIOF      = 1;
   TipusCredencialsFarmacia = 2;

var
   Proceso: TStringList;
   Srvr_IOFNET: string='http://80.169.85.244/soap/servlet/rpcrouter';
   FicheroLog: String;
   wsCredencials, wsServidor, wsDataBase, Resposta: widestring;
   CredencialsUsuari: string;


function  SQLServer_ExecSQL(Credenciales, Servidor, DataBase:WideString; ComandoSQL: WideString): WideString; stdcall; external 'SOAPFedeFarmTimeout.dll';
function  GetLastDLLError_SQL : WideString; stdcall; external 'SOAPFedeFarmTimeout.dll';
function  CompararNumLineasVentaFDB_y_BI( strDirIMS, strDirExe: string; out textoSalida: string ): integer;
function  CompararNumLineasVentaIOFWIN_y_BI_V2( strDirIMS, strDirExe: string; out textoSalida: string ): integer;
function  FitxerLog(NombreFichero,cTexto: string): boolean;
procedure LogMsg(const msg: String);
function  DameVentasFede(DesdeFecha,HastaFecha:TDateTime): Integer;
function  DameVentasPorDiaFede( DesdeFecha,HastaFecha:TDateTime; out IBQueryConsulta:TIBquery ): integer;
function  DameTotalBisual(DesdeFecha,HastaFecha:TDateTime;sSoci:String):Integer;
function  DameVentasPorDiaBisual( FechaIni,FechaFin:TDateTime; Socio:string ): IXMLSqlresponseType;
function  BuscaDiaEnTotalesBisual( arrDiaTotal:TarrTotalesPorDiaBisual; fecha:TDateTime ): TTotalDiaBisual;
function  DameVentasIMS_NEW_7( FechaIni,FechaFin:TDateTime ): Integer;
procedure ComprobaCredencials(TipusCredencials: integer);
function  miSQLSrvr_ExecSQL(ComandoSQL: String; TipusSortida:string='xml'; TipusCredencials: integer=TipusCredencialsIOF): String;
function  DameComponerFecha(CFecha: String): String;
Procedure Ejecuta_SP(cadena:string;GRAVAr:boolean);


implementation



(*----------------------------------------------------------------------------------------------------------------------*)
(*------[ LO NUEVO, INVERTIR COMPARACION (manda IOFWIN, no IB) ]--------------------------------------------------------*)
(*----------------------------------------------------------------------------------------------------------------------*)


(*-----------------------------------------------------------------------------
  AQUI EJECUTAMOS EL PROCESO
------------------------------------------------------------------------------*)
function CompararNumLineasVentaIOFWIN_y_BI_V2( strDirIMS, strDirExe: string; out textoSalida: string ): integer;
Var
  sSoci         : String;
  biBisual      : String;
  iRespuestaWeb : IXMLSqlresponseType;
  i             : Integer;
  ExportaExcel  : TextFile;

  TotalesFede, TotalesBisual, TotalesIMSNEW7 : Integer;

  DesdeFecha,HastaFecha : TDateTime;

  Fecha,Destino : String;
  Totales: Boolean;
  fechaChecked: variant;
  {FedeIgualIMSNEW7,}BisualIgualIMSNEW7: boolean;

  biFecha: TDateTime;
  biTotal: integer;
  diaBI: TTotalDiaBisual;
  arrTotalesPorDiaBisual: TarrTotalesPorDiaBisual;

begin
   result := ERROR_COMPARA_FDB_BI;
   try //Si algo falla abortar el proceso, ya se volver� a intentar la proxima vez.

      ForceDirectories( strDirIMS );
      FicheroLog := strDirIMS+NOM_LOG_BICHECK+'_'+FormatDateTime('yyyymmdd',now)+'.log';
      try
        Destino   := strDirIMS+NOM_CSV_VENTAS_BI+'_'+FormatDateTime('yyyymmdd_hhmmss',Now)+'.CSV';
        BiBisual  := Eje_SP_Ret('select BI_ACTIU as Retorn from BI_CONTROL');
        sSoci     := Eje_SP_Ret('select usuari_iofnet as retorn from empleats_iofnet where NUM_CATEGORIA=0');

        LogMsg('Destino CSV: '+Destino);
        LogMsg('Bi_ACTIU en BI_CONTROL: '+BiBisual);
        LogMsg('Socio: '+sSoci);
      except
        on e:Exception do
        begin
           raise exception.create( 'obtenidos datos para la consulta, '+ bi_dm.DataModule1.Database1.DatabaseName+' Error: '+e.message );
        end;
      end;


      //CREO EL CAMPO BI_CONTROL.BI_DATA_CHECKED POR SI NO EXISTE.
      //NO SE CONTROLA SI PETA O NO PETA PORQUE, LO NORMAL, ES QUE PETE AL INTENTAR A�ADIR EL CAMPO PORQUE YA EXISTA (SOLO FUNCIONAR� LA PRIMERA VEZ)
      //YA SE CONTROLA EN Ejecuta_SP() PARA QUE NO SALTE EL ERROR
      Ejecuta_SP('EXECUTE PROCEDURE ADD_CAMP("BI_CONTROL", "BI_DATA_CHECKED", "DATA_VUIDA");', True);
      if Eje_SP_Ret('Select BI_DATA_CHECKED as RETORN From BI_CONTROL',true) = strToDateTime('01/01/1900 00:00:00') then //null then
      begin
         try
            Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr('31.12.2018 00:00:00'),True);
         except
            on e:exception do
            begin
               raise exception.create(' guardando fecha bi checked inicial. '+e.message );
            end;
         end;
      end;


      if UpperCase(BiBisual)='N' then
      begin
        LogMsg('No se ha realzado consulta. Usuario no registrado: BI_CONTROL ->BI_ACTIU =N . Consulta cancelada');
        result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
        Exit;
      end;


      //CALCULO DEL RANGO DE FECHAS A COMPARAR
      //DEBEMOS DEJAR UN RANGO DE SEGURIDAD DE 2 DIAS (PARA DAR TIEMPO A QUE LOS DATOS SE INTEGREN EN BI)
      fechaChecked := Eje_SP_Ret('Select BI_DATA_CHECKED as RETORN From BI_CONTROL',true);
      if fechaChecked = null then
      begin
         raise exception.create( 'obtenidos BI_DATA_CHECKED.' );
      end
      else
      begin
         DesdeFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_CHECKED as RETORN from BI_CONTROL')))+1;
         HastaFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_ENVIADA as RETORN from BI_CONTROL')));
         while (not (HastaFecha<(now-2))) do
           HastaFecha := HastaFecha-1;
      end;


      //CONTROLAR COHERENCIA DE FECHAS
      if DesdeFecha>HastaFecha then
      begin
         LogMsg('Todavia no es necesario hacer la comparaci�n.');
         LogMsg('Proceso de consulta FDB vs BISUAL finalizado');
         textoSalida := 'Todavia no es necesario hacer la comparaci�n.';
         result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
      end
      else
      begin
         bi.BI_Form1.PB_BICHECK.Position:= 0;
         bi.BI_Form1.PB_BICHECK.Max := 3;

         //PRIMERO CONSULTAMOS EL TOTAL DE LINEAS ENTRE LAS FECHAS INDICADAS (DE IOFWIN y DE BI)
         bi.BI_Form1.PB_BICHECK.StepIt;
         TotalesFede   := DameVentasFede(DesdeFecha,HastaFecha);
         bi.BI_Form1.PB_BICHECK.StepIt;
         TotalesBisual := DameTotalBisual(DesdeFecha,HastaFecha,sSoci);

         if TotalesBisual=ERROR_DAME_TOTAL_BISUAL then
         begin
            raise exception.create( 'consultando BI entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha) );
         end
         else
         begin
            if TotalesFede=DAME_TOTALES_FEDE_ERROR then
            begin
               raise exception.create( 'consultando BD FARMACIA entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha));
            end
            else
            begin

               BILog( 'Comparando ventas entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha) );
               LogMsg('Fecha de consulta:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha));
               LogMsg('Totales de ventas en IOFWIN: '+IntToStr(TotalesFede)+' y BI '+IntToStr(TotalesBisual));

               //SI TOTALES DE IOFWIN Y BI COINCIDEN...
               if (TotalesFede=TotalesBisual) then
               begin
               (*...................................> LOS TOTALES SON IGUALES <.......................................*)

                  logMsg('No es necesaria la consulta desde :'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha)+#13+#09+
                  '       TotalesFede:   '+IntToStr(TotalesFede)+#13+#09+
                  '       TotalesBisual: '+IntToStr(TotalesBisual));

                  //SERGIO, ACTUALIZO LA FECHA DEL ULTIMO DIA CHECKED OK.
                  Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',HastaFecha)),True);
                  LogMsg('Proceso de consulta IOFWIN vs BI finalizado.');
                  textoSalida := 'Totales iguales no es necesario revisar dia por dia.';

                  result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
                  textoSalida := 'Los totales coinciden, no hay diferencias.';

                  bi.BI_Form1.PB_BICHECK.StepIt;

               end
               else
               begin
               (*...................................> LOS TOTALES NO COINCIDEN <.......................................*)

                  bi.BI_Form1.PB_BICHECK.StepIt;
                  Application.ProcessMessages;
                  try
                      try
                          //OBTENER VENTAS DIA POR DIA DE IOFWIN
                          if DameVentasPorDiaFede(DesdeFecha,HastaFecha,bi_dm.DataModule1.IBQ_Generico) = DAME_TOTALES_POR_DIA_FEDE_OK then
                          begin
                              bi.BI_Form1.PB_BICHECK.Max := bi.BI_Form1.PB_BICHECK.Position + 2;
                              bi.BI_Form1.PB_BICHECK.StepIt;
                              Application.ProcessMessages;

                              //OBTENER VENTAS DIA POR DIA DE BI
                              IRespuestaWeb := DameVentasPorDiaBisual( DesdeFecha,HastaFecha,sSoci );
                              bi.BI_Form1.PB_BICHECK.StepIt;
                              Application.ProcessMessages;
                              if assigned(iRespuestaWeb) then
                              begin
                                  bi.BI_Form1.PB_BICHECK.Max := bi.BI_Form1.PB_BICHECK.Position + IRespuestaWeb.Rowset.Count;

                                  //CARGO EN ARRAY LOS TOTALES DIA POR DIA DE BI
                                  for i := 0 to IRespuestaWeb.Rowset.Count-1 do
                                  begin
                                     try
                                        SetLength( arrTotalesPorDiaBisual, Length(arrTotalesPorDiaBisual)+1 );
                                        biFecha   := StrToDate( Copy( DameComponerFecha(Copy(iRespuestaWeb.Rowset.R[i].Nodes[1].Text,1,Pos('T',iRespuestaWeb.Rowset.R[i].Nodes[1].Text)+2))+'01/01/1901',1,10) );
                                        biTotal   := StrToIntDef( iRespuestaWeb.Rowset.R[i].Nodes[0].Text,0 );
                                        arrTotalesPorDiaBisual[high(arrTotalesPorDiaBisual)].fecha := biFecha;
                                        arrTotalesPorDiaBisual[high(arrTotalesPorDiaBisual)].total := biTotal;
                                        //Totales := False;
                                        bi.BI_Form1.PB_BICHECK.StepIt;
                                     except
                                       on e:exception do
                                       begin
                                          raise Exception.Create('cargando array con totales de BI. '+e.message);
                                       end;
                                     end;
                                  end;
                                  Application.ProcessMessages;

                                  //CREA INFORME CSV
                                  Try
                                    if FileExists(Destino) then
                                       DeleteFile(Destino);   //donde se va guardar el CSV

                                     AssignFile(ExportaExcel,Destino);
                                     Rewrite(ExportaExcel);
                                     WriteLn(ExportaExcel,'Periodo consulta desde:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha));
                                     WriteLn(ExportaExcel,'Desde Fecha;Hasta Fecha;Total Fede;Total BisualFarma;Direrencias');
                                     LogMsg('Creado fichero CSV para exportaci�n: '+Destino);
                                  except
                                     on e:Exception do
                                     begin
                                        raise exception.create( 'no se ha podido crear fichero CSV para exportaci�n Error: '+e.message );
                                     end;
                                  end;

                                  //PROCESO DE COMPARACI�N DE TOTALES DIA POR DIA (SQL IOFWIN CONTRA ARRAY BI)
                                  with bi_dm.DataModule1 do
                                  begin
                                      IBQ_Generico.Last;
                                      bi.BI_Form1.PB_BICHECK.Max := bi.BI_Form1.PB_BICHECK.Position + IBQ_Generico.RecordCount;
                                      IBQ_Generico.First;
                                      //RECORRO LOS DIAS OBTENIDOS DE IOFWIN Y LOS BUSCO EN EL ARRAY DE BI
                                      //COMPARANDO LOS DATOS DE IOFWIN CON LOS DATOS BISUALFARMA DIA POR DIA
                                      logMsg('Comprobando totales IOFWIN vs BISUAL');
                                      while not IBQ_Generico.eof do
                                      begin
                                         bi.BI_Form1.PB_BICHECK.StepIt;
                                         Application.ProcessMessages;

                                         Fecha := formatDateTime('dd/mm/yyyy',IBQ_Generico.FieldByName('fecha').AsDateTime);
                                         diaBI := BuscaDiaEnTotalesBisual( arrTotalesPorDiaBisual, StrToDate(fecha) );
                                         //if (diaBI.Total=-1) and (diaBI.fecha=strToDate('01/01/1900')) then
                                         //   TotalesBisual := 0;
                                         TotalesFede   := IBQ_Generico.fieldbyname('totales').AsInteger;
                                         TotalesBisual := max(0,diaBI.Total); //porque BuscaDiaEnTotalesBisual() devuelve -1 si no encuentra el dia
                                         Totales       := TotalesFede=TotalesBisual;

                                       //REGISTRO EN INFORME CSV, ACTUALIZO TABLA CONTROL Y LOG
                                         //Si no hay diferencia
                                         if Totales then
                                         begin
                                            //Este dia esta bien
                                            //Anoto en el informe csv
                                            //y actualizo BI_CONTROL.BI_DATA_CHECKED (fecha ultimo dia chekced OK)
                                            Writeln( ExportaExcel, Fecha+';' +Fecha+';' +IntToStr(TotalesFede)+';' +IntToStr(TotalesBisual) );
                                            Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha))),True);
                                         end
                                         else
                                         begin
                                            //Este dia esta MAL
                                            TotalesIMSNEW7     := DameVentasIMS_NEW_7(StrToDateTime(Fecha),StrToDateTime(Fecha));
                                            BisualIgualIMSNEW7 := TotalesBisual=TotalesIMSNEW7;
                                            //FedeIgualIMSNEW7   := TotalesFede=TotalesIMSNEW7;
                                            if not {FedeIgualIMSNEW7} BisualIgualIMSNEW7 then
                                            begin
                                               //Anoto en el informe csv
                                               //y actualizo BI_CONTROL.BI_DATA_CHECKED, lo marco como dia correcto porque no se desea reenviar la informaci�n
                                               Writeln( ExportaExcel, Fecha+';' +Fecha+';' +IntToStr(TotalesFede)+';' +IntToStr(TotalesBisual)+';' +'*** no reenviar porque nuevo fichero <> total ventas BI' ); //IOFWIN');
                                               LogMsg('* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'); //IOFWIN.');
                                               BILog ('* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'); //IOFWIN.');
                                               Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha))),True);
                                            end
                                            else
                                            begin
                                               //Anoto en el informe csv
                                               //Actualiza BI_CONTROL poniendo como �ltima fecha enviada la fecha que estamos procesando - 1
                                               // para que al ejecutar el proceso de envio del BI_FEDINTIA_CLIENT se envie desde esa fecha. Se enviaran todos los dias desde esa fecha.
                                               //Y actualizo BI_CONTROL.BI_DATA_CHECKED con la fecha del dia incorrecto -1 para que la pr�xima vez checkee desde el d�a incorrecto
                                               Writeln(ExportaExcel, Fecha+';' +Fecha+';' +IntToStr(TotalesFede)+';' +IntToStr(TotalesBisual)+';' +'***');
                                               Ejecuta_SP('update bi_control set BI_DATA_ENVIADA='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha)-1)),True);
                                               Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha)-1)),True);
                                               break;
                                            end;
                                         end;

                                         if i mod 100 =0 then
                                            Application.ProcessMessages;

                                         IBQ_Generico.Next;
                                      end; //fin bucle while not IBQ_Generico.eof
                                  end; //fin with bi_dm.DataModule1

                                  //CODIGO Y TEXTO DEL RESULTADO DEL PROCESO
                                  //Si no se han encontrado diferencias
                                  if totales then
                                  begin
                                     result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
                                     LogMsg( 'No se han encontrado diferencias.' );
                                     textoSalida := 'No se han encontrado diferencias.';
                                  end
                                  else
                                  begin
                                     if BisualIgualIMSNEW7 {FedeIgualIMSNEW7} then
                                     begin
                                        result := OK_COMPARA_FDB_BI_CON_DIFERENCIAS;
                                        LogMsg('* Diferencia encontrada, REENVIAMOS fichero, dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero: '+IntToStr(TotalesIMSNEW7));
                                        textoSalida := '* Diferencia encontrada, REENVIAMOS fichero, dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero: '+IntToStr(TotalesIMSNEW7);
                                     end
                                     else
                                     begin
                                        result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
                                        LogMsg('* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'); //IOFWIN.');
                                        textoSalida := '* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'; //IOFWIN.';
                                     end;
                                  end;

                                  LogMsg('Proceso de consulta FDB vs BISUAL finalizado.');
                                  bi.BI_Form1.PB_BICHECK.position := bi.BI_Form1.PB_BICHECK.Max;
                              end
                              else //else respuesta consulta totales por dia BI correcta
                              begin
                                  raise Exception.Create('consultando totales por dia en BI.');
                              end;
                          end
                          else //si a fallado la obtencion de las ventas por dia de IOFWIN
                          begin
                              raise Exception.Create('obteniendo totales por dia de IOFWIN.');
                          end;
                      except
                        on e:Exception do
                        begin
                          raise exception.create( 'comparando dia por dia, '+e.message);
                        end;
                      end;
                  finally
                     CloseFile(ExportaExcel);
                     LogMsg(STR_EMPTY);
                  end;
              (*...................................> FIN LOS TOTALES NO COINCIDEN <.......................................*)


               end; //FIN SI TOTALES DE IOFWIN Y BI COINCIDEN
            end; //Fin si no hay error en la consulta de totales Fede
         end; //Fin si no hay error en la consulta de totales a BI
      end; //Fin control coherencia de fechas.

   except
      on e:exception do
      begin
         result := ERROR_COMPARA_FDB_BI;
         LogMsg('Error comparando lineas de venta de IOFWIN con BISUALFARMA, '+e.message);
      end;
   end;
end;

(*----------------------------------------------------------------------------------------------------------------------
  CONSULTA EN EL WS SQL DE BI LOS TOTALES DE LINEAS DE VENTA DIA POR DIA ENTRE LAS FECHA PASADAS POR PARAMETRO PARA EL
  SOCIO INDICADO POR PARAMETRO.
  DEVUELVE UN IXMLSqlresponseType CON LA RESPUESTA DEL WS.
----------------------------------------------------------------------------------------------------------------------*)
function DameVentasPorDiaBisual( FechaIni,FechaFin:TDateTime; Socio:string ): IXMLSqlresponseType;
var
  CadenaSQL: String;
  Resp: WideString;
  RespWeb: TStringStream;
  XMLDoc: TXMLDocument;
begin
   Result := nil;
   CadenaSQL := ' SELECT count(*) as totales, ticket_date from ff_bi_iofwin where store ='+QuotedStr(Trim(Socio))+
                ' and ticket_date between '+QuotedStr(DateToStr(FechaIni))+' and '+QuotedStr(DateToStr(FechaFin))+
                ' and typeoperation='+QuotedStr('V')+
                ' group by ticket_date '+
                ' order by ticket_date ';

   //AQUI CONSULTAMOS LOS DATOS BISUALFARMA UTILIZANDO EL WS
   LogMsg('Haciendo consulta al servicio web '+Srvr_IOFNET);
   try
      Resp     := miSQLSrvr_ExecSQL(CadenaSQL);
      RespWeb  := TStringStream.Create(Resp);
      try
         RespWeb.Position:=0;
         bi.BI_Form1.PB_BICHECK.StepIt;
         try
            XMLDoc := TXMLDocument.Create(nil);
            try
              XMlDoc.LoadFromStream(RespWeb);
              XMLDoc.Active;
              Result := Getsqlresponse(XMLDoc);
            finally
              //SERGIO AGUADO, TODO, Peta???? Utilizar IXMLdocument???
              //FreeAndNil(XMLDoc);
            end;
         except
            on e:exception do
            begin
               raise Exception.Create(' cargando respuesta en XML. '+e.message);
            end;
         end;
      finally
         FreeAndNil(RespWeb);
      end;
   except
      on e:exception do
      begin
         raise exception.create( 'consultando datos contra WS de BI. Error: '+e.message);
      end;
   end;
end;

(*-----------------------------------------------------------------------------
  CONSULTA historic_operacions_detall EN LA BD DE LA FARMACIA Y
  DEVUELVE EL NUMERO DE LINEAS DE VENTA TOTALES ENTRE UNAS FECHAS DETERMINADAS
------------------------------------------------------------------------------*)
function DameVentasPorDiaFede( DesdeFecha,HastaFecha:TDateTime; out IBQueryConsulta:TIBquery ): integer;
begin
   result:=DAME_TOTALES_POR_DIA_FEDE_ERROR;

   with IBQueryConsulta do
   begin

       If Active then
          Close;

       SQL.Clear;
       SQL.Text :=
          ' select count(*) as Totales, '+
          ' CAST( extract( YEAR FROM mes.data_operacio)||''.''||extract( MONTH FROM mes.data_operacio)||''.''||extract( DAY FROM mes.data_operacio) AS DATE) AS fecha '+
          ' from historic_operacions_mestre mes '+
          ' inner join historic_operacions_detall det on mes.codi_relacio=det.codi_relacio '+
          ' where mes.data_operacio between '+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',DesdeFecha))+
          ' and '+QuotedStr(FormatDateTime('dd.mm.yyy 23:59:59',HastaFecha))+
          ' and mes.codi_operacio=''0'' '+
          ' group by 2 '+
          ' order by 2 ';
       try
          Prepare;
          if Prepared then
            Open;
          result := DAME_TOTALES_POR_DIA_FEDE_OK;
       except
          //result:=0;
          result := DAME_TOTALES_POR_DIA_FEDE_ERROR;
       end;

   end;
end;


(*---------------------------------------------------------------------------------------------------------------------
  BUSCA UN DIA CONCRETO ENTRE LAS DUPLAS DIA,TOTALES DE BISUALFARMA.
  BUSCA UNA FECHA CONCRETA PASADA POR PARAMETRO EN EL ARRAY TIPO TarrTotalesPorDiaBisual PASADO POR PARAMETRO
  SI LO ENCUENTRA DEVUELVE DICHO DIA CON SU TOTAL, SI NO LO ENCUENTRA DEVUELVE total=-1 y fecha='01/01/1900'
  SI ELEVA LA EXCEPCI�N
-----------------------------------------------------------------------------------------------------------------------*)
function BuscaDiaEnTotalesBisual( arrDiaTotal:TarrTotalesPorDiaBisual; fecha:TDateTime ): TTotalDiaBisual;
var x:Integer;
begin
  result.Total := -1;
  result.Fecha := StrToDate('01/01/1900');
  try
    for x:=Low( arrDiaTotal ) to High( arrDiaTotal ) do
    begin
      if arrDiaTotal[x].Fecha = fecha then
      begin
        Result := arrDiaTotal[x];
        Exit;
      end;
    end;
  except
    on e:Exception do
    begin
      raise Exception.Create(' buscando dia en array de totales de BI. '+e.message);
    end;
  end;
end;

(*--------------------------------------------------------------------------------------------------------------------*)
(*----------------------fin nuevo, invertir comparacion---------------------------------------------------------------*)
(*--------------------------------------------------------------------------------------------------------------------*)



(*-----------------------------------------------------------------------------
  AQUI EJECUTAMOS EL PROCESO
------------------------------------------------------------------------------*)
function CompararNumLineasVentaFDB_y_BI( strDirIMS, strDirExe: string; out textoSalida: string ): integer;
Var
  CadenaSQL     : String;
  sSoci         : String;
  Resp          : WideString;
  RespWeb       : TStringStream;

  biBisual      : String;
  iRespuestaWeb : IXMLSqlresponseType;
  i             : Integer;
  ExportaExcel  : TextFile;

  TotalesFede, TotalesBisual, TotalesIMSNEW7 : Integer;

  DesdeFecha,HastaFecha : TDateTime;

  Fecha,Destino : String;
  Totales: Boolean;
  XMLDoc: TXMLDocument;
  fechaChecked: variant;
  {FedeIgualIMSNEW7,}BisualIgualIMSNEW7: boolean;

begin
   result := ERROR_COMPARA_FDB_BI;
   try //Si algo falla abortar el proceso, ya se volver� a intentar la proxima vez.

      ForceDirectories( strDirIMS );
      FicheroLog := strDirIMS+NOM_LOG_BICHECK+'_'+FormatDateTime('yyyymmdd',now)+'.log';
      try
        Destino   := strDirIMS+NOM_CSV_VENTAS_BI+'_'+FormatDateTime('yyyymmdd_hhmmss',Now)+'.CSV';
        BiBisual  := Eje_SP_Ret('select BI_ACTIU as Retorn from BI_CONTROL');
        sSoci     := Eje_SP_Ret('select usuari_iofnet as retorn from empleats_iofnet where NUM_CATEGORIA=0');

        LogMsg('Destino CSV: '+Destino);
        LogMsg('Bi_ACTIU en BI_CONTROL: '+BiBisual);
        LogMsg('Socio: '+sSoci);
      except
        on e:Exception do
        begin
           raise exception.create( 'obtenidos datos para la consulta, '+ bi_dm.DataModule1.Database1.DatabaseName+' Error: '+e.message );
        end;
      end;


      //SERGIO AGUADO, 27/05/2021. CREO EL CAMPO BI_CONTROL.BI_DATA_CHECKED POR SI NO EXISTE.
      //NO SE CONTROLA SI PETA O NO PETA PORQUE, LO NORMAL, ES QUE PETE AL INTENTAR A�ADIR EL CAMPO PORQUE YA EXISTA (SOLO FUNCIONAR� LA PRIMERA VEZ)
      //YA SE CONTROLA EN Ejecuta_SP() PARA QUE NO SALTE EL ERROR
      Ejecuta_SP('EXECUTE PROCEDURE ADD_CAMP("BI_CONTROL", "BI_DATA_CHECKED", "DATA_VUIDA");', True);
      if Eje_SP_Ret('Select BI_DATA_CHECKED as RETORN From BI_CONTROL',true) = strToDateTime('01/01/1900 00:00:00') then //null then
      begin
         try
            Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr('31.12.2018 00:00:00'),True);
         except
            on e:exception do
            begin
               raise exception.create(' guardando fecha bi checked inicial. '+e.message );
            end;
         end;
      end;


      if UpperCase(BiBisual)='N' then
      begin
        LogMsg('No se ha realzado consulta. Usuario no registrado: BI_CONTROL ->BI_ACTIU =N . Consulta cancelada');
        result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
        Exit;
      end;


      //SERGIO, CALCULO DEL RANGO DE FECHAS A COMPARAR
      fechaChecked := Eje_SP_Ret('Select BI_DATA_CHECKED as RETORN From BI_CONTROL',true);
      if fechaChecked = null then
      begin
         raise exception.create( 'obtenidos BI_DATA_CHECKED.' );
      end
      else
      begin
         DesdeFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_CHECKED as RETORN from BI_CONTROL')))+1;
         HastaFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_ENVIADA as RETORN from BI_CONTROL')));
         (* SERGIO, ESTO YA NO ES NECESARIO
         if (fechaChecked = '01/01/2019 00:00:00') then
         begin
            DesdeFecha := StrToDateTime('01/01/2019');
            HastaFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_ENVIADA as Retorn from BI_CONTROL')));
         end
         else
         begin
            //DesdeFecha:= StrToDateTime(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_ENVIADA as Retorn from BI_CONTROL')));
            //HastaFecha:= StrToDateTime(FormatDateTime('dd/mm/yyyy',now));
            DesdeFecha:= StrToDateTime(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('Select BI_DATA_CHECKED as RETORN From BI_CONTROL')))+1;
            HastaFecha := StrToDate(FormatDateTime('dd/mm/yyyy',Eje_SP_Ret('select BI_DATA_ENVIADA as Retorn from BI_CONTROL')));
         end;
         *)
         //DEBEMOS DEJAR UN RANGO DE SEGURIDAD DE 2 DIAS (PARA DAR TIEMPO A QUE LOS DATOS SE INTEGREN EN BI)
         while (not (HastaFecha<(now-2))) do
           HastaFecha := HastaFecha-1;
      end;


      //SERGIO, CONTROLAR COHERENCIA DE FECHAS
      if DesdeFecha>HastaFecha then
      begin
         LogMsg('Todavia no es necesario hacer la comparaci�n.');
         LogMsg('Proceso de consulta FDB vs BISUAL finalizado');
         textoSalida := 'Todavia no es necesario hacer la comparaci�n.';
         result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
      end
      else
      begin
         bi.BI_Form1.PB_BICHECK.Position:= 0;
         bi.BI_Form1.PB_BICHECK.Max := 5;

         bi.BI_Form1.PB_BICHECK.StepIt;
         TotalesFede   := DameVentasFede(DesdeFecha,HastaFecha);
         bi.BI_Form1.PB_BICHECK.StepIt;
         TotalesBisual := DameTotalBisual(DesdeFecha,HastaFecha,sSoci);

         if TotalesBisual=ERROR_DAME_TOTAL_BISUAL then
         begin
            raise exception.create( 'consultando BI entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha) );
         end
         else
         begin
            if TotalesFede=DAME_TOTALES_FEDE_ERROR then
            begin
               raise exception.create( 'consultando BD FARMACIA entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha));
            end
            else
            begin

               BILog( 'Comparando ventas entre fechas:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha) );
               LogMsg('Fecha de consulta:'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha));
               LogMsg('Totales de ventas en FDB: '+IntToStr(TotalesFede)+' y BiSualconsulta '+IntToStr(TotalesBisual));

               if (TotalesFede=TotalesBisual) then
               begin
               (*....> LOS TOTALES SON IGUALES <.....*)

                  logMsg('No es necesaria la consulta desde :'+DateTimeToStr(DesdeFecha)+' hasta '+DateTimeToStr(HastaFecha)+#13+#09+
                  '       TotalesFede:   '+IntToStr(TotalesFede)+#13+#09+
                  '       TotalesBisual: '+IntToStr(TotalesBisual));

                  //SERGIO, ACTUALIZO LA FECHA DEL ULTIMO DIA CHECKED OK.
                  Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',HastaFecha)),True);
                  LogMsg('Proceso de consulta FDB vs BISUAL finalizado.');
                  textoSalida := 'Totales iguales no es necesario revisar dia por dia.';

                  result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
                  //SERGIO, NO ROMPER EL FLUJO DE INTRUCCIONES. SALDR� POR LA RAMA ADECUADA SIN HACER NADA. SIN TENER QUE USAR EXIT
                  //exit;

               end
               else
               begin
               (*....> LOS TOTALES NO COINCIDEN <.....*)

                  //Fichero informe CSV
                  Try
                    if FileExists(Destino) then
                         DeleteFile(Destino);   //donde se va guardar el CSV

                     AssignFile(ExportaExcel,Destino);
                     Rewrite(ExportaExcel);
                     WriteLn(ExportaExcel,'Periodo consulta desde:'+Fecha+' hasta '+Fecha);
                     WriteLn(ExportaExcel,'Desde Fecha;Hasta Fecha;Total Fede;Total BisualFarma;Direrencias');
                     LogMsg('Creado fichero CSV para exportaci�n: '+Destino);
                  except
                     on e:Exception do
                     begin
                        raise exception.create( 'no se ha podido crear fichero CSV para exportaci�n Error: '+e.message );
                     end;
                  end;

                  CadenaSQL := ' SELECT count(*) as totales, ticket_date from ff_bi_iofwin where store ='+QuotedStr(Trim(sSoci))+
                               ' and ticket_date between '+QuotedStr(DateToStr(DesdeFecha))+' and '+QuotedStr(DateToStr(HastaFecha))+
                               ' and typeoperation='+QuotedStr('V')+
                               ' group by ticket_date '+
                               ' order by ticket_date ';

                  try
                     //SERGIO, AQUI CONSULTAMOS LOS DATOS BISUALFARMA UTILIZANDO EL WS
                     LogMsg('Haciendo consulta al servicio web '+Srvr_IOFNET);
                     bi.BI_Form1.PB_BICHECK.StepIt;
                     try
                        Resp     := miSQLSrvr_ExecSQL(CadenaSQL);
                        RespWeb  := TStringStream.Create(Resp);
                        RespWeb.Position:=0;
                        bi.BI_Form1.PB_BICHECK.StepIt;
                        try
                           XMLDoc := TXMLDocument.Create(nil);
                           try
                             XMlDoc.LoadFromStream(RespWeb);
                             XMLDoc.Active;
                             iRespuestaWeb:=Getsqlresponse(XMLDoc);
                           finally
                             //SERGIO AGUADO, TODO, Peta???? Utilizar IXMLdocument???
                             //FreeAndNil(XMLDoc);
                           end;
                        except
                           on e:exception do
                           begin
                              raise Exception.Create(' cargando respuesta en XML. '+e.message);
                           end;
                        end;
                     except
                        on e:exception do
                        begin
                           raise exception.create( 'consultando datos contra WS de BI. Error: '+e.message);
                        end;
                     end;


                     //SERGIO, COMPARANDO LOS DATOS DE IOFWIN CON LOS DATOS BISUALFARMA DIA POR DIA
                     logMsg('Comprobando totales FDB vs BISUAL');
                     bi.BI_Form1.PB_BICHECK.Max := bi.BI_Form1.PB_BICHECK.Position + IRespuestaWeb.Rowset.Count;
                     for i := 0 to IRespuestaWeb.Rowset.Count-1 do
                     begin
                        try
                           bi.BI_Form1.PB_BICHECK.StepIt;
                           Application.ProcessMessages;
                           Totales := False;
                           Fecha   := DameComponerFecha(Copy(iRespuestaWeb.Rowset.R[i].Nodes[1].Text,1,Pos('T',iRespuestaWeb.Rowset.R[i].Nodes[1].Text)+2));

                           if Fecha<>STR_EMPTY then
                           begin
                              TotalesFede   := DameVentasFede(StrToDateTime(Fecha),StrToDateTime(Fecha));
                              TotalesBisual := StrToInt(iRespuestaWeb.Rowset.R[i].Nodes[0].Text);
                              Totales       := TotalesFede=TotalesBisual;

                              if TotalesBisual<>ERROR_DAME_TOTAL_BISUAL then
                              begin
                                if Totales then
                                begin
                                   //Este dia esta bien
                                   Writeln(ExportaExcel,
                                          Fecha+';'+
                                          Fecha+';'+
                                          IntToStr(TotalesFede)+';'+
                                          IntToStr(TotalesBisual))
                                end
                                else
                                begin
                                   //Este dia esta MAL
                                   TotalesIMSNEW7     := DameVentasIMS_NEW_7(StrToDateTime(Fecha),StrToDateTime(Fecha));
                                   BisualIgualIMSNEW7 := TotalesBisual=TotalesIMSNEW7;
                                   //FedeIgualIMSNEW7   := TotalesFede=TotalesIMSNEW7;
                                   if not {FedeIgualIMSNEW7} BisualIgualIMSNEW7 then
                                   begin
                                      Writeln(ExportaExcel,
                                             Fecha+';'+
                                             Fecha+';'+
                                             IntToStr(TotalesFede)+';'+
                                             IntToStr(TotalesBisual)+';'+
                                             '*** no reenviar porque nuevo fichero <> total ventas BI'); //IOFWIN');
                                   end
                                   else
                                   begin
                                      Writeln(ExportaExcel,
                                             Fecha+';'+
                                             Fecha+';'+
                                             IntToStr(TotalesFede)+';'+
                                             IntToStr(TotalesBisual)+';'+
                                             '***');
                                   end;
                                end;
                              end;
                           end;

                           if i mod 100 =0 then
                              Application.ProcessMessages;

                           if not Totales  then
                           begin
                               //Solo si el nuevo fichero que generaremos sera igual a lo que hay en IOFWIN enviamos, porque igualaremos BI con IOFWIN
                               //Si son diferentes por muchas veces que enviemos el fichero no se solucionar� porque IOFWIN sera siempre <> BI
                               if {FedeIgualIMSNEW7} BisualIgualIMSNEW7 then
                               begin
                                  //SERGIO, INTEGRACION. ACTUALIZA BI_CONTROL PONIENDO COMO ULTIMA FECHA ENVIADA LA FECHA QUE ESTAMOS PROCESANDO - 1
                                  //PARA QUE AL EJECUTAR EL PROCESO DE ENVIO DEL BI_FEDINTIA_CLIENT SE ENVIE DESDE ESA FECHA. SE ENVIARAN TODOS LOS DIAS DESDE ESA FECHA.
                                  Ejecuta_SP('update bi_control set BI_DATA_ENVIADA='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha)-1)),True);

                                  //SERGIO, ACTUALIZO BI_CONTROL.BI_DATA_CHECKED CON LA FECHA DEL DIA INCORRECTO - 1 PARA QUE LA PROXIMA VEZ CHECKEE DESDE EL DIA INCORRECTO
                                  Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha)-1)),True);
                                  break;
                               end
                               else
                               begin
                                  LogMsg('* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'); //IOFWIN.');
                                  BILog ('* Diferencia encontrada (NO se reenvia), dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero(NO ENVIADO): '+IntToStr(TotalesIMSNEW7)+'. NO se envia fichero porque el fichero generado sigue siendo diferente a lo que hay en BI.'); //IOFWIN.');
                                  //SERGIO, ACTUALIZO BI_CONTROL.BI_DATA_CHECKED, LO MARCO COMO DIA CORRECTO PORQUE NO SE DESEA REENVIAR LA INFORMACI�N
                                  Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha))),True);
                               end;
                           end
                           else
                           begin
                               //SERGIO, ACTUALIZO BI_CONTROL.BI_DATA_CHECKED (fecha ultimo dia chekced OK)
                               Ejecuta_SP('update bi_control set BI_DATA_CHECKED='+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',StrToDate(Fecha))),True);
                           end;

                        except
                           on e:Exception do
                           begin
                               raise exception.create( 'generando linea fichero CSV, '+e.message);
                           end;
                        end;
                     end; //Fin bucle for totales dia por dia

                     if totales then
                     begin
                        result := OK_COMPARA_FDB_BI_SIN_DIFERENCIAS;
                        LogMsg( 'No se han encontrado diferencias.' );
                        textoSalida := 'No se han encontrado diferencias.';
                     end
                     else
                     begin
                        result := OK_COMPARA_FDB_BI_CON_DIFERENCIAS;
                        LogMsg('* Diferencia encontrada, REENVIAMOS fichero, dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero: '+IntToStr(TotalesIMSNEW7));
                        textoSalida := '* Diferencia encontrada, REENVIAMOS fichero, dia: '+fecha+', IOFWIN: '+IntToStr(TotalesFede)+' BISUALFARMA: '+IntToStr(TotalesBisual)+' NuevoFichero: '+IntToStr(TotalesIMSNEW7);
                     end;

                     LogMsg('Proceso de consulta FDB vs BISUAL finalizado.');
                     bi.BI_Form1.PB_BICHECK.position := bi.BI_Form1.PB_BICHECK.Max;


                  finally
                     CloseFile(ExportaExcel);
                     FreeAndNil(RespWeb);
                     LogMsg(STR_EMPTY);
                  end;

               end; //Fin si totales coinciden
            end; //Fin si no hay error en la consulta de totales Fede
         end; //Fin si no hay error en la consulta de totales a BI
      end; //Fin control coherencia de fechas.

   except
      on e:exception do
      begin
         result := ERROR_COMPARA_FDB_BI;
         LogMsg('Error comparando lineas de venta de IOFWIN con BISUALFARMA, '+e.message);
      end;
   end;
end;


//DEVUELVE EL NUMERO DE LINEAS DE VENTA DEL PROCEDIMIENTO ALMACENADO IMS_NEW_7, QUE ES EL QUE SE UTILIZA PARA GENERAR EL FICHERO BI
function DameVentasIMS_NEW_7( FechaIni,FechaFin:TDateTime ): Integer;
var
   contLineasVenta: integer;
begin
   result := -1;
   try
       contLineasVenta := 0;
       with BI_DM.DataModule1.IBQ_BI do
       begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT ID_OPERACION, DATA_OPERAC, TIPUS_VENDA, NUM_EMPRESA, NOM_EMPRESA, CODI_ARTICLE,UNI_VEN_CON_REC,VENTAS_CON_REC,UNI_VEN_SIN_REC,');
          SQL.Add('VENTAS_SIN_REC, UNI_COMPRA, CODI_PROVEIDOR, NOM_PROV, STOCK, P_COST, PVP_FITXA, PVP_VENDA, CODI_EAN13, NOM_PROD, FEDINTIA_CLIENT, CODI_FIDELITZACIO,');
          SQL.Add('CODI_PRESCRITO, CODI_VDA_CREUADA, CODI_USUARI, CODI_LABORATORI, CODI_CATEGORIA, TIPUS_PRODUCTE, PTOS_EXTRA, EUROS_EXTRA, PORCEN_IVA, CODI_CATEGORIA_IMS,');
          SQL.Add('PUNTS_VENDA, PUNTS_BESCANVIATS_VDA, CODI_FAMILIA, DESCRIPCIO_FAMILIA, CODI_SUBFAMILIA, DESCRIPCIO_SUBFAMILIA, DESCRIPCIO_LABORATORI,');
          SQL.Add('CODI_CLIENT, NOM_CLIENT, NOM_EMPLEAT, LINEAL,');
          SQL.Add('LINEAL_ANCHO, LINEAL_PROFUNDIDAD, PUC, PMC, PC_FIFO, DESC_TIPUS_PRODUCTE,');
          SQL.Add('RECARGO_EQUIVALENCIA, MARGEN_COMERCIAL, CODI_SUPERFAMILIA, DESCRIPCIO_SUPERFAMILIA');
          SQL.Add('FROM IMS_NEW_7("'+FormatDateTime('dd.mm.yyyy',FechaIni)+'","'+FormatDateTime('dd.mm.yyyy 23:59:59',fechafin)+'")');
          Open;
          while not EOF do
          begin
             if (FieldByName('UNI_VEN_SIN_REC').AsInteger <>0) or (FieldByName('UNI_VEN_CON_REC').AsInteger <>0) then
                inc(contLineasVenta);
             next;
          end;
          close;
          result := contLineasVenta;
       end;
   except
      on e:exception do
      begin
         raise exception.create(' DameventasIMS_NEW7(), '+e.message);
      end;
   end;
end;


(*-----------------------------------------------------------------------------
  CONSULTA historic_operacions_detall EN LA BD DE LA FARMACIA Y
  DEVUELVE EL NUMERO DE LINEAS DE VENTA TOTALES ENTRE UNAS FECHAS DETERMINADAS
------------------------------------------------------------------------------*)
function DameVentasFede(DesdeFecha,HastaFecha:TDateTime): Integer;
Var
  DesdeFechaConsulta,HastaFechaConsulta:TDateTime;
begin
  result:=DAME_TOTALES_FEDE_ERROR;

  with bi_dm.DataModule1.IBQ_Generico do
  begin

     If Active then
       Close;

     SQL.Clear;
     SQL.Text :=
     ' select count(*) as Totales from historic_operacions_mestre mes '+
     ' inner join historic_operacions_detall det on mes.codi_relacio=det.codi_relacio '+
     ' where mes.data_operacio between '+QuotedStr(FormatDateTime('dd.mm.yyy 00:00:00',DesdeFecha))+
     ' and '+QuotedStr(FormatDateTime('dd.mm.yyy 23:59:59',HastaFecha))+
     ' and mes.codi_operacio='+QuotedStr('0');

     try
       Prepare;
       if Prepared then
         Open;
       result := FieldByName('Totales').asInteger;
     except
       result := DAME_TOTALES_FEDE_ERROR;
     end;

  end;
end;


(*-----------------------------------------------------------------------------
  MEDIANTE UNA QUERY CONTRA ff_bi_iofwin UTILIZANDO EL WEBSERVICE DE CONSULTAS
  OBTIENE EL NUMERO DE LINEAS DE VENTA TOTAL ENTRE UNAS FECHAS DETERMINADAS.
  DEVUELVE -2 SI SE PRODUCE UN ERROR.
------------------------------------------------------------------------------*)
function DameTotalBisual(DesdeFecha,HastaFecha:TDateTime;sSoci:String):Integer;
Var
  CadenaSQL:String;
  Respuesta:String;
  cDesdeFecha,cHastaFecha:String;

begin
     result:=ERROR_DAME_TOTAL_BISUAL;

     cDesdeFecha  := DateTimeToStr(DesdeFecha);
     cDesdeFecha  := StringReplace(cDesdeFecha,'/','-',[rfReplaceAll]);

     cHastaFecha  := DateTimeToStr(HastaFecha);
     cHastaFecha  := StringReplace(cHastaFecha,'/','-',[rfReplaceAll]);

     CadenaSQL := ' SELECT count(*) as totales from ff_bi_iofwin where store ='+QuotedStr(sSoci)+
                  ' and ticket_date between '+QuotedStr(cDesdeFecha)+' and '+QuotedStr(cHastaFecha)+
                  ' and typeoperation='+QuotedStr('V');

    try
       respuesta := miSQLSrvr_ExecSQL(CadenaSQL);

       if (Pos('SQLException:',Respuesta)=0)  and (Pos('<sqlmaxrows>',Respuesta)>0) then
       begin
         delete(respuesta,1,Pos('<r>',Respuesta)+2);
         delete(respuesta,1,pos('<c>',Respuesta)+2);
         delete(respuesta,pos('</c>',Respuesta),length(Respuesta));
         result:=StrToInt(Respuesta);
       end
       else
       begin
         result:=ERROR_DAME_TOTAL_BISUAL;
       end;
    except
        result:=ERROR_DAME_TOTAL_BISUAL;
    end;
end;


(*-----------------------------------------------------------------------------
  DEVUELVE LA FECHA EN FORMATO DD/MM/YYYY
------------------------------------------------------------------------------*)
function DameComponerFecha(CFecha: String): String;
Var
  Dia,Mes,Ani:String;
  Zulu:Integer;
  Fecha:TDateTime;
begin
  try
    Zulu:=0;
    if Pos('T',cFecha)>0 then
    begin
      Zulu :=  StrToInt(Copy(CFecha,Pos('T',cFecha)+1,3));
    end;

    Delete(CFecha,Pos('T',cfecha),3);
    Ani := Copy(Cfecha,1,Pos('-',cFecha)-1);
    Delete(cFecha,1,Pos('-',cFecha));
    Mes := Copy(cFecha,1,Pos('-',cFecha)-1);
    Mes := StringOfChar('0',2-Length(Mes))+Mes;
    Delete(cFecha,1,Pos('-',cFecha));
    Dia := Copy(Cfecha,1,Length(cFecha));
    Dia := StringOfChar('0',2-Length(Dia))+Dia;

    Fecha := StrToDateTime(Dia+'/'+Mes+'/'+Ani);
    if Zulu in [22,23] then
         Fecha:=Fecha+1;
    //result:= Dia+'/'+Mes+'/'+Ani;
    result :=DateTimeToStr(Fecha);
  except
    result:=STR_EMPTY;
  end;
end;


(*-----------------------------------------------------------------------------
  Ejecuta un query pasant la cadena
------------------------------------------------------------------------------*)
Procedure Ejecuta_SP(cadena:string;GRAVAr:boolean);
begin
  With bi_dm.DataModule1.IBQ_Ejecuta_SP_biCheck do
  Begin
      Try
       close;
       SQL.Text:= cadena;
       ExecSQL;
       If gravar and bi_dm.DataModule1.Transaccion1.InTransaction Then
         bi_dm.DataModule1.Transaccion1.CommitRetaining;
       Close ;
      Except
       on e:Exception do
       begin
         if (pos('ADD_CAMP',upperCase(cadena))>0) and (pos('BI_DATA_CHECKED',upperCase(cadena))>0) then begin
           //SERGIO, me como el error, realmente, lo normal, es que no sea un error porque cuando el campo ya existe y se intente a�adir petar�.
         end
         else
         begin
           raise exception.create( 'Error ejecutando : ' + cadena );
         end;
       end;
      End;
  End;
end;


//************************************ SOLO PARA LAS CONSULTAS CONTRA EL WEBSERVICE ************************************

(*-----------------------------------------------------------------------------
  SERGIO, CONSTRUYE LAS CREDENCIALES PARA LA LLAMADA AL WEBSERVICE
------------------------------------------------------------------------------*)
procedure ComprobaCredencials(TipusCredencials: integer);
begin
     If TipusCredencials = TipusCredencialsIOF Then
     Begin
          wsCredencials := 'aW9md2luOml' + 'vZndpbjEyMw==';
     end
     else
     Begin
          While Length(CredencialsUsuari) mod 4 <> 0 Do
                CredencialsUsuari := CredencialsUsuari + '=';
          wsCredencials := widestring(CredencialsUsuari);
     end;
end;

(*-----------------------------------------------------------------------------
  SERGIO, LANZA UNA QUERY CONTRA EL WEBSERVICES DE CONSULTAS
------------------------------------------------------------------------------*)
function miSQLSrvr_ExecSQL(ComandoSQL: String; TipusSortida:string='xml'; TipusCredencials: integer=TipusCredencialsIOF): String;
Begin
  Result := 'No implementado:';
  wsServidor := Srvr_IOFNET;
  wsDataBase := 'bi_iofwin';
  ComprobaCredencials(TipusCredencials);

  try
     Resposta := SQLServer_ExecSQL(wsCredencials, wsServidor, wsDataBase, ComandoSQL);
     Resposta := AnsiReplaceText(Resposta,'�����','aeiou');
  except
     on e:Exception do
        Resposta := e.message;
  end;



  If Resposta = '' Then
     Resposta := GetLastDLLError_SQL;

  If Pos('<?xml ', Resposta)=0 Then
  Begin
                   //<?xml version="1.0" encoding="iso-8859-1"?>
       Resposta := '<?xml version="1.0" encoding="ISO-8859-1" ?>' + Resposta;
  end;

  try
     Resposta := FormatXMLData(Resposta);
  except
  end;

  Result := string(Resposta);

end;


//************************************ SOLO PARA EL LOG ***************************************************************

(*-----------------------------------------------------------------------------
  INTERNAL ESCRIBE EN LOG
------------------------------------------------------------------------------*)
function FitxerLog(NombreFichero,cTexto: string): boolean;
var
  Fichero:TextFile;
Begin
     Try
      result:=true;

      AssignFile(FICHERO,NombreFichero);
      If FileExists(NombreFichero) Then
        Append(FICHERO)
      Else
        Rewrite(Fichero);
      cTexto  := '['+FormatDateTime('dd/mm/yyy hh:nn:ss',now)+'] '+ cTexto+#13#10;
      Write(FICHERO,cTexto);
      Flush(Fichero);
      CloseFile(Fichero);
     Except
          result:= False;
     End;
end;

(*-----------------------------------------------------------------------------
  ESCRIBE EN LOG
------------------------------------------------------------------------------*)
procedure LogMsg(const msg: String);
begin
  FitxerLog(FicheroLog,msg);
end;

//********************************************************************************************************************

end.
